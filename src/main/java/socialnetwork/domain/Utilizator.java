package socialnetwork.domain;

import socialnetwork.utils.Constants;
import org.mindrot.jbcrypt.BCrypt;

import java.util.*;

public class Utilizator extends Entity<Long> {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private List<Utilizator> friends;

    public Utilizator() {
        this.firstName = "";
        this.lastName = "";
        this.email = "";
        this.password = "";
        this.friends = new ArrayList<Utilizator>();
    }

    public Utilizator(String firstName, String lastName, String email, String password) {
        setId(generateRandomId());
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.friends = new ArrayList<Utilizator>();
    }

    public Utilizator(Long id, String firstName, String lastName, String email, String password) {
        setId(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.friends = new ArrayList<Utilizator>();
    }

    private Long generateRandomId() {
        long leftLimit = 1L;
        long rightLimit = 1000000000L;
        long generatedLong = leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
        return generatedLong;
    }

    private String hashPassword(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return this.email;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = hashPassword(password);
    }

    public List<Utilizator> getFriends() {
        return this.friends;
    }

    public String friendsToString() {
        String friendsToString = "[";
        for (Utilizator utilizator : this.friends) {
            friendsToString += utilizator.getId() + ";" + utilizator.firstName + ";" + utilizator.lastName + ", ";
        }
        if (friendsToString.length() > 2) {
            friendsToString = friendsToString.substring(0, friendsToString.length() - 2);
        }
        friendsToString += "]";
        return friendsToString;
    }

    public void addFriend(Utilizator friend) {
        this.friends.add(friend);
    }

    public void deleteAllFriends() {
        this.friends.clear();
    }

    @Override
    public String toString() {
        return this.getFirstName() + " | " + this.getLastName() + " | " + this.getEmail();
        /*return "Utilizator{" +
                "id=" + getId() +
                ", firstName='" + getFirstName() + '\'' +
                ", lastName='" + getLastName() + '\'' +
                ", email='" + getEmail() + '\'' +
                ", password='" + getPassword() + '\'' +
                ", friends=" + friendsToString() +
                '}';*/
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Utilizator)) return false;
        Utilizator that = (Utilizator) o;
        return getFirstName().equals(that.getFirstName()) &&
                getLastName().equals(that.getLastName()) &&
                getEmail().equals(that.getEmail()) &&
                getPassword().equals(that.getPassword()) &&
                getFriends().equals(that.getFriends());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getEmail(), getPassword(), getFriends());
    }
}