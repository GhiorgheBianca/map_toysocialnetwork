package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Eveniment extends Entity<Long> {
    private String name;
    private String description;
    private String type; //"public" or "private"
    private LocalDateTime start_date;
    private LocalDateTime final_date;
    private Set<Long> participants = new HashSet<>();

    public Eveniment(String name, String description, String type, LocalDateTime start_date, LocalDateTime final_date, Set<Long> participants) {
        this.setId(this.generateRandomId());
        this.name = name;
        this.description = description;
        this.type = type;
        this.start_date = start_date;
        this.final_date = final_date;
        this.participants = participants;
    }

    public Eveniment(Long id, String name, String description, String type, LocalDateTime start_date, LocalDateTime final_date, Set<Long> participants) {
        this.setId(id);
        this.name = name;
        this.description = description;
        this.type = type;
        this.start_date = start_date;
        this.final_date = final_date;
        this.participants = participants;
    }

    private Long generateRandomId() {
        long leftLimit = 1L;
        long rightLimit = 1000000000L;
        long generatedLong = leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
        return generatedLong;
    }

    public void addUser_toParticipants(Long id) {
        this.participants.add(id);
    }

    public void removeUser_fromParticipants(Long id) {
        this.participants.remove(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LocalDateTime getStart_date() {
        return start_date;
    }

    public void setStart_date(LocalDateTime start_date) {
        this.start_date = start_date;
    }

    public LocalDateTime getFinal_date() {
        return final_date;
    }

    public void setFinal_date(LocalDateTime final_date) {
        this.final_date = final_date;
    }

    public Set<Long> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<Long> participants) {
        this.participants = participants;
    }

    @Override
    public String toString() {
        return "Eveniment{" +
                "id=" + getId() +
                ", nume='" + getName() + '\'' +
                ", descriere='" + getDescription() + '\'' +
                ", tip='" + getType() + '\'' +
                ", participanti=" + getParticipants().toString() +
                ", data de inceput=" + getStart_date().format(Constants.DATE_TIME_FORMATTER) +
                ", data de final=" + getFinal_date().format(Constants.DATE_TIME_FORMATTER) +
                '}';
    }
}
