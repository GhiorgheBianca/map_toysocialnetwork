package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class Prietenie extends Entity<Tuple<Long,Long>> {
    private LocalDateTime date;

    public Prietenie() {
        this.date = LocalDateTime.now();
    }

    public Prietenie(Long id_utilizator1, Long id_utilizator2) {
        setId(new Tuple<>(id_utilizator1, id_utilizator2));
        this.date = LocalDateTime.now();
    }

    public Prietenie(Long id_utilizator1, Long id_utilizator2, LocalDateTime date) {
        setId(new Tuple<>(id_utilizator1, id_utilizator2));
        this.date = date;
    }

    /**
     *
     * @return the date when the friendship was created
     */
    public LocalDateTime getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Prietenie{" +
                "id1=" + getId().getLeft() +
                ", id2=" + getId().getRight() +
                ", date=" + getDate().format(Constants.DATE_TIME_FORMATTER) +
                '}';
    }
}
