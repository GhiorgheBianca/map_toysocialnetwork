package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Mesaj extends Entity<Long> {
    private Long from;
    private List<Long> to = new ArrayList<>();
    private String message;
    private LocalDateTime date;
    private Long id_reply = null;

    public Mesaj(Long from, List<Long> to, String message) {
        this.setId(generateRandomId());
        this.from = from;
        this.to = to;
        this.message = message;
        this.date = LocalDateTime.now();
    }

    public Mesaj(Long from, List<Long> to, String message, Long id_reply) {
        this.setId(generateRandomId());
        this.from = from;
        this.to = to;
        this.message = message;
        this.date = LocalDateTime.now();
        this.id_reply = id_reply;
    }

    public Mesaj(Long id, Long from, List<Long> to, String message, LocalDateTime date, Long id_reply) {
        this.setId(id);
        this.from = from;
        this.to = to;
        this.message = message;
        this.date = date;
        this.id_reply = id_reply;
    }

    private Long generateRandomId() {
        long leftLimit = 1L;
        long rightLimit = 1000000000L;
        long generatedLong = leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
        return generatedLong;
    }

    public Long getFrom() {
        return from;
    }
    public void setFrom(Long from) {
        this.from = from;
    }

    public List<Long> getTo() {
        return to;
    }
    public void setTo(List<Long> to) {
        this.to = to;
    }

    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getDate() {
        return date;
    }
    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Long getId_reply() {
        return id_reply;
    }
    public void setId_reply(Long id_reply) {
        this.id_reply = id_reply;
    }

    @Override
    public String toString() {
        if (getId_reply() != null) {
            return "Mesaj{" +
                    "id=" + getId() +
                    ", from='" + getFrom() + '\'' +
                    ", to='" + getTo().toString() + '\'' +
                    ", message='" + getMessage() + '\'' +
                    ", date=" + getDate().format(Constants.DATE_TIME_FORMATTER) + '\'' +
                    ", id_reply=" + getId_reply().toString() +
                    '}';
        }
        return "Mesaj{" +
                "id=" + getId() +
                ", from='" + getFrom() + '\'' +
                ", to=" + getTo().toString() +
                ", message='" + getMessage() + '\'' +
                ", date=" + getDate().format(Constants.DATE_TIME_FORMATTER) +
                ", id_reply=null" +
                '}';
    }
}
