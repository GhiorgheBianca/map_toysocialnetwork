package socialnetwork.domain;

import socialnetwork.service.SuperService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class UsersPage {
    private Utilizator connected_user;
    private SuperService service;

    public UsersPage(Utilizator user, SuperService service) {
        this.connected_user = user;
        this.service = service;
    }

    public void setConnected_user(Utilizator user) {
        this.connected_user = user;
    }

    public Utilizator getConnected_user() {
        return this.connected_user;
    }

    public SuperService getService() {
        return this.service;
    }

    public List<Utilizator> getUsersFriends() {
        service.getUtilizatorService().connectFriendships();
        return connected_user.getFriends();
    }

    public List<MesajDTO> getUsersReceivedMessages() {
        Iterable<MesajDTO> received_messages = service.getMesajService().getMessages_receivedByUser(connected_user.getId());
        List<MesajDTO> received_messageList = StreamSupport
                .stream(received_messages.spliterator(), false)
                .collect(Collectors.toList());
        return received_messageList;
    }

    public List<MesajDTO> getUsersSentMessages() {
        Iterable<MesajDTO> received_messages = service.getMesajService().getMessages_sentByUser(connected_user.getId());
        List<MesajDTO> received_messageList = StreamSupport
                .stream(received_messages.spliterator(), false)
                .collect(Collectors.toList());
        return received_messageList;
    }

    public List<CerereDTO> getUsersReceivedRequests() {
        Iterable<CerereDTO> received_requests = service.getCerereService().getRequests_receivedByUser(connected_user.getId());
        List<CerereDTO> received_requestList = StreamSupport
                .stream(received_requests.spliterator(), false)
                .collect(Collectors.toList());
        return received_requestList;
    }

    public List<CerereDTO> getUsersSentRequests() {
        Iterable<CerereDTO> sent_requests = service.getCerereService().getRequests_sentByUser(connected_user.getId());
        List<CerereDTO> sent_requestList = StreamSupport
                .stream(sent_requests.spliterator(), false)
                .collect(Collectors.toList());
        return sent_requestList;
    }

}
