package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.*;

public class NotificareDTO {
    private Long id;
    private String description;
    private String type; //"event" or "message" or "request" or "friend"
    private Map<Long,String> status; //"seen" or "unseen"
    private Set<Utilizator> to = new HashSet<>();
    private LocalDateTime date_time;
    private String type_id;

    public NotificareDTO(Long id, String description, String type, Map<Long,String> status, Set<Utilizator> to, LocalDateTime date_time, String type_id) {
        this.id = id;
        this.description = description;
        this.type = type;
        this.status = status;
        this.to = to;
        this.date_time = date_time;
        this.type_id = type_id;
    }

    public List<String> getTo_emails() {
        List<String> participants_emails = new ArrayList<>();
        for (Utilizator user : to) {
            participants_emails.add(user.getEmail());
        }
        return participants_emails;
    }

    public List<Long> getTo_ids() {
        List<Long> participants_ids = new ArrayList<>();
        for (Utilizator user : to) {
            participants_ids.add(user.getId());
        }
        return participants_ids;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<Long,String> getStatus() {
        return status;
    }

    public void setStatus(Map<Long,String> status) {
        this.status = status;
    }

    public Set<Utilizator> getTo() {
        return to;
    }

    public void setTo(Set<Utilizator> to) {
        this.to = to;
    }

    public LocalDateTime getDate_time() {
        return date_time;
    }

    public void setDate_time(LocalDateTime date_time) {
        this.date_time = date_time;
    }

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    @Override
    public String toString() {
        return this.getDescription() + " | " + this.getType() + " | " + this.getStatus() + " | " + this.getDate_time().format(Constants.DATE_TIME_FORMATTER) + " | " + this.getTo_emails().toString() + " | " + this.getType_id();
    }
}
