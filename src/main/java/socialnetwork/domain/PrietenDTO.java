package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.time.Month;

public class PrietenDTO {
    private Utilizator user;
    private LocalDateTime date;

    public PrietenDTO(Utilizator user, LocalDateTime date) {
        this.user = user;
        this.date = date;
    }

    public Utilizator getUser() {
        return this.user;
    }

    public String getFirstName() {
        return this.user.getFirstName();
    }

    public String getLastName() {
        return this.user.getLastName();
    }

    public String getEmail() {
        return this.user.getEmail();
    }

    public String getPassword() {
        return this.user.getPassword();
    }

    public LocalDateTime getDate() {
        return this.date;
    }

    @Override
    public String toString() {
        return this.getFirstName() + " | " + this.getLastName() + " | " + this.getEmail() + " | " + this.getDate().format(Constants.DATE_TIME_FORMATTER);
    }
}
