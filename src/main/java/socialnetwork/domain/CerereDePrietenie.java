package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.Objects;

public class CerereDePrietenie extends Entity<Tuple<Long,Long>> {
    private String state;
    private LocalDateTime date;

    public CerereDePrietenie() {
        this.date = LocalDateTime.now();
    }

    public CerereDePrietenie(Long first_id, Long second_id, String state, LocalDateTime date) {
        this.setId(new Tuple<Long,Long>(first_id, second_id));
        this.state = state;
        this.date = date;
    }

    public CerereDePrietenie(Long first_id, Long second_id) {
        this.setId(new Tuple<Long,Long>(first_id, second_id));
        this.state = "pending";
        this.date = LocalDateTime.now();
    }

    public String getState() {
        return this.state;
    }

    public LocalDateTime getDate() {
        return this.date;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "Cerere de prietenie{" +
                "id=" + getId() +
                ", first_user='" + getId().getLeft() + '\'' +
                ", second_user='" + getId().getRight() + '\'' +
                ", state=" + getState() +
                ", date=" + getDate().format(Constants.DATE_TIME_FORMATTER) +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(getState(), getDate());
    }
}
