package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

public class Notificare extends Entity<Long> {
    private String description;
    private String type; //"event" or "message" or "request" or "friend"
    private Map<Long,String> status = new HashMap<>(); //"seen" or "unseen"
    private Set<Long> to = new HashSet<>();
    private LocalDateTime date_time;
    private String type_id;

    public Notificare(String description, String type, Map<Long,String> status, Set<Long> to, LocalDateTime date_time, String type_id) {
        this.setId(this.generateRandomId());
        this.description = description;
        this.type = type;
        this.status = status;
        this.to = to;
        this.date_time = date_time;
        this.type_id = type_id;
    }

    public Notificare(Long id, String description, String type, Map<Long,String> status, Set<Long> to, LocalDateTime date_time, String type_id) {
        this.setId(id);
        this.description = description;
        this.type = type;
        this.status = status;
        this.to = to;
        this.date_time = date_time;
        this.type_id = type_id;
    }

    private Long generateRandomId() {
        long leftLimit = 1L;
        long rightLimit = 1000000000L;
        long generatedLong = leftLimit + (long) (Math.random() * (rightLimit - leftLimit));
        return generatedLong;
    }

    public void addUser_toUsers(Long id) {
        this.to.add(id);
    }

    public void removeUser_fromUsers(Long id) {
        this.to.remove(id);
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<Long,String> getStatus() {
        return status;
    }

    public void setStatus(Map<Long,String> status) {
        this.status = status;
    }

    public void setStatus_forKey(Long key, String value) {
        this.status.put(key, value);
    }

    public Set<Long> getTo() {
        return to;
    }

    public void setTo(Set<Long> to) {
        this.to = to;
    }

    public LocalDateTime getDate_time() {
        return date_time;
    }

    public void setDate_time(LocalDateTime date_time) {
        this.date_time = date_time;
    }

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    @Override
    public String toString() {
        return "Eveniment{" +
                "id=" + getId() +
                ", descriere='" + getDescription() + '\'' +
                ", tip='" + getType() + '\'' +
                ", id-ul obiectului cu care face legatura notificarea='" + getType_id() + '\'' +
                ", status='" + getStatus() + '\'' +
                ", catre=" + getTo().toString() +
                ", date=" + getDate_time().format(Constants.DATE_TIME_FORMATTER) +
                '}';
    }
}
