package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDateTime;

public class CerereDTO {
    private Utilizator user;
    private String state;
    private LocalDateTime date;

    public CerereDTO(Utilizator user, String state, LocalDateTime date) {
        this.user = user;
        this.state = state;
        this.date = date;
    }

    public Utilizator getUser() {
        return this.user;
    }

    public String getFirstName() {
        return this.user.getFirstName();
    }

    public String getLastName() {
        return this.user.getLastName();
    }

    public String getEmail() {
        return this.user.getEmail();
    }

    public String getPassword() {
        return this.user.getPassword();
    }

    public LocalDateTime getDate() {
        return this.date;
    }

    public String getState() {
        return this.state;
    }

    @Override
    public String toString() {
        return this.getFirstName() + " | " + this.getLastName() + " | " + this.getState() + " | " + this.getDate().format(Constants.DATE_TIME_FORMATTER);
    }
}
