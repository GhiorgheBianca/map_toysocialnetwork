package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MesajDTO {
    private Long id_message;
    private Utilizator from;
    private List<Utilizator> to;
    private String message;
    private LocalDateTime date;
    private Long id_reply = null;

    public MesajDTO(Long id_message, Utilizator from, List<Utilizator> to, String message, LocalDateTime date) {
        this.id_message = id_message;
        this.from = from;
        this.to = to;
        this.message = message;
        this.date = date;
    }

    public MesajDTO(Long id_message, Utilizator from, List<Utilizator> to, String message, LocalDateTime date, Long id_reply) {
        this.id_message = id_message;
        this.from = from;
        this.to = to;
        this.message = message;
        this.date = date;
        this.id_reply = id_reply;
    }

    public Long getId() {
        return this.id_message;
    }

    public String getFrom() {
        return this.from.getEmail();
    }

    public Long getFrom_id() {
        return this.from.getId();
    }

    public List<String> getTo() {
        List<String> to_emails = new ArrayList<>();
        for (Utilizator user : to) {
            to_emails.add(user.getEmail());
        }
        return to_emails;
    }

    public List<Long> getTo_ids() {
        List<Long> to_ids = new ArrayList<>();
        for (Utilizator user : to) {
            to_ids.add(user.getId());
        }
        return to_ids;
    }

    public String getMessage() {
        return this.message;
    }

    public LocalDateTime getDate() {
        return this.date;
    }

    public Long getId_reply() {
        return this.id_reply;
    }

    public String getFirstName_from() {
        return this.from.getFirstName();
    }
    public String getLastName_from() {
        return this.from.getLastName();
    }

    @Override
    public String toString() {
        if (this.getId_reply() == null) {
            return this.getId() + " | " + this.getFrom() + " | " + this.getTo() + " | " + this.getMessage() + " | " + this.getDate().format(Constants.DATE_TIME_FORMATTER);
        }
        return this.getId() + " | " + this.getFrom() + " | " + this.getTo() + " | " + this.getMessage() + " | " + this.getDate().format(Constants.DATE_TIME_FORMATTER) + " | " + this.getId_reply();
    }
}
