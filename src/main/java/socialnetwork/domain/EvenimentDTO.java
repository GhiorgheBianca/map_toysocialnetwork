package socialnetwork.domain;

import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EvenimentDTO {
    private Long id;
    private String name;
    private String description;
    private String type; //"public" or "private"
    private LocalDateTime start_date;
    private LocalDateTime final_date;
    private Set<Utilizator> participants = new HashSet<>();

    public EvenimentDTO(Long id, String name, String description, String type, LocalDateTime start_date, LocalDateTime final_date, Set<Utilizator> participants) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.type = type;
        this.start_date = start_date;
        this.final_date = final_date;
        this.participants = participants;
    }

    public List<String> getParticipants_emails() {
        List<String> participants_emails = new ArrayList<>();
        for (Utilizator user : participants) {
            participants_emails.add(user.getEmail());
        }
        return participants_emails;
    }

    public List<Long> getParticipants_ids() {
        List<Long> participants_ids = new ArrayList<>();
        for (Utilizator user : participants) {
            participants_ids.add(user.getId());
        }
        return participants_ids;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LocalDateTime getStart_date() {
        return start_date;
    }

    public void setStart_date(LocalDateTime start_date) {
        this.start_date = start_date;
    }

    public LocalDateTime getFinal_date() {
        return final_date;
    }

    public void setFinal_date(LocalDateTime final_date) {
        this.final_date = final_date;
    }

    public Set<Utilizator> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<Utilizator> participants) {
        this.participants = participants;
    }

    @Override
    public String toString() {
        return this.getName() + " | " + this.getDescription() + " | " + this.getType() + " | " + this.getStart_date().format(Constants.DATE_TIME_FORMATTER) + " | " + this.getFinal_date().format(Constants.DATE_TIME_FORMATTER) + " | " + this.getParticipants_emails().toString();
    }
}
