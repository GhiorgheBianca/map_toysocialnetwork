package socialnetwork.domain.validators;

import socialnetwork.domain.Eveniment;
import socialnetwork.domain.Mesaj;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class EvenimentValidator implements Validator<Eveniment> {
    private final Repository<Long, Utilizator> repoUtilizator;
    public EvenimentValidator(Repository<Long, Utilizator> repo) {
        this.repoUtilizator = repo;
    }

    @Override
    public void validate(Eveniment entity) throws ValidationException {
        String errMsg = "";
        if (entity.getId() == null || entity.getId() < 0) {
            errMsg += "Id error!\n";
        }

        if (entity.getName() == null || entity.getName().equals("")) {
            errMsg += "Name error!\n";
        }

        if (entity.getDescription() == null || entity.getDescription().equals("")) {
            errMsg += "Description error!\n";
        }

        //"public" or "private"
        if (entity.getType() == null || entity.getType().equals("") || (!entity.getType().equals("public") && !entity.getType().equals("private"))) {
            errMsg += "Type error! It must be \"public\" or \"private\"!\n";
        }

        if (entity.getParticipants().size() == 0) {
            errMsg += "Nu exista niciun participant!\n";
        }

        Set<Long> participanti = entity.getParticipants();
        for (Long id : participanti) {
            if (id == null || id < 0) {
                errMsg += "To id error: " + id + "!\n";
            }
            if (repoUtilizator.findOne(id) == null) {
                errMsg += "Nu exista niciun id corespunzator unui utilizator care participa la eveniment!\n";
            }
        }

        if (entity.getStart_date() == null) {
            errMsg += "Start date error!\n";
        }

        if (entity.getFinal_date() == null) {
            errMsg += "Final date error!\n";
        }

        if (entity.getStart_date().isAfter(entity.getFinal_date())) {
            errMsg += "Start date is too late compared to final date!\n";
        }

        if (!errMsg.equals("")) {
            throw new ValidationException(errMsg);
        }
    }
}
