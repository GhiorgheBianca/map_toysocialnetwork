package socialnetwork.domain.validators;

import socialnetwork.domain.CerereDePrietenie;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;

public class CerereValidator implements Validator<CerereDePrietenie> {
    private Repository<Long, Utilizator> repoUtilizator;
    private Repository<Tuple<Long,Long>, Prietenie> repoPrietenie;
    private Repository<Tuple<Long,Long>, CerereDePrietenie> repoCerere;

    public CerereValidator(Repository<Long,Utilizator> repoUtilizator, Repository<Tuple<Long,Long>,Prietenie> repoPrietenie) {
        this.repoUtilizator = repoUtilizator;
        this.repoPrietenie = repoPrietenie;
    }

    public void setRepoCerere(Repository<Tuple<Long,Long>, CerereDePrietenie> repoCerere) {
        this.repoCerere = repoCerere;
    }

    @Override
    public void validate(CerereDePrietenie entity) throws ValidationException {
        String errMsg = "";
        if (entity.getId() == null || entity.getId().getLeft() < 0 || entity.getId().getRight() < 0) {
            errMsg += "Id error!\n";
        }
        if (!entity.getState().equals("approved") && !entity.getState().equals("pending") && !entity.getState().equals("rejected")) {
            errMsg += "Statusul trebuie sa fie obligatoriu pending, approved sau rejected!\n";
        }

        if (repoUtilizator.findOne(entity.getId().getLeft()) == null) {
            errMsg += "Nu exista nici un utilizator cu id-ul persoanei care trimite cererea!\n";
        }
        if (repoUtilizator.findOne(entity.getId().getRight()) == null) {
            errMsg += "Nu exista nici un utilizator cu id-ul persoanei care primeste cererea!\n";
        }

        if (entity.getId().getLeft().equals(entity.getId().getRight())) {
            errMsg += "Un utilizator nu isi poate trimite cerere de prietenie sie insusi!\n";
        }

        if (repoPrietenie.findOne(entity.getId()) != null && entity.getState().equals("approved")) {
            errMsg += "Utilizatorii sunt deja prieteni!\n";
        }
        Tuple<Long,Long> id_invers = new Tuple<>(entity.getId().getRight(), entity.getId().getLeft());
        if (repoPrietenie.findOne(id_invers) != null && entity.getState().equals("approved")) {
            errMsg += "Utilizatorii sunt deja prieteni!\n";
        }

        CerereDePrietenie cerere_IDinvers = repoCerere.findOne(id_invers);
        if (cerere_IDinvers != null && cerere_IDinvers.getState().equals("pending")) {
            errMsg += "Eroare la cererea de prietenie!\n";
        }

        if (!errMsg.equals("")) {
            throw new ValidationException(errMsg);
        }
    }
}
