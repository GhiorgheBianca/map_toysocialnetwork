package socialnetwork.domain.validators;

import socialnetwork.domain.Utilizator;

public class UtilizatorValidator implements Validator<Utilizator> {
    @Override
    public void validate(Utilizator entity) throws ValidationException {
        String errMsg = "";
        if (entity.getId() == null || entity.getId() < 0) {
            errMsg += "Id error!\n";
        }
        if (entity.getFirstName() == null || "".equals(entity.getFirstName())) {
            errMsg += "First name error!\n";
        }
        if (entity.getLastName() == null || "".equals(entity.getLastName())) {
            errMsg += "Last name error!\n";
        }
        if (entity.getEmail() == null || "".equals(entity.getEmail())) {
            errMsg += "Last name error!\n";
        }
        if (entity.getPassword() == null || "".equals(entity.getPassword())) {
            errMsg += "Last name error!\n";
        }

        if (!errMsg.equals("")) {
            throw new ValidationException(errMsg);
        }
    }
}
