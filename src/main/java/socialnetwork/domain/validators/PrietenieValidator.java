package socialnetwork.domain.validators;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;

public class PrietenieValidator implements Validator<Prietenie> {
    private Repository<Long, Utilizator> repoUtilizator;
    private Repository<Tuple<Long,Long>, Prietenie> repoPrietenie = null;

    public PrietenieValidator(Repository<Long, Utilizator> repoUtilizator) {
        this.repoUtilizator = repoUtilizator;
    }

    public void setRepoPrietenie(Repository<Tuple<Long,Long>, Prietenie> repoPrietenie) {
        this.repoPrietenie = repoPrietenie;
    }

    @Override
    public void validate(Prietenie entity) throws ValidationException {
        String errMsg = "";
        if (entity.getId() == null || entity.getId().getLeft() < 0 || entity.getId().getRight() < 0) {
            errMsg += "Id error!\n";
        }

        if (repoUtilizator.findOne(entity.getId().getLeft()) == null) {
            errMsg += "Nu exista niciun utilizator cu id-ul primei persoane din prietenie!\n";
        }
        if (repoUtilizator.findOne(entity.getId().getRight()) == null) {
            errMsg += "Nu exista niciun utilizator cu id-ul celei de-a doua persoane din prietenie!\n";
        }

        if (entity.getId().getLeft().equals(entity.getId().getRight())) {
            errMsg += "Un utilizator nu poate fi prieten cu el insusi!\n";
        }

        Tuple<Long,Long> id_invers = new Tuple<>(entity.getId().getRight(), entity.getId().getLeft());
        if (repoPrietenie.findOne(id_invers) != null) {
            errMsg += "Utilizatorii sunt deja prieteni!\n";
        }

        if (!errMsg.equals("")) {
            throw new ValidationException(errMsg);
        }
    }
}
