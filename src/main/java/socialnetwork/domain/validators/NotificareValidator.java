package socialnetwork.domain.validators;

import socialnetwork.domain.Eveniment;
import socialnetwork.domain.Notificare;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;

import java.util.Set;

public class NotificareValidator implements Validator<Notificare> {
    private final Repository<Long, Utilizator> repoUtilizator;
    public NotificareValidator(Repository<Long, Utilizator> repo) {
        this.repoUtilizator = repo;
    }

    @Override
    public void validate(Notificare entity) throws ValidationException {
        String errMsg = "";
        if (entity.getId() == null || entity.getId() < 0) {
            errMsg += "Id error!\n";
        }

        if (entity.getDescription() == null || entity.getDescription().equals("")) {
            errMsg += "Description error!\n";
        }

        //"event" or "message" or "request" or "friend"
        if (entity.getType() == null || entity.getType().equals("") || (!entity.getType().equals("event") && !entity.getType().equals("message") && !entity.getType().equals("request") && !entity.getType().equals("friend"))) {
            errMsg += "Type error! It must be \"event\" or \"message\" or \"request\" or \"friend\"!\n";
        }

        for (Long ID : entity.getTo()) {
            //"seen" or "unseen"
            if (entity.getStatus().get(ID) == null || entity.getStatus().get(ID).equals("") || (!entity.getStatus().get(ID).equals("seen") && !entity.getStatus().get(ID).equals("unseen"))) {
                errMsg += "Status error! It must be \"seen\" or \"unseen\"!\n";
            }
        }

        if (entity.getTo().size() == 0) {
            errMsg += "Nu exista niciun destinatar!\n";
        }

        Set<Long> to = entity.getTo();
        for (Long id : to) {
            if (id == null || id < 0) {
                errMsg += "To id error: " + id + "!\n";
            }
            if (repoUtilizator.findOne(id) == null) {
                errMsg += "Nu exista niciun id corespunzator unui utilizator caruia i se adreseaza notificarea!\n";
            }
        }

        if (!errMsg.equals("")) {
            throw new ValidationException(errMsg);
        }
    }
}
