package socialnetwork.domain.validators;

import socialnetwork.domain.Mesaj;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;

public class MesajValidator implements Validator<Mesaj> {
    private Repository<Long, Utilizator> repoUtilizator;
    public MesajValidator(Repository<Long, Utilizator> repo) {
        this.repoUtilizator = repo;
    }

    @Override
    public void validate(Mesaj entity) throws ValidationException {
        String errMsg = "";
        if (entity.getId() == null || entity.getId() < 0) {
            errMsg += "Id error!\n";
        }

        if (entity.getFrom() == null || entity.getFrom() < 0) {
            errMsg += "From id error!\n";
        }

        if (entity.getTo().size() == 0) {
            errMsg += "Nu exista niciun destinatar!\n";
        }

        for (Long id : entity.getTo()) {
            if (id == null || id < 0) {
                errMsg += "To id error: " + id + "!\n";
            }
            if (repoUtilizator.findOne(id) == null) {
                errMsg += "Nu exista niciun id corespunzator unui utilizator caruia i se adreseaza mesajul!\n";
            }
        }

        if (entity.getMessage() == null || "".equals(entity.getMessage())) {
            errMsg += "Message text error!\n";
        }

        if (entity.getId_reply() != null) {
            if (entity.getId_reply() < 0) {
                errMsg += "Reply id error!\n";
            }
        }

        if (repoUtilizator.findOne(entity.getFrom()) == null) {
            errMsg += "Nu exista niciun utilizator cu id-ul persoanei care trimite mesajul!\n";
        }

        if (!errMsg.equals("")) {
            throw new ValidationException(errMsg);
        }
    }
}
