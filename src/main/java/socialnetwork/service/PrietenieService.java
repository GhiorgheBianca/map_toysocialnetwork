package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.repository.RepoException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.*;
import socialnetwork.utils.Constants;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.FriendRequestChangeEvent;
import socialnetwork.utils.events.FriendshipChangeEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class PrietenieService extends SuperService implements Observable<FriendshipChangeEvent> {
    //private Repository<Tuple<Long,Long>, Prietenie> repoPrietenie;
    private PagingRepository<Tuple<Long,Long>, Prietenie> repoPrietenie;
    private CerereService cerereService;

    //public PrietenieService(Repository<Tuple<Long,Long>,Prietenie> repoPrietenie) {
    //    this.repoPrietenie = repoPrietenie;
    //}

    public PrietenieService(PagingRepository<Tuple<Long,Long>,Prietenie> repoPrietenie) {
        this.repoPrietenie = repoPrietenie;
    }

    public Repository<Tuple<Long,Long>, Prietenie> getRepoPrietenie() {
        return this.repoPrietenie;
    }
    public void setCerereService(CerereService cerereService) {
        this.cerereService = cerereService;
    }

    private List<Observer<FriendshipChangeEvent>> observers = new ArrayList<>();

    @Override
    public void addObserver(Observer<FriendshipChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<FriendshipChangeEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(FriendshipChangeEvent t) {
        observers.stream()
                .forEach(x -> x.update(t));
    }


    ////OPERATIILE PENTRU PRIETENII
    public Prietenie addFriendship(Prietenie given_friendship) {
        Prietenie friendship = repoPrietenie.save(given_friendship);

        if (friendship == null) {
            notifyObservers(new FriendshipChangeEvent(ChangeEventType.ADD, friendship));
        }

        return friendship;
    }

    public Prietenie deleteFriendship(Tuple<Long, Long> id) {
        Prietenie friendship = new Prietenie();
        try {
            friendship = repoPrietenie.delete(id, "");
        }
        catch (RepoException e) {
            if (e.getMessage().equals("Entitatea cu ID-ul dat nu exista!\n")) {
                Tuple<Long,Long> id_invers = new Tuple<>(id.getRight(), id.getLeft());
                friendship = repoPrietenie.delete(id_invers, "");
            }
        }

        if (friendship != null) {
            notifyObservers(new FriendshipChangeEvent(ChangeEventType.DELETE, friendship));
        }

        if (friendship != null) {
            CerereDePrietenie request = cerereService.deleteRequest(id);
        }

        return friendship;
    }

    public Iterable<Prietenie> getAllFriendships(){
        return repoPrietenie.findAll();
    }

    public int nrPrietenii() {
        return repoPrietenie.size("all");
    }

    public void printFriendships() {
        this.getAllFriendships().forEach(System.out::println);
    }

    public Prietenie findFriendship_id(Long first_id, Long second_id) {
        Prietenie friendship_found = repoPrietenie.findOne(new Tuple<>(first_id, second_id));
        if (friendship_found == null) {
            throw new ServiceException("Nu exista nicio prietenie cu id-ul mentionat!");
        }
        return friendship_found;
    }
    //////


    ////PAGINARE
    private int page = 0;
    private int size = Constants.NUMBER_OF_RECORDS_ON_PAGE;

    private Pageable pageable;

    public void setPageSize(int size) {
        this.size = size;
    }

    //public void setPageable(Pageable pageable) {
    //    this.pageable = pageable;
    //}

    public List<Prietenie> getNextFriendship(Long id) {
        this.page++;
        return getFriendshipsOnPage(this.page, id);
    }

    public List<Prietenie> getFriendshipsOnPage(int page, Long id) {
        this.page = page;
        Pageable pageable = new PageableImplementation(page, this.size);
        Page_version2<Prietenie> friendshipPage = repoPrietenie.findAll(pageable, id, "all");
        return friendshipPage.getContent().collect(Collectors.toList());
    }
    //////

    public int number_of_friends(Long id) {
        /*int count = 0;
        Iterable<Prietenie> prietenii = prietenieService.getAllFriendships();
        for (Prietenie prietenie : prietenii) {
            if (prietenie.getId().getLeft().equals(id) || prietenie.getId().getRight().equals(id)) {
                count++;
            }
        }*/
        return repoPrietenie.size("friendsWith_" + id.toString());
    }
}
