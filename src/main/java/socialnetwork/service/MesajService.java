package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.*;
import socialnetwork.utils.Constants;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.FriendRequestChangeEvent;
import socialnetwork.utils.events.MessageChangeEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class MesajService extends SuperService implements Observable<MessageChangeEvent> {
    //private Repository<Long, Mesaj> repoMesaj;
    private PagingRepository<Long, Mesaj> repoMesaj;

    private UtilizatorService utilizatorService;

    //public MesajService(Repository<Long, Mesaj> repoMesaj) {
    //    this.repoMesaj = repoMesaj;
    //}

    public MesajService(PagingRepository<Long, Mesaj> repoMesaj) {
        this.repoMesaj = repoMesaj;
    }

    public Repository<Long, Mesaj> getRepoMesaj() {
        return this.repoMesaj;
    }

    public void setUtilizatorService(UtilizatorService utilizatorService) {
        this.utilizatorService = utilizatorService;
    }

    private List<Observer<MessageChangeEvent>> observers = new ArrayList<>();

    @Override
    public void addObserver(Observer<MessageChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<MessageChangeEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(MessageChangeEvent t) {
        observers.stream()
                .forEach(x -> x.update(t));
    }


    ////OPERATIILE PENTRU MESAJE
    public Iterable<Mesaj> getAllMessages() {
        return repoMesaj.findAll();
    }

    public void sendNewMessage(Long from, List<Long> to, String message) {
        Mesaj mesaj = new Mesaj(from, to, message);

        Mesaj respond_save = repoMesaj.save(mesaj);
        if (respond_save == null) {
            notifyObservers(new MessageChangeEvent(ChangeEventType.ADD, mesaj));
        }
    }

    public void respondMessage(Long id_user, Long id_message, String text) {
        Mesaj message = this.findMessage_id(id_message);

        List<Long> to = new ArrayList<>();
        to.add(message.getFrom());

        if (message.getTo().contains(id_user)) {
            Mesaj reply_message = new Mesaj(id_user, to, text, id_message);

            if (repoMesaj.save(reply_message) == null) {
                notifyObservers(new MessageChangeEvent(ChangeEventType.ADD, reply_message));
            }
        }
        else {
            throw new ServiceException("Nu aveti niciun mesaj cu id-ul respectiv!");
        }
    }

    public void respondAllMessage(Long id_user, Long id_message, String text) {
        Mesaj message = this.findMessage_id(id_message);

        List<Long> to = new ArrayList<>();
        int aparitii_user = 0;
        for (Long to_user : message.getTo()) {
            //adaug numai utilizatorii care inca mai exista
            if (!to_user.equals(id_user) && utilizatorService.findUser_id(to_user) != null) {
                to.add(to_user);
            }
            else if (to_user.equals(id_user)) {
                aparitii_user++;
            }
        }
        //adaug numai utilizatorii care inca mai exista
        if (utilizatorService.findUser_id(message.getFrom()) != null) {
            to.add(message.getFrom());
        }

        if (aparitii_user != 0) {
            Mesaj reply_message = new Mesaj(id_user, to, text, id_message);

            if (repoMesaj.save(reply_message) == null) {
                notifyObservers(new MessageChangeEvent(ChangeEventType.ADD, reply_message));
            }
        }
        else if (to.size() == 0) {
            throw new ServiceException("Mesajul nu este trimis niciunui utilizator!");
        }
        else {
            throw new ServiceException("Nu aveti niciun mesaj cu id-ul respectiv!");
        }
    }

    public List<Mesaj> getConversation(Long id_user, Long id_receiver) {
        List<Mesaj> mesaje = new ArrayList<>();

        for (Mesaj mess : this.getAllMessages()) {
            if (mess.getFrom().equals(id_user) && mess.getTo().contains(id_receiver)) {
                mesaje.add(mess);
            }
            if (mess.getTo().contains(id_user) && mess.getFrom().equals(id_receiver)) {
                mesaje.add(mess);
            }
        }

        Collections.sort(mesaje, new Comparator<Mesaj>() {
            @Override
            public int compare(Mesaj m1, Mesaj m2) {
                return  m1.getDate().compareTo(m2.getDate());
            }
        });

        return mesaje;
    }

    public Mesaj findMessage_id(Long id) {
        Mesaj message_found = repoMesaj.findOne(id);
        if (message_found == null) {
            throw new ServiceException("Nu exista niciun mesaj cu id-ul mentionat!");
        }
        return message_found;
    }

    public List<MesajDTO> getMessages_sentByUser(Long id) {
        List<MesajDTO> mesaje_primite = new ArrayList<>();

        for (Mesaj mesaj : repoMesaj.findAll()) {
            if (mesaj.getFrom().equals(id)) {
                List<Utilizator> to_users = new ArrayList<>();
                for (Long to_id : mesaj.getTo()) {
                    try {
                        Utilizator found_user = utilizatorService.findUser_id(to_id);
                        to_users.add(utilizatorService.findUser_id(to_id));
                    }
                    catch (Exception ex) {
                        Utilizator utilizator_inexistent = new Utilizator(0L, "Utilizator", "Inexistent", "nouser@mail.com", "nopassword");
                        to_users.add(utilizator_inexistent);
                    }
                }

                MesajDTO mesaj_dto;
                try {
                    Utilizator found_user = utilizatorService.findUser_id(mesaj.getFrom());
                    mesaj_dto = new MesajDTO(mesaj.getId(), found_user, to_users, mesaj.getMessage(), mesaj.getDate(), mesaj.getId_reply());
                }
                catch (Exception ex) {
                    Utilizator utilizator_inexistent = new Utilizator(0L, "Utilizator", "Inexistent", "nouser@mail.com", "nopassword");
                    mesaj_dto = new MesajDTO(mesaj.getId(), utilizator_inexistent, to_users, mesaj.getMessage(), mesaj.getDate(), mesaj.getId_reply());
                }

                mesaje_primite.add(mesaj_dto);
            }
        }

        return mesaje_primite;
    }
    public List<MesajDTO> getMessages_receivedByUser(Long id) {
        List<MesajDTO> mesaje_primite = new ArrayList<>();

        for (Mesaj mesaj : repoMesaj.findAll()) {
            if (mesaj.getTo().contains(id)) {
                List<Utilizator> to_users = new ArrayList<>();
                for (Long to_id : mesaj.getTo()) {
                    try {
                        Utilizator found_user = utilizatorService.findUser_id(to_id);
                        to_users.add(utilizatorService.findUser_id(to_id));
                    }
                    catch (Exception ex) {
                        Utilizator utilizator_inexistent = new Utilizator(0L, "Utilizator", "Inexistent", "nouser@mail.com", "nopassword");
                        to_users.add(utilizator_inexistent);
                    }
                }

                MesajDTO mesaj_dto;
                try {
                    Utilizator found_user = utilizatorService.findUser_id(mesaj.getFrom());
                    mesaj_dto = new MesajDTO(mesaj.getId(), found_user, to_users, mesaj.getMessage(), mesaj.getDate(), mesaj.getId_reply());
                }
                catch (Exception ex) {
                    Utilizator utilizator_inexistent = new Utilizator(0L, "Utilizator", "Inexistent", "nouser@mail.com", "nopassword");
                    mesaj_dto = new MesajDTO(mesaj.getId(), utilizator_inexistent, to_users, mesaj.getMessage(), mesaj.getDate(), mesaj.getId_reply());
                }

                mesaje_primite.add(mesaj_dto);
            }
        }

        return mesaje_primite;
    }

    public List<MesajDTO> getConversation_betweenUsers(Long id_user, Long id_receiver) {
        List<MesajDTO> conversation = new ArrayList<>();

        for (Mesaj mesaj : this.getConversation(id_user, id_receiver)) {
            List<Utilizator> to_users = new ArrayList<>();
            for (Long to_id : mesaj.getTo()) {
                try {
                    Utilizator found_user = utilizatorService.findUser_id(to_id);
                    to_users.add(utilizatorService.findUser_id(to_id));
                }
                catch (Exception ex) {
                    Utilizator utilizator_inexistent = new Utilizator(0L, "Utilizator", "Inexistent", "nouser@mail.com", "nopassword");
                    to_users.add(utilizator_inexistent);
                }
            }

            MesajDTO mesaj_dto;
            try {
                Utilizator found_user = utilizatorService.findUser_id(mesaj.getFrom());
                mesaj_dto = new MesajDTO(mesaj.getId(), found_user, to_users, mesaj.getMessage(), mesaj.getDate(), mesaj.getId_reply());
            }
            catch (Exception ex) {
                Utilizator utilizator_inexistent = new Utilizator(0L, "Utilizator", "Inexistent", "nouser@mail.com", "nopassword");
                mesaj_dto = new MesajDTO(mesaj.getId(), utilizator_inexistent, to_users, mesaj.getMessage(), mesaj.getDate(), mesaj.getId_reply());
            }

            conversation.add(mesaj_dto);
        }

        return conversation;
    }

    public List<MesajDTO> getMessages_betweenDates(LocalDateTime start_date, LocalDateTime end_date, Long id_user) {
        List<MesajDTO> messages = new ArrayList<>();

        for (MesajDTO mesajDTO : this.getMessages_receivedByUser(id_user)) {
            if (mesajDTO.getDate().isAfter(start_date) && mesajDTO.getDate().isBefore(end_date)) {
                messages.add(mesajDTO);
            }
        }

        return messages;
    }
    public List<MesajDTO> getMessages_betweenDates_fromUser(LocalDateTime start_date, LocalDateTime end_date, Long id_receiver, Long id_sender) {
        List<MesajDTO> messages = new ArrayList<>();

        for (MesajDTO mesajDTO : this.getMessages_receivedByUser(id_receiver)) {
            if (mesajDTO.getDate().isAfter(start_date) && mesajDTO.getDate().isBefore(end_date) && mesajDTO.getFrom_id().equals(id_sender)) {
                messages.add(mesajDTO);
            }
        }

        return messages;
    }
    //////


    ////PAGINARE
    private int page = 0;
    private int size = Constants.NUMBER_OF_RECORDS_ON_PAGE;

    private Pageable pageable;

    public void setPageSize(int size) {
        this.size = size;
    }

    //public void setPageable(Pageable pageable) {
    //    this.pageable = pageable;
    //}

    public List<Mesaj> getNextMessage(Long id) {
        this.page++;
        return getMessagesOnPage(this.page, id);
    }

    public List<Mesaj> getMessagesOnPage(int page, Long id) {
        this.page = page;
        Pageable pageable = new PageableImplementation(page, this.size);
        Page_version2<Mesaj> messagePage = repoMesaj.findAll(pageable, id, "all");
        return messagePage.getContent().collect(Collectors.toList());
    }

    public List<Mesaj> getSentMessagesOnPage(int page, Long id) {
        this.page = page;
        Pageable pageable = new PageableImplementation(page, this.size);
        Page_version2<Mesaj> messagePage = repoMesaj.findAll(pageable, id, "sent");
        return messagePage.getContent().collect(Collectors.toList());
    }

    public List<Mesaj> getReceivedMessagesOnPage(int page, Long id) {
        this.page = page;
        Pageable pageable = new PageableImplementation(page, this.size);
        Page_version2<Mesaj> messagePage = repoMesaj.findAll(pageable, id, "received");
        return messagePage.getContent().collect(Collectors.toList());
    }

    public List<Mesaj> getBetweenUsersMessagesOnPage(int page, Long main_id, Long id) {
        this.page = page;
        Pageable pageable = new PageableImplementation(page, this.size);
        Page_version2<Mesaj> messagePage = repoMesaj.findAll(pageable, main_id, "messages_" + id.toString());
        return messagePage.getContent().collect(Collectors.toList());
    }
    //////

    public List<MesajDTO> getMesssages_SentByUser_PAGED(int page, Long id) {
        List<MesajDTO> result = new ArrayList<>();
        Iterable<Mesaj> mesaje = this.getSentMessagesOnPage(page, id);
        for (Mesaj mesaj : mesaje) {
            if (id.equals(mesaj.getFrom())) {
                List<Utilizator> to_users = new ArrayList<>();
                for (Long to_id : mesaj.getTo()) {
                    try {
                        Utilizator found_user = utilizatorService.findUser_id(to_id);
                        to_users.add(found_user);
                    }
                    catch (Exception ex) {
                        Utilizator utilizator_inexistent = new Utilizator(0L, "Utilizator", "Inexistent", "nouser@mail.com", "nopassword");
                        to_users.add(utilizator_inexistent);
                    }
                }

                MesajDTO mesajDTO = new MesajDTO(mesaj.getId(), utilizatorService.findUser_id(mesaj.getFrom()), to_users, mesaj.getMessage(), mesaj.getDate(), mesaj.getId_reply());
                result.add(mesajDTO);
            }
        }
        return result;
    }

    public List<MesajDTO> getMessages_ReceivedByUser_PAGED(int page, Long id) {
        List<MesajDTO> result = new ArrayList<>();
        Iterable<Mesaj> mesaje = this.getReceivedMessagesOnPage(page, id);
        for (Mesaj mesaj : mesaje) {
            if (mesaj.getTo().contains(id)) {
                List<Utilizator> to_users = new ArrayList<>();
                for (Long to_id : mesaj.getTo()) {
                    try {
                        Utilizator found_user = utilizatorService.findUser_id(to_id);
                        to_users.add(found_user);
                    }
                    catch (Exception ex) {
                        Utilizator utilizator_inexistent = new Utilizator(0L, "Utilizator", "Inexistent", "nouser@mail.com", "nopassword");
                        to_users.add(utilizator_inexistent);
                    }
                }

                MesajDTO mesajDTO;
                try {
                    Utilizator found_user = utilizatorService.findUser_id(mesaj.getFrom());
                    mesajDTO = new MesajDTO(mesaj.getId(), found_user, to_users, mesaj.getMessage(), mesaj.getDate(), mesaj.getId_reply());
                }
                catch (Exception ex) {
                    Utilizator utilizator_inexistent = new Utilizator(0L, "Utilizator", "Inexistent", "nouser@mail.com", "nopassword");
                    mesajDTO = new MesajDTO(mesaj.getId(), utilizator_inexistent, to_users, mesaj.getMessage(), mesaj.getDate(), mesaj.getId_reply());
                }
                result.add(mesajDTO);
            }
        }
        return result;
    }

    public List<MesajDTO> getMesssages_BetweenUsers_PAGED(int page, Long main_id, Long id) {
        List<MesajDTO> result = new ArrayList<>();
        Iterable<Mesaj> mesaje = this.getBetweenUsersMessagesOnPage(page, main_id, id);
        for (Mesaj mesaj : mesaje) {
            List<Utilizator> to_users = new ArrayList<>();
            for (Long to_id : mesaj.getTo()) {
                try {
                    Utilizator found_user = utilizatorService.findUser_id(to_id);
                    to_users.add(found_user);
                }
                catch (Exception ex) {
                    Utilizator utilizator_inexistent = new Utilizator(0L, "Utilizator", "Inexistent", "nouser@mail.com", "nopassword");
                    to_users.add(utilizator_inexistent);
                }
            }

            MesajDTO mesajDTO;
            try {
                Utilizator found_user = utilizatorService.findUser_id(mesaj.getFrom());
                mesajDTO = new MesajDTO(mesaj.getId(), found_user, to_users, mesaj.getMessage(), mesaj.getDate(), mesaj.getId_reply());
            }
            catch (Exception ex) {
                Utilizator utilizator_inexistent = new Utilizator(0L, "Utilizator", "Inexistent", "nouser@mail.com", "nopassword");
                mesajDTO = new MesajDTO(mesaj.getId(), utilizator_inexistent, to_users, mesaj.getMessage(), mesaj.getDate(), mesaj.getId_reply());
            }
            result.add(mesajDTO);
        }
        return result;
    }

    public int number_of_messages_Sent(Long id) {
        return repoMesaj.size("sent_" + id.toString());
    }

    public int number_of_messages_Received(Long id) {
        return repoMesaj.size("received_" + id.toString());
    }

    public int number_of_messages_BetweenUsers(Long main_id, Long id) {
        return repoMesaj.size("messages_" + main_id.toString() + "_" + id.toString());
    }
}
