package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Page_version2;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.repository.paging.PagingRepository;
import socialnetwork.utils.Constants;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.EventChangeEvent;
import socialnetwork.utils.events.FriendshipChangeEvent;
import socialnetwork.utils.events.MessageChangeEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class EvenimentService extends SuperService implements Observable<EventChangeEvent> {
    private PagingRepository<Long, Eveniment> repoEveniment;

    private UtilizatorService utilizatorService;
    private NotificareService notificareService;

    public EvenimentService(PagingRepository<Long, Eveniment> repoEveniment) {
        this.repoEveniment = repoEveniment;
    }

    public Repository<Long, Eveniment> getRepoEveniment() {
        return this.repoEveniment;
    }

    public void setUtilizatorService(UtilizatorService utilizatorService) {
        this.utilizatorService = utilizatorService;
    }
    public void setNotificareService(NotificareService notificareService) {
        this.notificareService = notificareService;
    }

    private List<Observer<EventChangeEvent>> observers = new ArrayList<>();

    @Override
    public void addObserver(Observer<EventChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<EventChangeEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(EventChangeEvent t) {
        observers.stream()
                .forEach(x -> x.update(t));
    }

    ////OPERATIILE PENTRU EVENIMENTE
    public Iterable<Eveniment> getAllEvents() {
        return repoEveniment.findAll();
    }

    public Eveniment addEvent(Long user_id, Long id, String name, String description, String type, LocalDateTime start_date, LocalDateTime final_date, Set<Long> participants_id) {
        Eveniment eveniment = new Eveniment(id, name, description, type, start_date, final_date, participants_id);

        Eveniment event_save = repoEveniment.save(eveniment);
        if (event_save == null) {
            notifyObservers(new EventChangeEvent(ChangeEventType.ADD, eveniment));
        }

        try {
            notificareService.addUser_toNotification(user_id, notificareService.findNotification_type_id(id.toString()).getId());
        }
        catch (ServiceException ex) {
            //nothing happens
        }

        return eveniment;
    }

    public Eveniment addNewEvent(String name, String description, String type, LocalDateTime start_date, LocalDateTime final_date, Set<Long> participants_id) {
        Eveniment eveniment = new Eveniment(name, description, type, start_date, final_date, participants_id);

        Eveniment event_save = repoEveniment.save(eveniment);
        if (event_save == null) {
            notifyObservers(new EventChangeEvent(ChangeEventType.ADD, eveniment));
        }

        String descriere = "V-ati abonat la evenimentul " + name + "!";
        Map<Long,String> status_map = new HashMap<>();
        for (Long participant : participants_id) {
            status_map.put(participant, "unseen");
        }
        notificareService.addNewNotification(descriere, "event", status_map, participants_id, LocalDateTime.now(), eveniment.getId().toString());

        return eveniment;
    }

    public Eveniment deleteEvent(Long event_id, Long user_id) {
        Eveniment event_delete = repoEveniment.delete(event_id, "");

        if (event_delete != null) {
            notifyObservers(new EventChangeEvent(ChangeEventType.DELETE, event_delete));
        }

        if (event_delete != null) {
            try {
                notificareService.removeUser_fromNotification(user_id, notificareService.findNotification_type_id(event_delete.getId().toString()).getId());
            }
            catch (ServiceException ex) {
                //nothing happens
            }
        }
        else {
            throw new ServiceException("ID-ul evenimentului nu poate fi null!");
        }

        return event_delete;
    }

    public void addUser_toEvent(Long user_id, Long event_id) {
        Eveniment eveniment = this.findEvent_id(event_id);
        eveniment.addUser_toParticipants(user_id);

        this.deleteEvent(event_id, user_id);
        this.addEvent(user_id, eveniment.getId(), eveniment.getName(), eveniment.getDescription(), eveniment.getType(), eveniment.getStart_date(), eveniment.getFinal_date(), eveniment.getParticipants());
    }

    public Eveniment removeUser_fromEvent(Long user_id, Long event_id) {
        Eveniment event_delete = repoEveniment.delete(event_id, user_id.toString());

        if (event_delete != null) {
            notifyObservers(new EventChangeEvent(ChangeEventType.DELETE, event_delete));
        }

        if (event_delete != null) {
            try {
                notificareService.removeUser_fromNotification(user_id, notificareService.findNotification_type_id(event_delete.getId().toString()).getId());
            }
            catch (ServiceException ex) {
                //nothing happens
            }
        }
        else {
            throw new ServiceException("ID-ul evenimentului nu poate fi null!");
        }

        return event_delete;

        //VARIANTA 2
        //Eveniment eveniment = this.findEvent_id(event_id);
        //eveniment.removeUser_fromParticipants(user_id);

        //this.deleteEvent(event_id);
        //this.addEvent(eveniment.getName(), eveniment.getDescription(), eveniment.getType(), eveniment.getStart_date(), eveniment.getFinal_date(), eveniment.getParticipants());
    }

    public Eveniment findEvent_id(Long id) {
        Eveniment event_found = repoEveniment.findOne(id);
        if (event_found == null) {
            throw new ServiceException("Nu exista niciun eveniment cu id-ul mentionat!");
        }
        return event_found;
    }
    //////


    ////PAGINARE
    private int page = 0;
    private int size = Constants.NUMBER_OF_RECORDS_ON_PAGE;

    private Pageable pageable;

    public void setPageSize(int size) {
        this.size = size;
    }

    //public void setPageable(Pageable pageable) {
    //    this.pageable = pageable;
    //}

    public List<Eveniment> getNextEvent(Long id) {
        this.page++;
        return getEventsOnPage(this.page, id);
    }

    public List<Eveniment> getEventsOnPage(int page, Long id) {
        this.page = page;
        Pageable pageable = new PageableImplementation(page, this.size);
        Page_version2<Eveniment> messagePage = repoEveniment.findAll(pageable, id, "all");
        return messagePage.getContent().collect(Collectors.toList());
    }

    public List<Eveniment> getUsersGoesToEventsOnPage(int page, Long id) {
        this.page = page;
        Pageable pageable = new PageableImplementation(page, this.size);
        Page_version2<Eveniment> eventPage = repoEveniment.findAll(pageable, id, "participates");
        return eventPage.getContent().collect(Collectors.toList());
    }

    public List<Eveniment> getNoParticipationEventsOnPage(int page, Long id) {
        this.page = page;
        Pageable pageable = new PageableImplementation(page, this.size);
        Page_version2<Eveniment> eventPage = repoEveniment.findAll(pageable, id, "doesn't participates");
        return eventPage.getContent().collect(Collectors.toList());
    }
    //////

    private List<EvenimentDTO> getEventDTOList(Iterable<Eveniment> events) {
        List<EvenimentDTO> result = new ArrayList<>();
        for (Eveniment event : events) {
            Set<Utilizator> to_users = new HashSet<>();
            for (Long to_id : event.getParticipants()) {
                try {
                    Utilizator found_user = utilizatorService.findUser_id(to_id);
                    to_users.add(found_user);
                }
                catch (Exception ex) {
                    Utilizator utilizator_inexistent = new Utilizator(0L, "Utilizator", "Inexistent", "nouser@mail.com", "nopassword");
                    to_users.add(utilizator_inexistent);
                }
            }

            EvenimentDTO evenimentDTO = new EvenimentDTO(event.getId(), event.getName(), event.getDescription(), event.getType(), event.getStart_date(), event.getFinal_date(), to_users);
            result.add(evenimentDTO);
        }
        return result;
    }

    public List<EvenimentDTO> getEvents_UserDoesntGoTo_PAGED(int page, Long id) {
        //List<EvenimentDTO> result = new ArrayList<>();
        Iterable<Eveniment> events = this.getNoParticipationEventsOnPage(page, id);
        return getEventDTOList(events);
    }

    public List<EvenimentDTO> getEvents_UserGoesTo_PAGED(int page, Long id) {
        //List<EvenimentDTO> result = new ArrayList<>();
        Iterable<Eveniment> events = this.getUsersGoesToEventsOnPage(page, id);
        return getEventDTOList(events);
    }

    public int number_of_events_goesTo(Long id) {
        /*int count = 0;
        Iterable<Eveniment> events = this.getAllEvents();
        for (Eveniment event : events) {
            if (event.getParticipants().contains(id)) {
                count++;
            }
        }*/
        return repoEveniment.size("goesTo_" + id.toString());
    }

    public int number_of_events_doesntGoTo(Long id) {
        /*int count = 0;
        Iterable<Eveniment> events = this.getAllEvents();
        for (Eveniment event : events) {
            if (!event.getParticipants().contains(id)) {
                count++;
            }
        }*/
        return repoEveniment.size("doesntGoTo_" + id.toString());
    }
}
