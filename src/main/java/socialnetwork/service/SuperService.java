package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.PagingRepository;

public class SuperService {
    private UtilizatorService utilizatorService;
    private PrietenieService prietenieService;
    private CerereService cerereService;
    private MesajService mesajService;
    private EvenimentService evenimentService;
    private NotificareService notificareService;

    public SuperService() {
    }

    public SuperService(PagingRepository<Long, Utilizator> repoUtilizator, PagingRepository<Tuple<Long,Long>, Prietenie> repoPrietenie, PagingRepository<Tuple<Long,Long>, CerereDePrietenie> repoCerere, PagingRepository<Long, Mesaj> repoMesaj, PagingRepository<Long, Eveniment> repoEveniment, PagingRepository<Long, Notificare> repoNotificare) {
        this.utilizatorService = new UtilizatorService(repoUtilizator);
        this.prietenieService = new PrietenieService(repoPrietenie);
        this.cerereService = new CerereService(repoCerere);
        this.mesajService = new MesajService(repoMesaj);
        this.evenimentService = new EvenimentService(repoEveniment);
        this.notificareService = new NotificareService(repoNotificare);
    }

    //public SuperService(Repository<Long, Utilizator> repoUtilizator, Repository<Tuple<Long,Long>, Prietenie> repoPrietenie, Repository<Tuple<Long,Long>, CerereDePrietenie> repoCerere, Repository<Long, Mesaj> repoMesaj) {
    //    this.utilizatorService = new UtilizatorService(repoUtilizator);
    //    this.prietenieService = new PrietenieService(repoPrietenie);
    //    this.cerereService = new CerereService(repoCerere);
    //    this.mesajService = new MesajService(repoMesaj);
    //}

    public SuperService(UtilizatorService utilizatorService, PrietenieService prietenieService, CerereService cerereService, MesajService mesajService) {
        this.utilizatorService = utilizatorService;
        this.prietenieService = prietenieService;
        this.cerereService = cerereService;
        this.mesajService = mesajService;
    }

    public UtilizatorService getUtilizatorService() {
        return this.utilizatorService;
    }
    public PrietenieService getPrietenieService() {
        return this.prietenieService;
    }
    public CerereService getCerereService() {
        return this.cerereService;
    }
    public MesajService getMesajService() {
        return this.mesajService;
    }
    public EvenimentService getEvenimentService() {
        return this.evenimentService;
    }
    public NotificareService getNotificareService() {
        return this.notificareService;
    }

}
