package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.RepoException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.*;
import socialnetwork.utils.Constants;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.FriendRequestChangeEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CerereService extends SuperService implements Observable<FriendRequestChangeEvent> {
    //private Repository<Tuple<Long,Long>, CerereDePrietenie> repoCerere;
    private PagingRepository<Tuple<Long,Long>, CerereDePrietenie> repoCerere;

    private UtilizatorService utilizatorService;
    private PrietenieService prietenieService;

    //public CerereService(Repository<Tuple<Long,Long>, CerereDePrietenie> repoCerere) {
    //    this.repoCerere = repoCerere;
    //}

    public CerereService(PagingRepository<Tuple<Long,Long>, CerereDePrietenie> repoCerere) {
        this.repoCerere = repoCerere;
    }

    public PagingRepository<Tuple<Long,Long>, CerereDePrietenie> getRepoCerere() {
        return this.repoCerere;
    }

    public void setUtilizatorService(UtilizatorService utilizatorService) {
        this.utilizatorService = utilizatorService;
    }
    public void setPrietenieService(PrietenieService prietenieService) {
        this.prietenieService = prietenieService;
    }

    private List<Observer<FriendRequestChangeEvent>> observers = new ArrayList<>();

    @Override
    public void addObserver(Observer<FriendRequestChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<FriendRequestChangeEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(FriendRequestChangeEvent t) {
        observers.stream()
                .forEach(x -> x.update(t));
    }


    ////OPERATIILE PENTRU CERERI DE PRIETENIE
    public Iterable<CerereDePrietenie> getAllRequests() {
        return repoCerere.findAll();
    }

    public CerereDePrietenie deleteRequest(Tuple<Long, Long> id) {
        CerereDePrietenie request = new CerereDePrietenie();
        try {
            request = repoCerere.delete(id, "");
        }
        catch (RepoException e) {
            if (e.getMessage().equals("Entitatea cu ID-ul dat nu exista!\n")) {
                Tuple<Long,Long> id_invers = new Tuple<>(id.getRight(), id.getLeft());
                request = repoCerere.delete(id_invers, "");
            }
        }

        if (request != null) {
            notifyObservers(new FriendRequestChangeEvent(ChangeEventType.DELETE, request));
        }

        return request;
    }

    public CerereDePrietenie sendRequest(Long first_id, Long second_id) {
        CerereDePrietenie request = new CerereDePrietenie(first_id, second_id);
        CerereDePrietenie saved = repoCerere.save(request);

        if (saved == null) {
            notifyObservers(new FriendRequestChangeEvent(ChangeEventType.ADD, request));
        }

        return saved;
    }

    public List<CerereDTO> getRequests_sentByUser(Long id) {
        List<CerereDTO> cereri_primite = new ArrayList<>();

        for (CerereDePrietenie cerere : repoCerere.findAll()) {
            if (cerere.getId().getLeft().equals(id)) {
                CerereDTO cerere_dto = new CerereDTO(utilizatorService.findUser_id(cerere.getId().getRight()), cerere.getState(), cerere.getDate());
                cereri_primite.add(cerere_dto);
            }
        }

        return cereri_primite;
    }
    public List<CerereDTO> getRequests_receivedByUser(Long id) {
        List<CerereDTO> cereri_primite = new ArrayList<>();

        for (CerereDePrietenie cerere : repoCerere.findAll()) {
            if (cerere.getId().getRight().equals(id)) {
                CerereDTO cerere_dto = new CerereDTO(utilizatorService.findUser_id(cerere.getId().getLeft()), cerere.getState(), cerere.getDate());
                cereri_primite.add(cerere_dto);
            }
        }

        return cereri_primite;
    }

    public void answerRequest(Long first_id, Long second_id, String raspuns) {
        CerereDePrietenie newRequest = new CerereDePrietenie(second_id, first_id, raspuns, LocalDateTime.now());
        CerereDePrietenie oldRequest = this.findRequest_id(first_id, second_id);
        if (repoCerere.update(newRequest, "") == null) {
            throw new ServiceException("Cererea nu va este adresata!");
        }

        if (raspuns.equals("approved")) {
            Prietenie prietenie = new Prietenie(newRequest.getId().getLeft(), newRequest.getId().getRight());
            try {
                prietenieService.addFriendship(prietenie);
            }
            catch (RepoException ex) {
                //nothing happens
            }
        }
        else if (raspuns.equals("rejected")) {
            prietenieService.deleteFriendship(new Tuple<>(first_id, second_id));
        }

        if (oldRequest != null) {
            notifyObservers(new FriendRequestChangeEvent(ChangeEventType.UPDATE, newRequest, oldRequest));
        }
    }

    public CerereDePrietenie findRequest_id(Long first_id, Long second_id) {
        CerereDePrietenie request_found = repoCerere.findOne(new Tuple<>(second_id, first_id));
        if (request_found == null) {
            throw new ServiceException("Nu exista nicio cerere cu id-ul mentionat!");
        }
        return request_found;
    }
    //////


    ////PAGINARE
    private int page = 0;
    private int size = Constants.NUMBER_OF_RECORDS_ON_PAGE;

    private Pageable pageable;

    public void setPageSize(int size) {
        this.size = size;
    }

    //public void setPageable(Pageable pageable) {
    //    this.pageable = pageable;
    //}

    public List<CerereDePrietenie> getNextRequest(Long id) {
        this.page++;
        return getRequestsOnPage(this.page, id);
    }

    public List<CerereDePrietenie> getRequestsOnPage(int page, Long id) {
        this.page = page;
        Pageable pageable = new PageableImplementation(page, this.size);
        Page_version2<CerereDePrietenie> requestPage = repoCerere.findAll(pageable, id, "all");
        return requestPage.getContent().collect(Collectors.toList());
    }

    public List<CerereDePrietenie> getSentRequestsOnPage(int page, Long id) {
        this.page = page;
        Pageable pageable = new PageableImplementation(page, this.size);
        Page_version2<CerereDePrietenie> requestPage = repoCerere.findAll(pageable, id, "sent");
        return requestPage.getContent().collect(Collectors.toList());
    }

    public List<CerereDePrietenie> getReceivedRequestsOnPage(int page, Long id) {
        this.page = page;
        Pageable pageable = new PageableImplementation(page, this.size);
        Page_version2<CerereDePrietenie> requestPage = repoCerere.findAll(pageable, id, "received");
        return requestPage.getContent().collect(Collectors.toList());
    }
    //////

    public List<CerereDTO> getRequests_SentByUser_PAGED(int page, Long id) {
        List<CerereDTO> result = new ArrayList<>();
        Iterable<CerereDePrietenie> cereri = this.getSentRequestsOnPage(page, id);
        for (CerereDePrietenie cerere : cereri) {
            if (id.equals(cerere.getId().getLeft())) {
                CerereDTO cerereDTO = new CerereDTO(utilizatorService.findUser_id(cerere.getId().getRight()), cerere.getState(), cerere.getDate());
                result.add(cerereDTO);
            }
        }
        return result;
    }

    public List<CerereDTO> getRequests_ReceivedByUser_PAGED(int page, Long id) {
        List<CerereDTO> result = new ArrayList<>();
        Iterable<CerereDePrietenie> cereri = this.getReceivedRequestsOnPage(page, id);
        for (CerereDePrietenie cerere : cereri) {
            if (id.equals(cerere.getId().getRight())) {
                CerereDTO cerereDTO = new CerereDTO(utilizatorService.findUser_id(cerere.getId().getLeft()), cerere.getState(), cerere.getDate());
                result.add(cerereDTO);
            }
        }
        return result;
    }

    public int number_of_requests_Sent(Long id) {
        /*int count = 0;
        Iterable<CerereDePrietenie> cereri = this.getAllRequests();
        for (CerereDePrietenie cerere : cereri) {
            if (cerere.getId().getLeft().equals(id)) {
                count++;
            }
        }*/
        return repoCerere.size("sent_" + id.toString());
    }

    public int number_of_requests_Received(Long id) {
        /*int count = 0;
        Iterable<CerereDePrietenie> cereri = this.getAllRequests();
        for (CerereDePrietenie cerere : cereri) {
            if (cerere.getId().getRight().equals(id)) {
                count++;
            }
        }*/
        return repoCerere.size("received_" + id.toString());
    }
}
