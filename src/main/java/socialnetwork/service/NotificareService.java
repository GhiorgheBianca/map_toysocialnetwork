package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Page_version2;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.repository.paging.PagingRepository;
import socialnetwork.utils.Constants;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.EventChangeEvent;
import socialnetwork.utils.events.FriendRequestChangeEvent;
import socialnetwork.utils.events.NotificationChangeEvent;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class NotificareService extends SuperService implements Observable<NotificationChangeEvent> {
    private PagingRepository<Long, Notificare> repoNotificare;

    private UtilizatorService utilizatorService;
    //private PrietenieService prietenieService;
    //private CerereService cerereService;
    //private MesajService mesajService;
    //private EvenimentService evenimentService;

    public NotificareService(PagingRepository<Long, Notificare> repoNotificare) {
        this.repoNotificare = repoNotificare;
    }

    public Repository<Long, Notificare> getRepoNotificare() {
        return this.repoNotificare;
    }

    public void setUtilizatorService(UtilizatorService utilizatorService) {
        this.utilizatorService = utilizatorService;
    }
    //public void setPrietenieService(PrietenieService prietenieService) {
    //    this.prietenieService = prietenieService;
    //}
    //public void setCerereService(CerereService cerereService) {
    //    this.cerereService = cerereService;
    //}
    //public void setMesajService(MesajService mesajService) {
    //    this.mesajService = mesajService;
    //}
    //public void setEvenimentService(EvenimentService evenimentService) {
    //    this.evenimentService = evenimentService;
    //}

    private List<Observer<NotificationChangeEvent>> observers = new ArrayList<>();

    @Override
    public void addObserver(Observer<NotificationChangeEvent> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<NotificationChangeEvent> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(NotificationChangeEvent t) {
        observers.stream()
                .forEach(x -> x.update(t));
    }

    ////OPERATIILE PENTRU NOTIFICARI
    public Iterable<Notificare> getAllNotifications() {
        return repoNotificare.findAll();
    }

    public boolean unseenNotifications_forUser(Long id) {
        for (Notificare notificare : this.getAllNotifications()) {
            if (notificare.getTo().contains(id) && notificare.getStatus().get(id).equals("unseen")) {
                return true;
            }
        }
        return false;
    }

    public Notificare addNewNotification(String description, String type, Map<Long,String> status, Set<Long> to_id, LocalDateTime date_time, String type_id) {
        Notificare notificare = new Notificare(description, type, status, to_id, date_time, type_id);

        Notificare notification_save = repoNotificare.save(notificare);
        if (notification_save == null) {
            notifyObservers(new NotificationChangeEvent(ChangeEventType.ADD, notificare));
        }

        return notificare;
    }

    public Notificare addNotification(Long id, String description, String type, Map<Long,String> status, Set<Long> to_id, LocalDateTime date_time, String type_id) {
        Notificare notificare = new Notificare(id, description, type, status, to_id, date_time, type_id);

        Notificare notification_save = repoNotificare.save(notificare);
        if (notification_save == null) {
            notifyObservers(new NotificationChangeEvent(ChangeEventType.ADD, notificare));
        }

        return notificare;
    }

    public Notificare deleteNotification(Long id) {
        Notificare notification_delete = repoNotificare.delete(id, "");

        if (notification_delete != null) {
            notifyObservers(new NotificationChangeEvent(ChangeEventType.DELETE, notification_delete));
        }

        return notification_delete;
    }

    public void addUser_toNotification(Long user_id, Long notification_id) {
        Notificare notificare = this.findNotification_id(notification_id);
        notificare.addUser_toUsers(user_id);
        notificare.setStatus_forKey(user_id, "unseen");

        this.deleteNotification(notification_id);
        this.addNotification(notificare.getId(), notificare.getDescription(), notificare.getType(), notificare.getStatus(), notificare.getTo(), notificare.getDate_time(), notificare.getType_id());
    }

    public Notificare removeUser_fromNotification(Long user_id, Long notification_id) {
        Notificare notification_delete = repoNotificare.delete(notification_id, user_id.toString());

        if (notification_delete != null) {
            notifyObservers(new NotificationChangeEvent(ChangeEventType.DELETE, notification_delete));
        }

        return notification_delete;

        //Notificare notificare = this.findNotification_id(notification_id);
        //notificare.removeUser_fromUsers(user_id);

        //this.deleteNotification(notification_id);
        //this.addNotification(notificare.getDescription(), notificare.getType(), notificare.getStatus(), notificare.getTo(), notificare.getDate_time(), notificare.getType_id());
    }

    public Notificare updateNotification(Notificare oldNotification, String description, Long user_id) {
        Notificare newNotification = new Notificare(oldNotification.getId(), description, oldNotification.getType(), oldNotification.getStatus(), oldNotification.getTo(), LocalDateTime.now(), oldNotification.getType_id());
        newNotification.setStatus_forKey(user_id, "unseen");

        if (repoNotificare.update(newNotification, user_id.toString()) == null) {
            notifyObservers(new NotificationChangeEvent(ChangeEventType.UPDATE, newNotification, oldNotification));
        }

        return null;
    }

    public Notificare markNotificationAsRead(Notificare oldNotification, Long user_id) {
        Notificare newNotification = new Notificare(oldNotification.getId(), oldNotification.getDescription(), oldNotification.getType(), oldNotification.getStatus(), oldNotification.getTo(), oldNotification.getDate_time(), oldNotification.getType_id());
        newNotification.setStatus_forKey(user_id, "seen");

        if (repoNotificare.update(newNotification, user_id.toString()) == null) {
            notifyObservers(new NotificationChangeEvent(ChangeEventType.UPDATE, newNotification, oldNotification));
        }

        return null;
    }

    public Notificare findNotification_id(Long id) {
        Notificare notification_found = repoNotificare.findOne(id);
        if (notification_found == null) {
            throw new ServiceException("Nu exista nicio notificare cu id-ul mentionat!");
        }
        return notification_found;
    }

    public Notificare findNotification_type_id(String type_id) {
        Iterable<Notificare> notificari = repoNotificare.findAll();
        for (Notificare notificare : notificari) {
            if (notificare.getType_id().equals(type_id)) {
                return notificare;
            }
        }
        throw new ServiceException("Nu exista nicio notificare pentru evenimentul cu id-ul mentionat!");
    }
    //////


    ////PAGINARE
    private int page = 0;
    private int size = Constants.NUMBER_OF_RECORDS_ON_PAGE;

    private Pageable pageable;

    public void setPageSize(int size) {
        this.size = size;
    }

    //public void setPageable(Pageable pageable) {
    //    this.pageable = pageable;
    //}

    public List<Notificare> getNextNotification(Long id) {
        this.page++;
        return getNotificationsOnPage(this.page, id);
    }

    public List<Notificare> getNotificationsOnPage(int page, Long id) {
        this.page = page;
        Pageable pageable = new PageableImplementation(page, this.size);
        Page_version2<Notificare> notificationsPage = repoNotificare.findAll(pageable, id, "all");
        return notificationsPage.getContent().collect(Collectors.toList());
    }

    public List<Notificare> getUsersNotificationsOnPage(int page, Long id) {
        this.page = page;
        Pageable pageable = new PageableImplementation(page, this.size);
        Page_version2<Notificare> notificationsPage = repoNotificare.findAll(pageable, id, "received");
        return notificationsPage.getContent().collect(Collectors.toList());
    }
    //////

    private List<NotificareDTO> getNotificationDTOList(Iterable<Notificare> notifications) {
        List<NotificareDTO> result = new ArrayList<>();
        for (Notificare notification : notifications) {
            Set<Utilizator> to_users = new HashSet<>();
            for (Long to_id : notification.getTo()) {
                try {
                    Utilizator found_user = utilizatorService.findUser_id(to_id);
                    to_users.add(found_user);
                }
                catch (Exception ex) {
                    Utilizator utilizator_inexistent = new Utilizator(0L, "Utilizator", "Inexistent", "nouser@mail.com", "nopassword");
                    to_users.add(utilizator_inexistent);
                }
            }

            NotificareDTO notificareDTO = new NotificareDTO(notification.getId(), notification.getDescription(), notification.getType(), notification.getStatus(), to_users, notification.getDate_time(), notification.getType_id());
            result.add(notificareDTO);
        }
        return result;
    }

    public List<NotificareDTO> getUsersNotifications_PAGED(int page, Long id) {
        List<NotificareDTO> result = new ArrayList<>();
        Iterable<Notificare> notifications = this.getUsersNotificationsOnPage(page, id);
        return getNotificationDTOList(notifications);
    }

    public int number_of_notifications(Long id) {
        /*int count = 0;
        Iterable<Notificare> notificari = this.getAllNotifications();
        for (Notificare notificare : notificari) {
            if (notificare.getTo().contains(id)) {
                count++;
            }
        }*/
        return repoNotificare.size("received_" + id.toString());
    }
}
