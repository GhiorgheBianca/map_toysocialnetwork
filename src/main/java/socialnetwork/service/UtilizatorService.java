package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.repository.paging.*;
import socialnetwork.utils.events.ChangeEventType;
import socialnetwork.utils.events.Event;
import socialnetwork.utils.events.FriendRequestChangeEvent;
import socialnetwork.utils.events.FriendshipChangeEvent;
import socialnetwork.utils.graph_algorithms.FriendshipsGraph;
import socialnetwork.repository.RepoException;
import socialnetwork.repository.Repository;
import socialnetwork.utils.Constants;
import socialnetwork.utils.observer.Observable;
import socialnetwork.utils.observer.Observer;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UtilizatorService extends SuperService {
    //private Repository<Long, Utilizator> repoUtilizator;
    private PagingRepository<Long, Utilizator> repoUtilizator;

    private PrietenieService prietenieService;
    private CerereService cerereService;

    //public UtilizatorService(Repository<Long, Utilizator> repoUtilizator) {
    //    this.repoUtilizator = repoUtilizator;
    //}

    public UtilizatorService(PagingRepository<Long, Utilizator> repoUtilizator) {
        this.repoUtilizator = repoUtilizator;
    }

    //public Repository<Long, Utilizator> getRepoUtilizator() {
    //    return this.repoUtilizator;
    //}

    public void setPrietenieService(PrietenieService prietenieService) {
        this.prietenieService = prietenieService;
    }
    public void setCerereService(CerereService cerereService) {
        this.cerereService = cerereService;
    }


    ////OPERATIILE PENTRU UTILIZATORI
    public Utilizator addUtilizator(Utilizator given_user) {
        Utilizator user = repoUtilizator.save(given_user);
        return user;
    }

    public Utilizator deleteUtilizator(Long id) {
        //sterge toate relatiile de prietenie in care era implicat utilizatorul
        Iterator<Prietenie> iterator_prietenii = prietenieService.getAllFriendships().iterator();
        while (iterator_prietenii.hasNext()) {
            Prietenie prietenie = iterator_prietenii.next();
            if (prietenie.getId().getLeft().equals(id) || prietenie.getId().getRight().equals(id)) {
                prietenieService.deleteFriendship(prietenie.getId());
                this.connectFriendships();
                iterator_prietenii = prietenieService.getAllFriendships().iterator();
            }
        }

        //sterge toate cererile de prietenie
        Iterator<CerereDePrietenie> iterator_cereri = cerereService.getAllRequests().iterator();
        while (iterator_cereri.hasNext()) {
            CerereDePrietenie cerere = iterator_cereri.next();
            if (cerere.getId().getLeft().equals(id) || cerere.getId().getRight().equals(id)) {
                cerereService.deleteRequest(cerere.getId());
                iterator_cereri = cerereService.getAllRequests().iterator();
            }
        }

        //stergerea utilizatorului
        Utilizator user = repoUtilizator.delete(id, "");
        return user;
    }

    public Iterable<Utilizator> getAllUsers(){
        return repoUtilizator.findAll();
    }

    public int nrUtilizatori() {
        return repoUtilizator.size("all");
    }

    public void printUsers() {
        this.getAllUsers().forEach(System.out::println);
    }

    public Utilizator findUser_id(Long id) {
        Utilizator user_found = repoUtilizator.findOne(id);
        if (user_found == null) {
            throw new ServiceException("Nu exista niciun utilizator cu id-ul mentionat!");
        }
        return user_found;
    }

    public Utilizator findUser_email(String email) {
        for (Utilizator user : this.getAllUsers()) {
            if (user.getEmail().equals(email)) {
                return user;
            }
        }
        return null;
    }

    public List<PrietenDTO> getFriends_forUser(Long id) {
        Utilizator user_found = this.findUser_id(id);
        this.connectFriendships();

        List<PrietenDTO> result = user_found.getFriends().stream()
                .map(friend -> {
                    try {
                        Prietenie friendship = prietenieService.findFriendship_id(friend.getId(), id);
                        return new PrietenDTO(friend, friendship.getDate());//.toString();
                    }
                    catch (ServiceException e) {
                        Prietenie friendship = prietenieService.findFriendship_id(id, friend.getId());
                        return new PrietenDTO(friend, friendship.getDate());//.toString();
                    }
                })
                .collect(Collectors.toList());

        return result;
    }

    public List<PrietenDTO> getFriends_forUser_PAGED(int page, Long id) {
        List<PrietenDTO> result = new ArrayList<>();
        Iterable<Prietenie> prietenii = prietenieService.getFriendshipsOnPage(page, id);
        for (Prietenie prietenie : prietenii) {
            if (id.equals(prietenie.getId().getRight())) {
                PrietenDTO prietenDTO = new PrietenDTO(this.findUser_id(prietenie.getId().getLeft()), prietenie.getDate());
                result.add(prietenDTO);
            }
            else if (id.equals(prietenie.getId().getLeft())) {
                PrietenDTO prietenDTO = new PrietenDTO(this.findUser_id(prietenie.getId().getRight()), prietenie.getDate());
                result.add(prietenDTO);
            }
        }
        return result;
    }

    public List<PrietenDTO> getAllFriends_forUser(Long id) {
        List<PrietenDTO> result = new ArrayList<>();
        Iterable<Prietenie> prietenii = prietenieService.getAllFriendships();
        for (Prietenie prietenie : prietenii) {
            if (id.equals(prietenie.getId().getRight())) {
                PrietenDTO prietenDTO = new PrietenDTO(this.findUser_id(prietenie.getId().getLeft()), prietenie.getDate());
                result.add(prietenDTO);
            }
            else if (id.equals(prietenie.getId().getLeft())) {
                PrietenDTO prietenDTO = new PrietenDTO(this.findUser_id(prietenie.getId().getRight()), prietenie.getDate());
                result.add(prietenDTO);
            }
        }
        return result;
    }

    public List<String> getFriendsByMonth_forUser(Long id, Month luna) {
        Utilizator user_found = this.findUser_id(id);
        this.connectFriendships();

        List<String> result = user_found.getFriends().stream()
                .map(friend -> {
                    try {
                        Prietenie friendship = prietenieService.findFriendship_id(friend.getId(), id);
                        return new PrietenDTO(friend, friendship.getDate());
                    }
                    catch (ServiceException e) {
                        Prietenie friendship = prietenieService.findFriendship_id(id, friend.getId());
                        return new PrietenDTO(friend, friendship.getDate());
                    }
                })
                .filter(friendDTO -> friendDTO.getDate().getMonth() == luna)
                .map(PrietenDTO::toString)
                .collect(Collectors.toList());

        return result;
    }
    //////

    public void connectFriendships() {
        Long id_firstFriend, id_secondFriend;
        getAllUsers().forEach(Utilizator::deleteAllFriends);
        for (Prietenie prietenie : prietenieService.getAllFriendships()) {
            id_firstFriend = prietenie.getId().getLeft();
            id_secondFriend = prietenie.getId().getRight();

            Utilizator firstFriend = repoUtilizator.findOne(id_firstFriend);
            Utilizator secondFriend = repoUtilizator.findOne(id_secondFriend);

            firstFriend.addFriend(secondFriend);
            secondFriend.addFriend(firstFriend);
        }
    }

    public List<Utilizator> getNonFriends_ForUser(Utilizator actual_user) {
        List<Utilizator> non_friends = new ArrayList<>();
        this.connectFriendships();

        for (Utilizator user : this.getAllUsers()) {
            if (!actual_user.getFriends().contains(user) && !actual_user.getId().equals(user.getId())) {
                non_friends.add(user);
            }
        }

        return non_friends;
    }

    public List<PrietenDTO> getFriends_betweenDates(LocalDateTime start_date, LocalDateTime end_date, Long id_user) {
        List<PrietenDTO> friends = new ArrayList<>();

        for (PrietenDTO prietenDTO : this.getAllFriends_forUser(id_user)) {
            if (prietenDTO.getDate().isAfter(start_date) && prietenDTO.getDate().isBefore(end_date)) {
                friends.add(prietenDTO);
            }
        }

        return friends;
    }


    ////COMENZI PENTRU COMUNITATI
    private FriendshipsGraph creareGrafPrietenii() {
        FriendshipsGraph graph = new FriendshipsGraph(nrUtilizatori(), prietenieService.nrPrietenii());
        graph.initialize(nrUtilizatori());

        int left_id, right_id;
        for (Prietenie prietenie : prietenieService.getAllFriendships()) {
            left_id = prietenie.getId().getLeft().intValue();
            right_id = prietenie.getId().getRight().intValue();
            graph.addEdge(left_id - 1, right_id - 1);
        }

        return graph;
    }

    public int numberOfCommunities() {
        return creareGrafPrietenii().connectedComponentsCount();
    }

    public List<Utilizator> ceaMaiSociabilaComunitate() {
        List<Utilizator> utilizatorii_comunitatii = new ArrayList<>();
        HashSet<Integer> user_ids = creareGrafPrietenii().longestPathComponent();

        for (Integer id : user_ids) {
            utilizatorii_comunitatii.add(findUser_id(Long.valueOf(id + 1)));
        }

        return utilizatorii_comunitatii;
    }
    //////


    ////PAGINARE
    private int page = 0;
    private int size = Constants.NUMBER_OF_RECORDS_ON_PAGE;

    private Pageable pageable;

    public void setPageSize(int size) {
        this.size = size;
    }

    //public void setPageable(Pageable pageable) {
    //    this.pageable = pageable;
    //}

    public Set<Utilizator> getNextUser(Long id) {
        this.page++;
        return getUsersOnPage(this.page, id);
    }

    public Set<Utilizator> getUsersOnPage(int page, Long id) {
        this.page = page;
        Pageable pageable = new PageableImplementation(page, this.size);
        Page_version2<Utilizator> userPage = repoUtilizator.findAll(pageable, id, "all");
        return userPage.getContent().collect(Collectors.toSet());
    }
    //////

}
