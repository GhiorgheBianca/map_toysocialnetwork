package socialnetwork.repository.file;

import socialnetwork.domain.Mesaj;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MesajFile extends AbstractFileRepository<Long, Mesaj> {
    public MesajFile(String fileName, Validator<Mesaj> validator) {
        super(fileName, validator);
    }

    @Override
    public Mesaj extractEntity(List<String> attributes) {
        List<String> to_String = Arrays.asList(attributes.get(2).split(","));
        List<Long> to_Long = new ArrayList<>();
        for (String to : to_String) {
            to_Long.add(Long.parseLong(to));
        }

        LocalDateTime dateTime = LocalDateTime.parse(attributes.get(4), Constants.DATE_TIME_FORMATTER);

        Long id_reply;
        if (attributes.get(5).equals("null")) {
            id_reply = null;
        }
        else {
            id_reply = Long.parseLong(attributes.get(5));
        }

        Mesaj mesaj = new Mesaj(Long.parseLong(attributes.get(0)), Long.parseLong(attributes.get(1)), to_Long, attributes.get(3), dateTime, id_reply);

        validator.validate(mesaj);
        return mesaj;
    }

    @Override
    protected String createEntityAsString(Mesaj entity) {
        String to_String = "";
        for (Long to : entity.getTo()) {
            to_String += to.toString() + ",";
        }
        if (to_String.length() > 1) {
            to_String = to_String.substring(0, to_String.length() - 1);
        }

        if (entity.getId_reply() == null) {
            return entity.getId() + ";" + entity.getFrom() + ";" + to_String + ";" + entity.getMessage() + ";" + entity.getDate().format(Constants.DATE_TIME_FORMATTER) + ";" + "null";
        }
        return entity.getId() + ";" + entity.getFrom() + ";" + to_String + ";" + entity.getMessage() + ";" + entity.getDate().format(Constants.DATE_TIME_FORMATTER) + ";" + entity.getId_reply().toString();
    }
}
