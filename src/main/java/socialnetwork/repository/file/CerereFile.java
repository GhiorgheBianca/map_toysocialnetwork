package socialnetwork.repository.file;

import socialnetwork.domain.CerereDePrietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;
import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.util.List;

public class CerereFile extends AbstractFileRepository<Tuple<Long,Long>, CerereDePrietenie> {

    public CerereFile(String fileName, Validator<CerereDePrietenie> validator) {
        super(fileName, validator);
    }

    @Override
    public CerereDePrietenie extractEntity(List<String> attributes) {
        LocalDateTime dateTime = LocalDateTime.parse(attributes.get(2), Constants.DATE_TIME_FORMATTER);

        CerereDePrietenie cerere = new CerereDePrietenie(Long.parseLong(attributes.get(0)), Long.parseLong(attributes.get(1)), attributes.get(2), dateTime);

        validator.validate(cerere);
        return cerere;
    }

    @Override
    protected String createEntityAsString(CerereDePrietenie entity) {
        return entity.getId().getLeft()+";"+entity.getId().getRight()+";"+entity.getState()+";"+entity.getDate().format(Constants.DATE_TIME_FORMATTER);
    }
}
