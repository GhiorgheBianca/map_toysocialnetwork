package socialnetwork.repository.file;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;
import socialnetwork.utils.Constants;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class PrietenieFile extends AbstractFileRepository<Tuple<Long,Long>,Prietenie> {

    public PrietenieFile(String fileName, Validator<Prietenie> validator) {
        super(fileName, validator);
    }

    @Override
    public Prietenie extractEntity(List<String> attributes) {
        LocalDateTime dateTime = LocalDateTime.parse(attributes.get(2), Constants.DATE_TIME_FORMATTER);

        Prietenie prietenie = new Prietenie(Long.parseLong(attributes.get(0)), Long.parseLong(attributes.get(1)), dateTime);

        validator.validate(prietenie);
        return prietenie;
    }

    @Override
    protected String createEntityAsString(Prietenie entity) {
        return entity.getId().getLeft()+";"+entity.getId().getRight()+";"+entity.getDate().format(Constants.DATE_TIME_FORMATTER);
    }
}
