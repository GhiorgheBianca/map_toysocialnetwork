package socialnetwork.repository.database;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.RepoException;
import socialnetwork.repository.paging.*;
import socialnetwork.utils.Constants;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class PrietenieDBRepository implements PagingRepository<Tuple<Long,Long>, Prietenie> {
    private String url;
    private String username;
    private String password;
    private Validator<Prietenie> validator;

    public PrietenieDBRepository(String url, String username, String password, Validator<Prietenie> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    @Override
    public Prietenie findOne(Tuple<Long,Long> aLong) {
        if (aLong == null) {
            throw new IllegalArgumentException("Entitatea nu poate fi null!\n");
        }

        String query = "SELECT * FROM friendships WHERE first_id = " + aLong.getLeft().toString() + " AND " + "second_id = " + aLong.getRight().toString();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {

            if (resultSet.next()) {
                Long first_id = resultSet.getLong("first_id");
                Long second_id = resultSet.getLong("second_id");
                String date = resultSet.getString("date_time");

                LocalDateTime dateTime = LocalDateTime.parse(date, Constants.DATE_TIME_FORMATTER);

                Prietenie friendship = new Prietenie(first_id, second_id, dateTime);

                return friendship;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Prietenie> findAll() {
        List<Prietenie> friendships = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM friendships ORDER BY first_id, second_id");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long first_id = resultSet.getLong("first_id");
                Long second_id = resultSet.getLong("second_id");
                String date = resultSet.getString("date_time");

                LocalDateTime dateTime = LocalDateTime.parse(date, Constants.DATE_TIME_FORMATTER);

                Prietenie friendship = new Prietenie(first_id, second_id, dateTime);
                friendships.add(friendship);
            }

            Collections.sort(friendships, new Comparator<Prietenie>() {
                @Override
                public int compare(Prietenie p1, Prietenie p2) {
                    return p1.getId().getLeft().compareTo(p2.getId().getLeft()) + p1.getId().getRight().compareTo(p2.getId().getRight());
                }
            });

            return friendships;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return friendships;
    }

    @Override
    public Page_version2<Prietenie> findAll(Pageable pageable, Long user_id, String mode) {
        int contor = 0;
        List<Prietenie> result = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM friendships WHERE first_id = " + user_id.toString() + " OR second_id = " + user_id.toString() + " ORDER BY date_time LIMIT ? OFFSET ?")) {

            statement.setInt(1, pageable.getPageSize());
            statement.setInt(2, pageable.getPageSize() * pageable.getPageNumber());
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long first_id = resultSet.getLong("first_id");
                Long second_id = resultSet.getLong("second_id");
                String date = resultSet.getString("date_time");

                LocalDateTime dateTime = LocalDateTime.parse(date, Constants.DATE_TIME_FORMATTER);

                Prietenie friendship = new Prietenie(first_id, second_id, dateTime);
                result.add(friendship);
                contor++;
            }

            //Collections.sort(result, new Comparator<Prietenie>() {
            //    @Override
            //    public int compare(Prietenie p1, Prietenie p2) {
            //        return p1.getId().getLeft().compareTo(p2.getId().getLeft()) + p1.getId().getRight().compareTo(p2.getId().getRight());
            //    }
            //});
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Page_version2<Prietenie> pageVersion2 = new Page_version2<>(result.stream(), contor);//this.size("all"));
        return pageVersion2;
    }

    @Override
    public Prietenie save(Prietenie entity) {
        if (entity == null) {
            throw new IllegalArgumentException("Entitatea nu poate fi null!\n");
        }

        validator.validate(entity);
        if (exists(entity.getId())) {
            throw new RepoException("Entitatea exista deja!\n");
        }

        else {
            String query = "INSERT INTO friendships(first_id, second_id, date_time) VALUES(" + entity.getId().getLeft().toString() + ", " + entity.getId().getRight().toString() + ", '" + entity.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) + "')";
            try (Connection connection = DriverManager.getConnection(url, username, password);
                 PreparedStatement statement = connection.prepareStatement(query)) {
                statement.execute();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public Prietenie delete(Tuple<Long,Long> aLong, String mode) {
        if (aLong == null) {
            throw new IllegalArgumentException("ID-ul nu poate fi null!\n");
        }

        Prietenie friendship = findOne(aLong);
        if (friendship == null) {
            throw new RepoException("Entitatea cu ID-ul dat nu exista!\n");
        }

        String query = "DELETE FROM friendships WHERE first_id = " + aLong.getLeft().toString() + " AND " + "second_id = " + aLong.getRight().toString();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return friendship;
    }

    @Override
    public Prietenie update(Prietenie entity, String mode) {
        if(entity == null) {
            throw new IllegalArgumentException("Entitatea nu poate fi null!\n");
        }

        validator.validate(entity);
        if (!exists(entity.getId())) {
            return entity;
        }

        String query = "UPDATE friendships SET date_time = '" + entity.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) + "' WHERE first_id = " + entity.getId().getLeft().toString() + " AND " + "second_id = " + entity.getId().getRight().toString();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public int size(String mode) {
        List<String> attributes = Arrays.asList(mode.split("_"));

        String query = "";
        if (attributes.get(0).equals("all")) {
            query = "SELECT COUNT(*) AS total FROM friendships";
        }
        else if (attributes.get(0).equals("friendsWith")) {
            query = "SELECT COUNT(*) AS total FROM friendships WHERE first_id = " + attributes.get(1) + " OR second_id = " + attributes.get(1);
        }
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {

            if (resultSet.next()) {
                int size = resultSet.getInt("total");
                return size;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public boolean exists(Tuple<Long,Long> id) {
        if (id == null) {
            throw new IllegalArgumentException("ID-ul nu poate fi null!\n");
        }

        if (findOne(id) != null) {
            return true;
        }
        return false;
    }

}
