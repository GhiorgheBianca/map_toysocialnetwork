package socialnetwork.repository.database;

import socialnetwork.domain.Eveniment;
import socialnetwork.domain.Mesaj;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.RepoException;
import socialnetwork.repository.paging.Page_version2;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PagingRepository;
import socialnetwork.utils.Constants;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class EvenimentDBRepository implements PagingRepository<Long, Eveniment> {
    private final String url;
    private final String username;
    private final String password;
    private final Validator<Eveniment> validator;

    public EvenimentDBRepository(String url, String username, String password, Validator<Eveniment> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    @Override
    public Eveniment findOne(Long aLong) {
        if (aLong == null) {
            throw new IllegalArgumentException("Entitatea nu poate fi null!\n");
        }

        String query = "SELECT e.id, e.name, e.description, e.type, e.start_date, e.final_date, ep.participant_id FROM events e LEFT OUTER JOIN events_participants ep ON e.id = ep.id_event WHERE e.id = " + aLong.toString();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {

            Long id_event = 0L;
            Set<Long> participants_list = new HashSet<>();
            String name = "", description = "", type = "";
            LocalDateTime start_date = LocalDateTime.now(), final_date = LocalDateTime.now();
            while (resultSet.next()) {
                id_event = resultSet.getLong("id");
                Long id_participant = resultSet.getLong("participant_id");
                name = resultSet.getString("name");
                description = resultSet.getString("description");
                type = resultSet.getString("type");
                String string_start = resultSet.getString("start_date");
                String string_final = resultSet.getString("final_date");

                start_date = LocalDateTime.parse(string_start, Constants.DATE_TIME_FORMATTER);
                final_date = LocalDateTime.parse(string_final, Constants.DATE_TIME_FORMATTER);

                if (!id_participant.equals(0L)) {
                    participants_list.add(id_participant);
                }
            }

            if (!description.equals("")) {
                Eveniment event = new Eveniment(id_event, name, description, type, start_date, final_date, participants_list);
                return event;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Eveniment> findAll() {
        List<Eveniment> events = new ArrayList<>();
        String query = "SELECT e.id, e.name, e.description, e.type, e.start_date, e.final_date, ep.participant_id FROM events e LEFT OUTER JOIN events_participants ep ON e.id = ep.id_event ORDER BY e.id";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {

            Long id_event = 0L;
            Set<Long> participants_list = new HashSet<>();
            String name = "", description = "", type = "";
            LocalDateTime start_date = LocalDateTime.now(), final_date = LocalDateTime.now();
            Long id_anterior = -1L;
            while (resultSet.next()) {
                id_event = resultSet.getLong("id");

                if (id_anterior.equals(-1L)) {
                    id_anterior = id_event;
                }
                if (!id_anterior.equals(id_event)) {
                    Eveniment event = new Eveniment(id_anterior, name, description, type, start_date, final_date, participants_list);
                    events.add(event);
                    participants_list = new HashSet<>();
                    id_anterior = id_event;
                }

                Long id_participant = resultSet.getLong("participant_id");
                name = resultSet.getString("name");
                description = resultSet.getString("description");
                type = resultSet.getString("type");
                String string_start = resultSet.getString("start_date");
                String string_final = resultSet.getString("final_date");

                start_date = LocalDateTime.parse(string_start, Constants.DATE_TIME_FORMATTER);
                final_date = LocalDateTime.parse(string_final, Constants.DATE_TIME_FORMATTER);

                if (!id_participant.equals(0L)) {
                    participants_list.add(id_participant);
                }
            }

            if (!description.equals("")) {
                Eveniment event = new Eveniment(id_anterior, name, description, type, start_date, final_date, participants_list);
                events.add(event);
            }

            Collections.sort(events, new Comparator<Eveniment>() {
                @Override
                public int compare(Eveniment e1, Eveniment e2) {
                    return e1.getId().compareTo(e2.getId());
                }
            });

            return events;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return events;
    }

    @Override
    public Page_version2<Eveniment> findAll(Pageable pageable, Long user_id, String mode) {
        if (mode.equals("participates")) {
            return filter_UserGoesTo(pageable, user_id, "yes");
        }
        else if (mode.equals("doesn't participates")) {
            return filter_UserGoesTo(pageable, user_id, "no");
        }
        else if (mode.equals("all")) {
            return filter_All(pageable);
        }
        return null;
    }

    public Page_version2<Eveniment> filter_UserGoesTo(Pageable pageable, Long user_id, String mode) {
        int contor = 0;
        List<Eveniment> result = new ArrayList<>();

        //SELECT *
        //FROM events
        //WHERE id IN (SELECT id_event
        //             FROM events_participants
        //             WHERE participant_id = )
        //LIMIT ? OFFSET ?;

        String query = "";
        if (mode.equals("yes")) {
            query = "SELECT * FROM events WHERE id IN (SELECT id_event FROM events_participants WHERE participant_id = " + user_id.toString() +") ORDER BY start_date, final_date LIMIT ? OFFSET ?";
        }
        else if (mode.equals("no")) {
            query = "SELECT * FROM events WHERE id NOT IN (SELECT id_event FROM events_participants WHERE participant_id = " + user_id.toString() +") ORDER BY start_date, final_date LIMIT ? OFFSET ?";
        }
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setInt(1, pageable.getPageSize());
            statement.setInt(2, pageable.getPageSize() * pageable.getPageNumber());
            ResultSet resultSet = statement.executeQuery();

            Long id_event = 0L;
            Set<Long> participants_list = new HashSet<>();
            String name = "", description = "", type = "";
            LocalDateTime start_date = LocalDateTime.now(), final_date = LocalDateTime.now();
            while (resultSet.next()) {
                id_event = resultSet.getLong("id");

                name = resultSet.getString("name");
                description = resultSet.getString("description");
                type = resultSet.getString("type");
                String string_start = resultSet.getString("start_date");
                String string_final = resultSet.getString("final_date");

                start_date = LocalDateTime.parse(string_start, Constants.DATE_TIME_FORMATTER);
                final_date = LocalDateTime.parse(string_final, Constants.DATE_TIME_FORMATTER);

                try (Connection second_connection = DriverManager.getConnection(url, username, password);
                     PreparedStatement second_statement = second_connection.prepareStatement("SELECT * FROM events_participants WHERE id_event = " + id_event.toString());
                     ResultSet second_resultSet = second_statement.executeQuery()) {

                    participants_list = new HashSet<>();

                    while (second_resultSet.next()) {
                        Long id_participant = second_resultSet.getLong("participant_id");

                        if (!id_participant.equals(0L)) {
                            participants_list.add(id_participant);
                        }
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }

                Eveniment event = new Eveniment(id_event, name, description, type, start_date, final_date, participants_list);
                result.add(event);
                contor++;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        Page_version2<Eveniment> pageVersion2 = new Page_version2<>(result.stream(), contor);
        return pageVersion2;
    }

    public Page_version2<Eveniment> filter_All(Pageable pageable) {
        int contor = 0;
        List<Eveniment> result = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM events ORDER BY start_date, final_date LIMIT ? OFFSET ?")) {

            statement.setInt(1, pageable.getPageSize());
            statement.setInt(2, pageable.getPageSize() * pageable.getPageNumber());
            ResultSet resultSet = statement.executeQuery();

            Long id_event = 0L;
            Set<Long> participants_list = new HashSet<>();
            String name = "", description = "", type = "";
            LocalDateTime start_date = LocalDateTime.now(), final_date = LocalDateTime.now();
            while (resultSet.next()) {
                id_event = resultSet.getLong("id");

                name = resultSet.getString("name");
                description = resultSet.getString("description");
                type = resultSet.getString("type");
                String string_start = resultSet.getString("start_date");
                String string_final = resultSet.getString("final_date");

                start_date = LocalDateTime.parse(string_start, Constants.DATE_TIME_FORMATTER);
                final_date = LocalDateTime.parse(string_final, Constants.DATE_TIME_FORMATTER);

                try (Connection second_connection = DriverManager.getConnection(url, username, password);
                     PreparedStatement second_statement = second_connection.prepareStatement("SELECT * FROM events_participants WHERE id_event = " + id_event.toString());
                     ResultSet second_resultSet = second_statement.executeQuery()) {

                    participants_list = new HashSet<>();

                    while (second_resultSet.next()) {
                        Long id_participant = second_resultSet.getLong("participant_id");

                        if (!id_participant.equals(0)) {
                            participants_list.add(id_participant);
                        }
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }

                Eveniment event = new Eveniment(id_event, name, description, type, start_date, final_date, participants_list);
                result.add(event);
                contor++;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        Page_version2<Eveniment> pageVersion2 = new Page_version2<>(result.stream(), contor);//this.size("all"));
        return pageVersion2;
    }

    @Override
    public Eveniment save(Eveniment entity) {
        if (entity == null) {
            throw new IllegalArgumentException("Entitatea nu poate fi null!\n");
        }

        validator.validate(entity);
        if (exists(entity.getId())) {
            throw new RepoException("Entitatea exista deja!\n");
        }

        else {
            String query_events = "INSERT INTO events(id, name, description, type, start_date, final_date) VALUES(" + entity.getId().toString() + ", '" + entity.getName() + "', '" + entity.getDescription() + "', '" + entity.getType() + "', '" + entity.getStart_date().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) + "', '" + entity.getFinal_date().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) + "')";
            try (Connection connection = DriverManager.getConnection(url, username, password);
                 PreparedStatement statement = connection.prepareStatement(query_events)) {
                statement.execute();

            } catch (SQLException e) {
                e.printStackTrace();
            }

            for (Long id : entity.getParticipants()) {
                String query_events_participants = "INSERT INTO events_participants(id_event, participant_id) VALUES(" + entity.getId().toString() + ", " + id.toString() + ")";
                try (Connection connection = DriverManager.getConnection(url, username, password);
                     PreparedStatement statement = connection.prepareStatement(query_events_participants)) {
                    statement.execute();

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public Eveniment delete(Long event_id, String mode) {
        if (event_id == null) {
            throw new IllegalArgumentException("ID-ul nu poate fi null!\n");
        }

        Eveniment eveniment = findOne(event_id);
        if (eveniment == null) {
            throw new RepoException("Entitatea cu ID-ul dat nu exista!\n");
        }

        String query = "";
        if (mode.equals("")) {
            query = "DELETE FROM events WHERE id = " + event_id.toString();
        }
        else {
            query = "DELETE FROM events_participants WHERE participant_id = " + mode + " AND id_event = " + event_id.toString();
        }
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return eveniment;
    }

    @Override
    public Eveniment update(Eveniment entity, String mode) {
        if (entity == null) {
            throw new IllegalArgumentException("Entitatea nu poate fi null!\n");
        }

        validator.validate(entity);
        if (!exists(entity.getId())) {
            return entity;
        }

        String query = "UPDATE events SET name = '" + entity.getName() + "', description = '" + entity.getDescription() + "', type = '" + entity.getType() + "', start_date = '" + entity.getStart_date().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) + "', final_date = '" + entity.getFinal_date().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) + "' WHERE id = '" + entity.getId().toString() + "'";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public int size(String mode) {
        List<String> attributes = Arrays.asList(mode.split("_"));

        String query = "";
        if (attributes.get(0).equals("all")) {
            query = "SELECT COUNT(*) AS total FROM events";
        }
        else if (attributes.get(0).equals("goesTo")) {
            query = "SELECT COUNT(*) AS total FROM events WHERE id IN (SELECT id_event FROM events_participants WHERE participant_id = " + attributes.get(1) + ")";
        }
        else if (attributes.get(0).equals("doesntGoTo")) {
            query = "SELECT COUNT(*) AS total FROM events WHERE id NOT IN (SELECT id_event FROM events_participants WHERE participant_id = " + attributes.get(1) + ")";
        }
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {

            if (resultSet.next()) {
                int size = resultSet.getInt("total");
                return size;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public boolean exists(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("ID-ul nu poate fi null!\n");
        }

        if (findOne(id) != null) {
            return true;
        }
        return false;
    }
}
