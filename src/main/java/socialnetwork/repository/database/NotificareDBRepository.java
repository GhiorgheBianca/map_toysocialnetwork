package socialnetwork.repository.database;

import socialnetwork.domain.Eveniment;
import socialnetwork.domain.Mesaj;
import socialnetwork.domain.Notificare;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.RepoException;
import socialnetwork.repository.paging.Page_version2;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PagingRepository;
import socialnetwork.utils.Constants;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class NotificareDBRepository implements PagingRepository<Long, Notificare> {
    private final String url;
    private final String username;
    private final String password;
    private final Validator<Notificare> validator;

    public NotificareDBRepository(String url, String username, String password, Validator<Notificare> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    @Override
    public Notificare findOne(Long aLong) {
        if (aLong == null) {
            throw new IllegalArgumentException("Entitatea nu poate fi null!\n");
        }

        String query = "SELECT n.id, n.description, n.type, n.type_id, nt.status, n.date_time, nt.to_userid FROM notifications n LEFT OUTER JOIN notifications_to nt ON n.id = nt.id_notification WHERE n.id = " + aLong.toString();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {

            Long id_notification = 0L;
            Map<Long,String> status_map = new HashMap<>();
            Set<Long> to_list = new HashSet<>();
            String description = "", type = "", type_id = "";
            LocalDateTime dateTime = LocalDateTime.now();
            while (resultSet.next()) {
                id_notification = resultSet.getLong("id");
                Long id_to = resultSet.getLong("to_userid");
                description = resultSet.getString("description");
                type = resultSet.getString("type");
                String status = resultSet.getString("status");
                String date = resultSet.getString("date_time");
                type_id = resultSet.getString("type_id");

                dateTime = LocalDateTime.parse(date, Constants.DATE_TIME_FORMATTER);

                if (!id_to.equals(0L)) {
                    to_list.add(id_to);
                    status_map.putIfAbsent(id_to, status);
                }
            }

            if (!description.equals("")) {
                Notificare notification = new Notificare(id_notification, description, type, status_map, to_list, dateTime, type_id);
                return notification;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Notificare> findAll() {
        List<Notificare> notifications = new ArrayList<>();
        String query = "SELECT n.id, n.description, n.type, n.type_id, nt.status, n.date_time, nt.to_userid FROM notifications n LEFT OUTER JOIN notifications_to nt ON n.id = nt.id_notification ORDER BY n.id";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {

            Long id_notification = 0L;
            Map<Long,String> status_map = new HashMap<>();
            Set<Long> to_list = new HashSet<>();
            String description = "", type = "", type_id = "";
            LocalDateTime dateTime = LocalDateTime.now();
            Long id_anterior = -1L;
            while (resultSet.next()) {
                id_notification = resultSet.getLong("id");

                if (id_anterior.equals(-1L)) {
                    id_anterior = id_notification;
                }
                if (!id_anterior.equals(id_notification)) {
                    Notificare notification = new Notificare(id_anterior, description, type, status_map, to_list, dateTime, type_id);
                    notifications.add(notification);
                    status_map = new HashMap<>();
                    to_list = new HashSet<>();
                    id_anterior = id_notification;
                }

                Long id_to = resultSet.getLong("to_userid");
                description = resultSet.getString("description");
                type = resultSet.getString("type");
                String status = resultSet.getString("status");
                String date = resultSet.getString("date_time");
                type_id = resultSet.getString("type_id");

                dateTime = LocalDateTime.parse(date, Constants.DATE_TIME_FORMATTER);

                if (!id_to.equals(0L)) {
                    to_list.add(id_to);
                    status_map.putIfAbsent(id_to, status);
                }
            }

            if (!description.equals("")) {
                Notificare notification = new Notificare(id_anterior, description, type, status_map, to_list, dateTime, type_id);
                notifications.add(notification);
            }

            Collections.sort(notifications, new Comparator<Notificare>() {
                @Override
                public int compare(Notificare n1, Notificare n2) {
                    return n1.getId().compareTo(n2.getId());
                }
            });

            return notifications;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return notifications;
    }

    @Override
    public Page_version2<Notificare> findAll(Pageable pageable, Long user_id, String mode) {
        if (mode.equals("received")) {
            return filter_UsersNotifications(pageable, user_id);
        }
        else if (mode.equals("all")) {
            return filter_All(pageable);
        }
        return null;
    }

    public Page_version2<Notificare> filter_UsersNotifications(Pageable pageable, Long user_id) {
        int contor = 0;
        List<Notificare> result = new ArrayList<>();
        String query = "SELECT * FROM notifications WHERE id IN (SELECT id_notification FROM notifications_to WHERE to_userid = " + user_id.toString() + ") ORDER BY date_time LIMIT ? OFFSET ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setInt(1, pageable.getPageSize());
            statement.setInt(2, pageable.getPageSize() * pageable.getPageNumber());
            ResultSet resultSet = statement.executeQuery();

            Long id_notification = 0L;
            Map<Long,String> status_map = new HashMap<>();
            Set<Long> to_list = new HashSet<>();
            String description = "", type = "", type_id = "";
            LocalDateTime dateTime = LocalDateTime.now();
            while (resultSet.next()) {
                id_notification = resultSet.getLong("id");

                description = resultSet.getString("description");
                type = resultSet.getString("type");
                String date = resultSet.getString("date_time");
                type_id = resultSet.getString("type_id");

                dateTime = LocalDateTime.parse(date, Constants.DATE_TIME_FORMATTER);

                try (Connection second_connection = DriverManager.getConnection(url, username, password);
                     PreparedStatement second_statement = second_connection.prepareStatement("SELECT * FROM notifications_to WHERE id_notification = " + id_notification.toString());
                     ResultSet second_resultSet = second_statement.executeQuery()) {

                    to_list = new HashSet<>();
                    status_map = new HashMap<>();

                    while (second_resultSet.next()) {
                        String status = second_resultSet.getString("status");
                        Long id_to = second_resultSet.getLong("to_userid");

                        to_list.add(id_to);
                        status_map.putIfAbsent(id_to, status);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }

                Notificare notification = new Notificare(id_notification, description, type, status_map, to_list, dateTime, type_id);
                if (to_list.contains(user_id)) {
                    result.add(notification);
                    contor++;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        Page_version2<Notificare> pageVersion2 = new Page_version2<>(result.stream(), contor);
        return pageVersion2;
    }

    public Page_version2<Notificare> filter_All(Pageable pageable) {
        int contor = 0;
        List<Notificare> result = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM notifications ORDER BY date_time LIMIT ? OFFSET ?")) {

            statement.setInt(1, pageable.getPageSize());
            statement.setInt(2, pageable.getPageSize() * pageable.getPageNumber());
            ResultSet resultSet = statement.executeQuery();

            Long id_notification = 0L;
            Map<Long,String> status_map = new HashMap<>();
            Set<Long> to_list = new HashSet<>();
            String description = "", type = "", type_id = "";
            LocalDateTime dateTime = LocalDateTime.now();
            while (resultSet.next()) {
                id_notification = resultSet.getLong("id");

                description = resultSet.getString("description");
                type = resultSet.getString("type");
                String date = resultSet.getString("date_time");
                type_id = resultSet.getString("type_id");

                dateTime = LocalDateTime.parse(date, Constants.DATE_TIME_FORMATTER);

                try (Connection second_connection = DriverManager.getConnection(url, username, password);
                     PreparedStatement second_statement = second_connection.prepareStatement("SELECT * FROM notifications_to WHERE id_notification = " + id_notification.toString());
                     ResultSet second_resultSet = second_statement.executeQuery()) {

                    to_list = new HashSet<>();
                    status_map = new HashMap<>();

                    while (second_resultSet.next()) {
                        String status = second_resultSet.getString("status");
                        Long id_to = second_resultSet.getLong("to_userid");

                        to_list.add(id_to);
                        status_map.putIfAbsent(id_to, status);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }

                Notificare notification = new Notificare(id_notification, description, type, status_map, to_list, dateTime, type_id);
                result.add(notification);
                contor++;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        Page_version2<Notificare> pageVersion2 = new Page_version2<>(result.stream(), contor);//this.size("all"));
        return pageVersion2;
    }

    @Override
    public Notificare save(Notificare entity) {
        if (entity == null) {
            throw new IllegalArgumentException("Entitatea nu poate fi null!\n");
        }

        validator.validate(entity);
        if (exists(entity.getId())) {
            throw new RepoException("Entitatea exista deja!\n");
        }

        else {
            String query_events = "INSERT INTO notifications(id, description, type, date_time, type_id) VALUES(" + entity.getId().toString() + ", '" + entity.getDescription() + "', '" + entity.getType() + "', '" + entity.getDate_time().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) + "', '" + entity.getType_id() + "')";
            try (Connection connection = DriverManager.getConnection(url, username, password);
                 PreparedStatement statement = connection.prepareStatement(query_events)) {
                statement.execute();

            } catch (SQLException e) {
                e.printStackTrace();
            }

            for (Long id : entity.getTo()) {
                String query_events_participants = "INSERT INTO notifications_to(id_notification, to_userid, status) VALUES(" + entity.getId().toString() + ", " + id.toString() + ", '" + entity.getStatus().get(id) + "')";
                try (Connection connection = DriverManager.getConnection(url, username, password);
                     PreparedStatement statement = connection.prepareStatement(query_events_participants)) {
                    statement.execute();

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public Notificare delete(Long notification_id, String mode) {
        if (notification_id == null) {
            throw new IllegalArgumentException("ID-ul nu poate fi null!\n");
        }

        Notificare notificare = findOne(notification_id);
        if (notificare == null) {
            throw new RepoException("Entitatea cu ID-ul dat nu exista!\n");
        }

        String query = "";
        if (mode.equals("")) {
            query = "DELETE FROM notifications WHERE id = " + notification_id.toString();
        }
        else {
            query = "DELETE FROM notifications_to WHERE to_userid = " + mode + " AND id_notification = " + notification_id.toString();
        }
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return notificare;
    }

    @Override
    public Notificare update(Notificare entity, String mode) {
        if (entity == null) {
            throw new IllegalArgumentException("Entitatea nu poate fi null!\n");
        }

        validator.validate(entity);
        if (!exists(entity.getId())) {
            return entity;
        }

        String query = "UPDATE notifications SET description = '" + entity.getDescription() + "', type = '" + entity.getType() + "', date_time = '" + entity.getDate_time().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) + "', type_id = '" + entity.getType_id() + "' WHERE id = '" + entity.getId().toString() + "'";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        query = "UPDATE notifications_to SET status = '" + entity.getStatus().get(Long.parseLong(mode)) + "' WHERE id_notification = " + entity.getId().toString() + " AND to_userid = " + mode;
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public int size(String mode) {
        List<String> attributes = Arrays.asList(mode.split("_"));

        String query = "";
        if (attributes.get(0).equals("all")) {
            query = "SELECT COUNT(*) AS total FROM notifications";
        }
        else if (attributes.get(0).equals("received")) {
            query = "SELECT COUNT(*) AS total FROM notifications WHERE id IN (SELECT id_notification FROM notifications_to WHERE to_userid = " + attributes.get(1) + ")";
        }
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {

            if (resultSet.next()) {
                int size = resultSet.getInt("total");
                return size;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public boolean exists(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("ID-ul nu poate fi null!\n");
        }

        if (findOne(id) != null) {
            return true;
        }
        return false;
    }
}
