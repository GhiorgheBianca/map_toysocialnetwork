package socialnetwork.repository.database;

import socialnetwork.domain.CerereDePrietenie;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.RepoException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.*;
import socialnetwork.utils.Constants;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class CerereDBRepository implements PagingRepository<Tuple<Long,Long>, CerereDePrietenie> {
    private String url;
    private String username;
    private String password;
    private Validator<CerereDePrietenie> validator;

    public CerereDBRepository(String url, String username, String password, Validator<CerereDePrietenie> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    @Override
    public CerereDePrietenie findOne(Tuple<Long,Long> aLong) {
        if (aLong == null) {
            throw new IllegalArgumentException("Entitatea nu poate fi null!\n");
        }

        String query = "SELECT * FROM friend_requests WHERE first_id = " + aLong.getLeft().toString() + " AND " + "second_id = " + aLong.getRight().toString();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {

            if (resultSet.next()) {
                Long first_id = resultSet.getLong("first_id");
                Long second_id = resultSet.getLong("second_id");
                String state = resultSet.getString("status");
                String date = resultSet.getString("date_time");

                LocalDateTime dateTime = LocalDateTime.parse(date, Constants.DATE_TIME_FORMATTER);

                CerereDePrietenie request = new CerereDePrietenie(first_id, second_id, state, dateTime);
                return request;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<CerereDePrietenie> findAll() {
        List<CerereDePrietenie> requests = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM friend_requests ORDER BY date_time");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long first_id = resultSet.getLong("first_id");
                Long second_id = resultSet.getLong("second_id");
                String state = resultSet.getString("status");
                String date = resultSet.getString("date_time");

                LocalDateTime dateTime = LocalDateTime.parse(date, Constants.DATE_TIME_FORMATTER);

                CerereDePrietenie request = new CerereDePrietenie(first_id, second_id, state, dateTime);
                requests.add(request);
            }

            Collections.sort(requests, new Comparator<CerereDePrietenie>() {
                @Override
                public int compare(CerereDePrietenie c1, CerereDePrietenie c2) {
                    return c1.getId().getLeft().compareTo(c2.getId().getLeft()) + c1.getId().getRight().compareTo(c2.getId().getRight());
                }
            });

            return requests;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return requests;
    }

    @Override
    public Page_version2<CerereDePrietenie> findAll(Pageable pageable, Long user_id, String mode) {
        if (mode.equals("sent")) {
            return filter_SentByUser(pageable, user_id);
        }
        else if (mode.equals("received")) {
            return filter_ReceivedByUser(pageable, user_id);
        }
        else if (mode.equals("all")) {
            return filter_All(pageable);
        }
        return null;
    }

    public Page_version2<CerereDePrietenie> filter_SentByUser(Pageable pageable, Long user_id) {
        int contor = 0;
        List<CerereDePrietenie> result = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM friend_requests WHERE first_id = " + user_id.toString() + " ORDER BY date_time LIMIT ? OFFSET ?")) {

            statement.setInt(1, pageable.getPageSize());
            statement.setInt(2, pageable.getPageSize() * pageable.getPageNumber());
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long first_id = resultSet.getLong("first_id");
                Long second_id = resultSet.getLong("second_id");
                String state = resultSet.getString("status");
                String date = resultSet.getString("date_time");

                LocalDateTime dateTime = LocalDateTime.parse(date, Constants.DATE_TIME_FORMATTER);

                CerereDePrietenie request = new CerereDePrietenie(first_id, second_id, state, dateTime);
                result.add(request);
                contor++;
            }

            //Collections.sort(result, new Comparator<CerereDePrietenie>() {
            //    @Override
            //    public int compare(CerereDePrietenie c1, CerereDePrietenie c2) {
            //        return c1.getId().getLeft().compareTo(c2.getId().getLeft()) + c1.getId().getRight().compareTo(c2.getId().getRight());
            //    }
            //});
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Page_version2<CerereDePrietenie> pageVersion2 = new Page_version2<>(result.stream(), contor);//this.size("sent_" + user_id.toString()));
        return pageVersion2;
    }

    public Page_version2<CerereDePrietenie> filter_ReceivedByUser(Pageable pageable, Long user_id) {
        int contor = 0;
        List<CerereDePrietenie> result = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM friend_requests WHERE second_id = " + user_id.toString() + " ORDER BY date_time LIMIT ? OFFSET ?")) {

            statement.setInt(1, pageable.getPageSize());
            statement.setInt(2, pageable.getPageSize() * pageable.getPageNumber());
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long first_id = resultSet.getLong("first_id");
                Long second_id = resultSet.getLong("second_id");
                String state = resultSet.getString("status");
                String date = resultSet.getString("date_time");

                LocalDateTime dateTime = LocalDateTime.parse(date, Constants.DATE_TIME_FORMATTER);

                CerereDePrietenie request = new CerereDePrietenie(first_id, second_id, state, dateTime);
                result.add(request);
                contor++;
            }

            //Collections.sort(result, new Comparator<CerereDePrietenie>() {
            //    @Override
            //    public int compare(CerereDePrietenie c1, CerereDePrietenie c2) {
            //        return c1.getId().getLeft().compareTo(c2.getId().getLeft()) + c1.getId().getRight().compareTo(c2.getId().getRight());
            //    }
            //});
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Page_version2<CerereDePrietenie> pageVersion2 = new Page_version2<>(result.stream(), contor);//this.size("received_" + user_id.toString()));
        return pageVersion2;
    }

    public Page_version2<CerereDePrietenie> filter_All(Pageable pageable) {
        int contor = 0;
        List<CerereDePrietenie> result = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM friend_requests ORDER BY date_time LIMIT ? OFFSET ?")) {

            statement.setInt(1, pageable.getPageSize());
            statement.setInt(2, pageable.getPageSize() * pageable.getPageNumber());
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long first_id = resultSet.getLong("first_id");
                Long second_id = resultSet.getLong("second_id");
                String state = resultSet.getString("status");
                String date = resultSet.getString("date_time");

                LocalDateTime dateTime = LocalDateTime.parse(date, Constants.DATE_TIME_FORMATTER);

                CerereDePrietenie request = new CerereDePrietenie(first_id, second_id, state, dateTime);
                result.add(request);
                contor++;
            }

            //Collections.sort(result, new Comparator<CerereDePrietenie>() {
            //    @Override
            //    public int compare(CerereDePrietenie c1, CerereDePrietenie c2) {
            //        return c1.getId().getLeft().compareTo(c2.getId().getLeft()) + c1.getId().getRight().compareTo(c2.getId().getRight());
            //    }
            //});
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Page_version2<CerereDePrietenie> pageVersion2 = new Page_version2<>(result.stream(), contor);//this.size("all"));
        return pageVersion2;
    }

    @Override
    public CerereDePrietenie save(CerereDePrietenie entity) {
        if (entity == null) {
            throw new IllegalArgumentException("Entitatea nu poate fi null!\n");
        }

        validator.validate(entity);
        if (exists(entity.getId())) {
            throw new RepoException("Entitatea exista deja!\n");
        }

        else {
            String query = "INSERT INTO friend_requests(first_id, second_id, status, date_time) VALUES(" + entity.getId().getLeft().toString() + ", " + entity.getId().getRight().toString() + ", '" + entity.getState() + "', '" + entity.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) + "')";
            try (Connection connection = DriverManager.getConnection(url, username, password);
                 PreparedStatement statement = connection.prepareStatement(query)) {
                statement.execute();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public CerereDePrietenie delete(Tuple<Long,Long> aLong, String mode) {
        if (aLong == null) {
            throw new IllegalArgumentException("ID-ul nu poate fi null!\n");
        }

        CerereDePrietenie request = findOne(aLong);
        if (request == null) {
            throw new RepoException("Entitatea cu ID-ul dat nu exista!\n");
        }

        String query = "DELETE FROM friend_requests WHERE first_id = " + aLong.getLeft().toString() + " AND " + "second_id = " + aLong.getRight().toString();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return request;
    }

    @Override
    public CerereDePrietenie update(CerereDePrietenie entity, String mode) {
        if(entity == null) {
            throw new IllegalArgumentException("Entitatea nu poate fi null!\n");
        }

        validator.validate(entity);
        if (!exists(entity.getId())) {
            return null;
        }

        String query = "UPDATE friend_requests SET status = '" + entity.getState() + "' WHERE first_id = " + entity.getId().getLeft().toString() + " AND " + "second_id = " + entity.getId().getRight().toString();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return entity;
    }

    @Override
    public int size(String mode) {
        List<String> attributes = Arrays.asList(mode.split("_"));

        String query = "";
        if (attributes.get(0).equals("all")) {
            query = "SELECT COUNT(*) AS total FROM friend_requests";
        }
        else if (attributes.get(0).equals("sent")) {
            query = "SELECT COUNT(*) AS total FROM friend_requests WHERE first_id = " + attributes.get(1);
        }
        else if (attributes.get(0).equals("received")) {
            query = "SELECT COUNT(*) AS total FROM friend_requests WHERE second_id = " + attributes.get(1);
        }
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {

            if (resultSet.next()) {
                int size = resultSet.getInt("total");
                return size;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public boolean exists(Tuple<Long,Long> id) {
        if (id == null) {
            throw new IllegalArgumentException("ID-ul nu poate fi null!\n");
        }

        if (findOne(id) != null) {
            return true;
        }
        return false;
    }
}
