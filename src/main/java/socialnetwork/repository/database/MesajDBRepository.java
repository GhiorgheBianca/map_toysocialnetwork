package socialnetwork.repository.database;

import socialnetwork.domain.CerereDePrietenie;
import socialnetwork.domain.Mesaj;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.RepoException;
import socialnetwork.repository.paging.*;
import socialnetwork.utils.Constants;

import java.sql.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class MesajDBRepository implements PagingRepository<Long, Mesaj> {
    private String url;
    private String username;
    private String password;
    private Validator<Mesaj> validator;

    public MesajDBRepository(String url, String username, String password, Validator<Mesaj> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    @Override
    public Mesaj findOne(Long aLong) {
        if (aLong == null) {
            throw new IllegalArgumentException("Entitatea nu poate fi null!\n");
        }

        //SELECT m.id, m.from_userid, mt.to_userid, m.text, m.date_time, m.id_reply FROM messages m INNER JOIN messages_to mt ON m.id = mt.id_message WHERE m.id =
        //String query = "SELECT m.id, m.from_userid, mt.to_userid, m.text, m.date_time, m.id_reply FROM messages m INNER JOIN messages_to mt ON m.id = mt.id_message WHERE m.id = " + aLong.toString();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT m.id, m.from_userid, mt.to_userid, m.text, m.date_time, m.id_reply FROM messages m INNER JOIN messages_to mt ON m.id = mt.id_message WHERE m.id = " + aLong.toString());
             ResultSet resultSet = statement.executeQuery()) {

            Long id_message = 0L, id_from = 0L, id_reply = 0L;
            List<Long> to_list = new ArrayList<>();
            String text = "";
            LocalDateTime dateTime = LocalDateTime.now();
            while (resultSet.next()) {
                id_message = resultSet.getLong("id");
                id_from = resultSet.getLong("from_userid");
                Long id_to = resultSet.getLong("to_userid");
                text = resultSet.getString("text");
                String date = resultSet.getString("date_time");
                id_reply = resultSet.getLong("id_reply");

                dateTime = LocalDateTime.parse(date, Constants.DATE_TIME_FORMATTER);

                to_list.add(id_to);
            }

            if (!text.equals("")) {
                Mesaj message = new Mesaj(id_message, id_from, to_list, text, dateTime, id_reply);
                return message;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Mesaj> findAll() {
        List<Mesaj> messages = new ArrayList<>();
        //"SELECT m.id, m.from_userid, mt.to_userid, m.text, m.date_time, m.id_reply FROM messages m INNER JOIN messages_to mt ON m.id = mt.id_message ORDER BY m.id LIMIT ? OFFSET ?"

        ////CREAREA VIEW-ULUI
        //CREATE VIEW messages_view AS
        //  SELECT m.id, m.from_userid, mt.to_userid, m.text, m.date_time, m.id_reply
        //  FROM messages m INNER JOIN messages_to mt ON m.id = mt.id_message
        //  ORDER BY m.id;

        //"SELECT * FROM messages_view LIMIT ? OFFSET ?"
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM messages_view ORDER BY id");
             ResultSet resultSet = statement.executeQuery()) {

            Long id_message = 0L, id_from = 0L, id_reply = 0L;
            String text = "";
            LocalDateTime dateTime = LocalDateTime.now();
            List<Long> to_list = new ArrayList<>();
            Long id_anterior = -1L;
            while (resultSet.next()) {
                id_message = resultSet.getLong("id");

                if (id_anterior.equals(-1L)) {
                    id_anterior = id_message;
                }
                if (!id_anterior.equals(id_message)) {
                    if (id_reply == 0) {
                        id_reply = null;
                    }
                    Mesaj message = new Mesaj(id_anterior, id_from, to_list, text, dateTime, id_reply);
                    messages.add(message);
                    to_list = new ArrayList<>();
                    id_anterior = id_message;
                }

                id_from = resultSet.getLong("from_userid");
                Long id_to = resultSet.getLong("to_userid");
                text = resultSet.getString("text");
                String date = resultSet.getString("date_time");
                id_reply = resultSet.getLong("id_reply");

                dateTime = LocalDateTime.parse(date, Constants.DATE_TIME_FORMATTER);

                to_list.add(id_to);
            }

            if (!text.equals("")) {
                if (id_reply == 0) {
                    id_reply = null;
                }
                Mesaj message = new Mesaj(id_anterior, id_from, to_list, text, dateTime, id_reply);
                messages.add(message);
            }

            Collections.sort(messages, new Comparator<Mesaj>() {
                @Override
                public int compare(Mesaj m1, Mesaj m2) {
                    return m1.getId().compareTo(m2.getId());
                }
            });

            return messages;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return messages;
    }

    @Override
    public Page_version2<Mesaj> findAll(Pageable pageable, Long user_id, String mode) {
        List<String> attributes = Arrays.asList(mode.split("_"));

        if (attributes.get(0).equals("sent")) {
            return filter_SentByUser(pageable, user_id);
        }
        else if (attributes.get(0).equals("received")) {
            return filter_ReceivedByUser(pageable, user_id);
        }
        else if (attributes.get(0).equals("all")) {
            return filter_All(pageable);
        }
        else if (attributes.get(0).equals("messages")) {
            return filter_Conversation(pageable, user_id.toString(), attributes.get(1));
        }
        return null;
    }

    public Page_version2<Mesaj> filter_SentByUser(Pageable pageable, Long user_id) {
        int contor = 0;
        List<Mesaj> result = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM messages WHERE from_userid = " + user_id.toString() + " ORDER BY date_time DESC LIMIT ? OFFSET ?")) {

            statement.setInt(1, pageable.getPageSize());
            statement.setInt(2, pageable.getPageSize() * pageable.getPageNumber());
            ResultSet resultSet = statement.executeQuery();

            Long id_message = 0L, id_from = 0L, id_reply = 0L;
            String text = "";
            LocalDateTime dateTime = LocalDateTime.now();
            List<Long> to_list = new ArrayList<>();
            while (resultSet.next()) {
                id_message = resultSet.getLong("id");

                id_from = resultSet.getLong("from_userid");
                text = resultSet.getString("text");
                String date = resultSet.getString("date_time");
                id_reply = resultSet.getLong("id_reply");

                dateTime = LocalDateTime.parse(date, Constants.DATE_TIME_FORMATTER);

                try (Connection second_connection = DriverManager.getConnection(url, username, password);
                     PreparedStatement second_statement = second_connection.prepareStatement("SELECT * FROM messages_to WHERE id_message = " + id_message.toString());
                     ResultSet second_resultSet = second_statement.executeQuery()) {

                    to_list = new ArrayList<>();

                    while (second_resultSet.next()) {
                        Long id_toUser = second_resultSet.getLong("to_userid");

                        to_list.add(id_toUser);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }

                if (id_reply == 0) {
                    id_reply = null;
                }
                Mesaj message = new Mesaj(id_message, id_from, to_list, text, dateTime, id_reply);
                result.add(message);
                contor++;
            }

            //Collections.sort(result, new Comparator<Mesaj>() {
            //    @Override
            //    public int compare(Mesaj m1, Mesaj m2) {
            //        return m1.getId().compareTo(m2.getId());
            //    }
            //});
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Page_version2<Mesaj> pageVersion2 = new Page_version2<>(result.stream(), contor);
        return pageVersion2;
    }

    public Page_version2<Mesaj> filter_ReceivedByUser(Pageable pageable, Long user_id) {
        int contor = 0;
        List<Mesaj> result = new ArrayList<>();
        String query = "SELECT * FROM messages WHERE id IN (SELECT id_message FROM messages_to WHERE to_userid = " + user_id.toString() + ") ORDER BY date_time DESC LIMIT ? OFFSET ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setInt(1, pageable.getPageSize());
            statement.setInt(2, pageable.getPageSize() * pageable.getPageNumber());
            ResultSet resultSet = statement.executeQuery();

            Long id_message = 0L, id_from = 0L, id_reply = 0L;
            String text = "";
            LocalDateTime dateTime = LocalDateTime.now();
            List<Long> to_list = new ArrayList<>();
            while (resultSet.next()) {
                id_message = resultSet.getLong("id");

                id_from = resultSet.getLong("from_userid");
                text = resultSet.getString("text");
                String date = resultSet.getString("date_time");
                id_reply = resultSet.getLong("id_reply");

                dateTime = LocalDateTime.parse(date, Constants.DATE_TIME_FORMATTER);

                try (Connection second_connection = DriverManager.getConnection(url, username, password);
                     PreparedStatement second_statement = second_connection.prepareStatement("SELECT * FROM messages_to WHERE id_message = " + id_message.toString());
                     ResultSet second_resultSet = second_statement.executeQuery()) {

                    to_list = new ArrayList<>();

                    while (second_resultSet.next()) {
                        Long id_toUser = second_resultSet.getLong("to_userid");

                        to_list.add(id_toUser);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }

                if (id_reply == 0) {
                    id_reply = null;
                }
                Mesaj message = new Mesaj(id_message, id_from, to_list, text, dateTime, id_reply);
                result.add(message);
                contor++;
            }

            //Collections.sort(result, new Comparator<Mesaj>() {
            //    @Override
            //    public int compare(Mesaj m1, Mesaj m2) {
            //        return m1.getId().compareTo(m2.getId());
            //    }
            //});
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Page_version2<Mesaj> pageVersion2 = new Page_version2<>(result.stream(), contor);
        return pageVersion2;
    }

    public Page_version2<Mesaj> filter_All(Pageable pageable) {
        int contor = 0;
        List<Mesaj> result = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM messages ORDER BY date_time DESC LIMIT ? OFFSET ?")) {

            statement.setInt(1, pageable.getPageSize());
            statement.setInt(2, pageable.getPageSize() * pageable.getPageNumber());
            ResultSet resultSet = statement.executeQuery();

            Long id_message = 0L, id_from = 0L, id_reply = 0L;
            String text = "";
            LocalDateTime dateTime = LocalDateTime.now();
            List<Long> to_list = new ArrayList<>();
            while (resultSet.next()) {
                id_message = resultSet.getLong("id");

                id_from = resultSet.getLong("from_userid");
                text = resultSet.getString("text");
                String date = resultSet.getString("date_time");
                id_reply = resultSet.getLong("id_reply");

                dateTime = LocalDateTime.parse(date, Constants.DATE_TIME_FORMATTER);

                try (Connection second_connection = DriverManager.getConnection(url, username, password);
                     PreparedStatement second_statement = second_connection.prepareStatement("SELECT * FROM messages_to WHERE id_message = " + id_message.toString());
                     ResultSet second_resultSet = second_statement.executeQuery()) {

                    to_list = new ArrayList<>();

                    while (second_resultSet.next()) {
                        Long id_toUser = second_resultSet.getLong("to_userid");

                        to_list.add(id_toUser);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }

                if (id_reply == 0) {
                    id_reply = null;
                }
                Mesaj message = new Mesaj(id_message, id_from, to_list, text, dateTime, id_reply);
                result.add(message);
                contor++;
            }

            //Collections.sort(result, new Comparator<Mesaj>() {
            //    @Override
            //    public int compare(Mesaj m1, Mesaj m2) {
            //        return m1.getId().compareTo(m2.getId());
            //    }
            //});
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Page_version2<Mesaj> pageVersion2 = new Page_version2<>(result.stream(), contor);//this.size("all"));
        return pageVersion2;
    }

    public Page_version2<Mesaj> filter_Conversation(Pageable pageable, String user_id, String id) {
        int contor = 0;
        List<Mesaj> result = new ArrayList<>();
        String query = "SELECT * FROM messages WHERE id IN (SELECT id_message FROM messages_to WHERE to_userid = " + user_id + " OR to_userid = " + id + ") AND (from_userid = " + user_id + " OR from_userid = " + id + ") ORDER BY date_time ASC LIMIT ? OFFSET ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)) {

            statement.setInt(1, pageable.getPageSize());
            statement.setInt(2, pageable.getPageSize() * pageable.getPageNumber());
            ResultSet resultSet = statement.executeQuery();

            Long id_message = 0L, id_from = 0L, id_reply = 0L;
            String text = "";
            LocalDateTime dateTime = LocalDateTime.now();
            List<Long> to_list = new ArrayList<>();
            while (resultSet.next()) {
                id_message = resultSet.getLong("id");

                id_from = resultSet.getLong("from_userid");
                text = resultSet.getString("text");
                String date = resultSet.getString("date_time");
                id_reply = resultSet.getLong("id_reply");

                dateTime = LocalDateTime.parse(date, Constants.DATE_TIME_FORMATTER);

                try (Connection second_connection = DriverManager.getConnection(url, username, password);
                     PreparedStatement second_statement = second_connection.prepareStatement("SELECT * FROM messages_to WHERE id_message = " + id_message.toString());
                     ResultSet second_resultSet = second_statement.executeQuery()) {

                    to_list = new ArrayList<>();

                    while (second_resultSet.next()) {
                        Long id_toUser = second_resultSet.getLong("to_userid");

                        to_list.add(id_toUser);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                }

                if (id_reply == 0) {
                    id_reply = null;
                }
                Mesaj message = new Mesaj(id_message, id_from, to_list, text, dateTime, id_reply);
                result.add(message);
                contor++;
            }

            //Collections.sort(result, new Comparator<Mesaj>() {
            //    @Override
            //    public int compare(Mesaj m1, Mesaj m2) {
            //        return m1.getId().compareTo(m2.getId());
            //    }
            //});
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Page_version2<Mesaj> pageVersion2 = new Page_version2<>(result.stream(), contor);
        return pageVersion2;
    }

    @Override
    public Mesaj save(Mesaj entity) {
        if (entity == null) {
            throw new IllegalArgumentException("Entitatea nu poate fi null!\n");
        }

        validator.validate(entity);
        if (exists(entity.getId())) {
            throw new RepoException("Entitatea exista deja!\n");
        }

        else {
            String query_messages = "";
            if (entity.getId_reply() != null) {
                query_messages = "INSERT INTO messages(id, from_userid, text, date_time, id_reply) VALUES(" + entity.getId().toString() + ", " + entity.getFrom().toString() + ", '" + entity.getMessage() + "', '" + entity.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) + "', " + entity.getId_reply().toString() + ")";
            }
            else {
                query_messages = "INSERT INTO messages(id, from_userid, text, date_time) VALUES(" + entity.getId().toString() + ", " + entity.getFrom().toString() + ", '" + entity.getMessage() + "', '" + entity.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) + "')";
            }
            try (Connection connection = DriverManager.getConnection(url, username, password);
                 PreparedStatement statement = connection.prepareStatement(query_messages)) {
                statement.execute();

            } catch (SQLException e) {
                e.printStackTrace();
            }

            for (Long to : entity.getTo()) {
                String query_messages_to = "INSERT INTO messages_to(id_message, to_userid) VALUES(" + entity.getId().toString() + ", " + to.toString() + ")";
                try (Connection connection = DriverManager.getConnection(url, username, password);
                     PreparedStatement statement = connection.prepareStatement(query_messages_to)) {
                    statement.execute();

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    @Override
    public Mesaj delete(Long aLong, String mode) {
        if (aLong == null) {
            throw new IllegalArgumentException("ID-ul nu poate fi null!\n");
        }

        Mesaj mesaj = findOne(aLong);
        if (mesaj == null) {
            throw new RepoException("Entitatea cu ID-ul dat nu exista!\n");
        }

        String query = "DELETE FROM messages WHERE id = " + aLong.toString();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return mesaj;
    }

    @Override
    public Mesaj update(Mesaj entity, String mode) {
        if (entity == null) {
            throw new IllegalArgumentException("Entitatea nu poate fi null!\n");
        }

        validator.validate(entity);
        if (!exists(entity.getId())) {
            return entity;
        }

        String query = "UPDATE messages SET text = '" + entity.getMessage() + "' WHERE id = '" + entity.getId().toString() + "'";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public int size(String mode) {
        List<String> attributes = Arrays.asList(mode.split("_"));

        String query = "";
        if (attributes.get(0).equals("all")) {
            query = "SELECT COUNT(*) AS total FROM messages";
        }
        else if (attributes.get(0).equals("sent")) {
            query = "SELECT COUNT(*) AS total FROM messages WHERE from_userid = " + attributes.get(1);
        }
        else if (attributes.get(0).equals("received")) {
            query = "SELECT COUNT(*) AS total FROM messages WHERE id IN (SELECT id_message FROM messages_to WHERE to_userid = " + attributes.get(1) + ")";
        }
        else if(attributes.get(0).equals("messages")) {
            query = "SELECT COUNT(*) AS total FROM messages WHERE id IN (SELECT id_message FROM messages_to WHERE to_userid = " + attributes.get(1) + " OR to_userid = " + attributes.get(2) + ") AND (from_userid = " + attributes.get(1) + " OR from_userid = " + attributes.get(2) + ")";
        }
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query);
             ResultSet resultSet = statement.executeQuery()) {

            if (resultSet.next()) {
                int size = resultSet.getInt("total");
                return size;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public boolean exists(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("ID-ul nu poate fi null!\n");
        }

        if (findOne(id) != null) {
            return true;
        }
        return false;
    }
}
