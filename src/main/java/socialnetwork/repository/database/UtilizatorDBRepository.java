package socialnetwork.repository.database;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.RepoException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Page_version2;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.util.*;

public class UtilizatorDBRepository implements PagingRepository<Long, Utilizator> {
    private String url;
    private String username;
    private String password;
    private Validator<Utilizator> validator;

    public UtilizatorDBRepository(String url, String username, String password, Validator<Utilizator> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    @Override
    public Utilizator findOne(Long aLong) {
        if (aLong == null) {
            throw new IllegalArgumentException("entity must not be null!\n");
        }

        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE id = " + aLong.toString());
             ResultSet resultSet = statement.executeQuery()) {

            if (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String email = resultSet.getString("email");
                String password = resultSet.getString("user_password");

                Utilizator utilizator = new Utilizator(firstName, lastName, email, password);
                utilizator.setId(id);

                return utilizator;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Utilizator> findAll() {
        Set<Utilizator> users = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM users ORDER BY first_name, last_name");
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String email = resultSet.getString("email");
                String password = resultSet.getString("user_password");

                Utilizator utilizator = new Utilizator(firstName, lastName, email, password);
                utilizator.setId(id);

                users.add(utilizator);
            }

            return users;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    @Override
    public Page_version2<Utilizator> findAll(Pageable pageable, Long user_id, String mode) {
        int contor = 0;
        List<Utilizator> result = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * FROM users ORDER BY first_name, last_name LIMIT ? OFFSET ?")) {

            statement.setInt(1, pageable.getPageSize());
            statement.setInt(2, pageable.getPageSize() * pageable.getPageNumber());
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String email = resultSet.getString("email");
                String password = resultSet.getString("user_password");

                Utilizator utilizator = new Utilizator(firstName, lastName, email, password);
                utilizator.setId(id);

                result.add(utilizator);
                contor++;
            }

            //Collections.sort(result, new Comparator<Utilizator>() {
            //    @Override
            //    public int compare(Utilizator u1, Utilizator u2) {
            //        return u1.getId().compareTo(u2.getId());
            //    }
            //});
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Page_version2<Utilizator> pageVersion2 = new Page_version2<>(result.stream(), contor);//this.size("all"));
        return pageVersion2;
    }

    @Override
    public Utilizator save(Utilizator entity) {
        if (entity == null)
            throw new IllegalArgumentException("entity must not be null!\n");

        validator.validate(entity);
        if (exists(entity.getId()))
            throw new RepoException("entity already exists!\n");

        else {
            String query = "INSERT INTO users(id, first_name, last_name, email, user_password) VALUES(" + entity.getId().toString() + ", '" + entity.getFirstName() + "', '" + entity.getLastName() + "', '" + entity.getEmail() + "', '" + entity.getPassword() + "')";
            try (Connection connection = DriverManager.getConnection(url, username, password);
                 PreparedStatement statement = connection.prepareStatement(query)) {
                statement.execute();

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public Utilizator delete(Long aLong, String mode) {
        if (aLong == null) {
            throw new IllegalArgumentException("id must not be null!\n");
        }

        Utilizator user = findOne(aLong);
        if (user == null) {
            throw new RepoException("the entity with the given id does not exists!\n");
        }

        String query = "DELETE FROM users WHERE id = " + aLong.toString();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    @Override
    public Utilizator update(Utilizator entity, String mode) {
        if(entity == null) {
            throw new IllegalArgumentException("entity must not be null!\n");
        }

        validator.validate(entity);
        if (!exists(entity.getId())) {
            return entity;
        }

        String query = "UPDATE users SET first_name = '" + entity.getFirstName() + "', last_name = '" + entity.getLastName() + "', email = '" + entity.getEmail() + "', user_password = '" + entity.getPassword() + "' WHERE id = '" + entity.getId().toString() + "'";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(query)) {
            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public int size(String mode) {
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT COUNT(*) AS total FROM users");
             ResultSet resultSet = statement.executeQuery()) {

            if (resultSet.next()) {
                int size = resultSet.getInt("total");
                return size;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public boolean exists(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("id must not be null!\n");
        }

        if (findOne(id) != null) {
            return true;
        }
        return false;
    }
}
