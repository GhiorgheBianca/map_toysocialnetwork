package socialnetwork.repository.memory;

import socialnetwork.domain.Entity;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.RepoException;
import socialnetwork.repository.Repository;

import java.util.HashMap;
import java.util.Map;

public class InMemoryRepository<ID, E extends Entity<ID>> implements Repository<ID,E> {

    protected Validator<E> validator;
    Map<ID,E> entities;

    public InMemoryRepository(Validator<E> validator) {
        this.validator = validator;
        entities=new HashMap<ID,E>();
    }

    @Override
    public E findOne(ID id){
        if (id == null) {
            throw new IllegalArgumentException("ID-ul nu poate fi null!\n");
        }
        return entities.get(id);
    }

    @Override
    public Iterable<E> findAll() {
        return entities.values();
    }

    @Override
    public int size(String mode) {
        return entities.size();
    }

    @Override
    public E save(E entity) {
        if (entity==null)
            throw new IllegalArgumentException("Entitatea nu poate fi null!\n");

        validator.validate(entity);
        if (exists(entity.getId()))
            throw new RepoException("Entitatea exista deja!\n");

        if(entities.get(entity.getId()) != null) {
            return entity;
        }
        else entities.put(entity.getId(),entity);
        return null;
    }

    @Override
    public E delete(ID id, String mode) {
        if (id == null) {
            throw new IllegalArgumentException("ID-ul nu poate fi null!\n");
        }
        if (!exists(id)) {
            throw new RepoException("Entitatea cu ID-ul dat nu exista!\n");
        }

        return entities.remove(id);
    }

    @Override
    public E update(E entity, String mode) {

        if(entity == null)
            throw new IllegalArgumentException("Entitatea nu poate fi null!\n");
        validator.validate(entity);

        //entities.put(entity.getId(),entity);

        if(entities.get(entity.getId()) != null) {
            //entities.put(entity.getId(),entity);
            return null;
        }

        entities.put(entity.getId(),entity);
        return entity;

    }

    @Override
    public boolean exists(ID id) {
        if (id == null) {
            throw new IllegalArgumentException("Entitatea nu poate fi null!\n");
        }

        if (findOne(id) != null) {
            return true;
        }
        return false;
    }

}
