package socialnetwork.ui;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.RepoException;
import socialnetwork.service.ServiceException;
import socialnetwork.service.SuperService;
import socialnetwork.utils.Constants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UI {
    private SuperService service;
    private BufferedReader reader = null;

    public UI(SuperService service) {
        this.service = service;
    }

    private void adaugaUtilizator() throws Exception {
        System.out.println("ID: ");
        String data = reader.readLine();
        Long id = Long.parseLong(data);

        System.out.println("Nume: ");
        String nume = reader.readLine();

        System.out.println("Prenume: ");
        String prenume = reader.readLine();

        System.out.println("Email: ");
        String email = reader.readLine();

        System.out.println("Parola: ");
        String parola = reader.readLine();

        Utilizator user = new Utilizator(id, nume, prenume, email, parola);
        service.getUtilizatorService().addUtilizator(user);

        System.out.println("Adaugare reusita!\n");
        System.out.println();
    }

    private void stergeUtilizator() throws Exception {
        System.out.println("ID: ");
        String data = reader.readLine();
        Long id = Long.parseLong(data);

        service.getUtilizatorService().deleteUtilizator(id);
        System.out.println("Stergere reusita!\n");
        System.out.println();
    }

    private void adaugaPrietenie() throws Exception {
        System.out.println("ID-ul primului prieten: ");
        String data = reader.readLine();
        Long id1 = Long.parseLong(data);

        System.out.println("ID-ul celui de-al doilea prieten: ");
        data = reader.readLine();
        Long id2 = Long.parseLong(data);

        Prietenie friendship = new Prietenie(id1, id2);
        service.getPrietenieService().addFriendship(friendship);

        System.out.println("Adaugare reusita!\n");
        System.out.println();
    }

    private void stergePrietenie() throws Exception {
        System.out.println("ID-ul primului prieten: ");
        String data = reader.readLine();
        Long id1 = Long.parseLong(data);

        System.out.println("ID-ul celui de-al doilea prieten: ");
        data = reader.readLine();
        Long id2 = Long.parseLong(data);

        service.getPrietenieService().deleteFriendship(new Tuple<Long, Long>(id1, id2));
        System.out.println("Stergere reusita!\n");
        System.out.println();
    }

    private void printeazaUtilizatori() {
        service.getUtilizatorService().printUsers();
        System.out.println();
    }

    private void printeazaPrietenii() {
        service.getPrietenieService().printFriendships();
        System.out.println();
    }

    private void afiseazaNrComunitati() {
        System.out.println("Numarul de comunitati: " + service.getUtilizatorService().numberOfCommunities());
        System.out.println();
    }

    private void afiseazaCeaMaiSociabilaComunitate() {
        System.out.println("Cea mai sociabila comunitate: ");
        service.getUtilizatorService().ceaMaiSociabilaComunitate().forEach(System.out::println);
        System.out.println();
    }

    private void afiseazaRelatiileDePrietenie() throws Exception {
        System.out.println("ID: ");
        String data = reader.readLine();
        Long id = Long.parseLong(data);

        /*List<Prietenie> prietenieList = new ArrayList<>();
        utilizatorService.getAllFriendships().forEach(x -> prietenieList.add(x));

        prietenieList.stream()
                .filter(x -> x.getId().getLeft().equals(id))
                .map(y -> {
                    Utilizator user = utilizatorService.findUser_id(y.getId().getRight());
                    String info = user.getFirstName() + " | " + user.getLastName() + " | " + y.getDate().format(Constants.DATE_TIME_FORMATTER);
                    return info;
                })
                .forEach(System.out::println);

        prietenieList.stream()
                .filter(x -> x.getId().getRight().equals(id))
                .map(y -> {
                    Utilizator user = utilizatorService.findUser_id(y.getId().getLeft());
                    String info = user.getFirstName() + " | " + user.getLastName() + " | " + y.getDate().format(Constants.DATE_TIME_FORMATTER);
                    return info;
                })
                .forEach(System.out::println);*/

        service.getUtilizatorService().getFriends_forUser(id).forEach(System.out::println);
        System.out.println();
    }

    private void afiseazaRelatiileDePrietenieDupaLuna() throws Exception {
        System.out.println("ID: ");
        String data = reader.readLine();
        Long id = Long.parseLong(data);

        System.out.println("Luna: ");
        String luna = reader.readLine();
        int nr_luna = Integer.parseInt(luna);

        try {
            service.getUtilizatorService().getFriendsByMonth_forUser(id, Month.of(nr_luna)).forEach(System.out::println);
        }
        catch (Exception e) {
            throw new ValidationException("Nu exista luna mentionata! Trebuie sa introduceti o valoare intre 1 si 12.");
        }
        System.out.println();
    }

    private void trimiteCerere() throws Exception {
        System.out.println("Conecteaza-te cu ID-ul: ");
        String data = reader.readLine();
        Long first_id = Long.parseLong(data);

        System.out.println("Trimite cererea catre ID-ul: ");
        data = reader.readLine();
        Long second_id = Long.parseLong(data);

        service.getCerereService().sendRequest(first_id, second_id);
        System.out.println("Cererea a fost trimisa!");
        System.out.println();
    }

    private void afiseazaCererile() throws Exception {
        System.out.println("Conecteaza-te cu ID-ul: ");
        String data = reader.readLine();
        Long id = Long.parseLong(data);

        service.getCerereService().getRequests_receivedByUser(id).forEach(System.out::println);
        System.out.println();
    }

    private void raspundeCerere() throws Exception {
        System.out.println("Conecteaza-te cu ID-ul: ");
        String data = reader.readLine();
        Long first_id = Long.parseLong(data);

        System.out.println("Cerere de la utilizatorul cu ID-ul: ");
        data = reader.readLine();
        Long second_id = Long.parseLong(data);

        System.out.println("Raspuns (approved/rejected): ");
        String raspuns = reader.readLine();

        service.getCerereService().answerRequest(first_id, second_id, raspuns);
        if (raspuns.equals("approved")) {
            System.out.println("Ati acceptat cererea de prietenie!");
        }
        else {
            System.out.println("Ati respins cererea de prietenie!");
        }
        System.out.println();
    }

    private void trimiteMesaj() throws Exception {
        System.out.println("Conecteaza-te cu ID-ul: ");
        String data = reader.readLine();
        Long from = Long.parseLong(data);

        System.out.println("Trimite mesajul catre ID-urile (separate prin virgula): ");
        data = reader.readLine();
        List<String> to_String = Arrays.asList(data.split(","));
        List<Long> to_Long = new ArrayList<>();
        for (String to : to_String) {
            to_Long.add(Long.parseLong(to));
        }

        System.out.println("Textul mesajului: ");
        data = reader.readLine();

        service.getMesajService().sendNewMessage(from, to_Long, data);
        System.out.println("Mesajul a fost trimis!");
        System.out.println();
    }

    private void raspundeMesaj() throws Exception {
        System.out.println("Conecteaza-te cu ID-ul: ");
        String data = reader.readLine();
        Long id_utilizator = Long.parseLong(data);

        System.out.println("Raspunde la mesajul cu ID-ul: ");
        data = reader.readLine();
        Long id_mesaj = Long.parseLong(data);

        System.out.println("Textul mesajului: ");
        data = reader.readLine();

        service.getMesajService().respondMessage(id_utilizator, id_mesaj, data);
        System.out.println("Mesajul a fost trimis!");
        System.out.println();
    }

    private void raspundeTuturorMesaj() throws Exception {
        System.out.println("Conecteaza-te cu ID-ul: ");
        String data = reader.readLine();
        Long id_utilizator = Long.parseLong(data);

        System.out.println("Raspunde la mesajul cu ID-ul: ");
        data = reader.readLine();
        Long id_mesaj = Long.parseLong(data);

        System.out.println("Textul mesajului: ");
        data = reader.readLine();

        service.getMesajService().respondAllMessage(id_utilizator, id_mesaj, data);
        System.out.println("Mesajul a fost trimis!");
        System.out.println();
    }

    private void afiseazaConversatie() throws Exception {
        System.out.println("Conecteaza-te cu ID-ul: ");
        String data = reader.readLine();
        Long id_utilizator = Long.parseLong(data);

        System.out.println("Conversatia cu utilizatorul cu ID-ul: ");
        data = reader.readLine();
        Long id_receptor = Long.parseLong(data);

        service.getMesajService().getConversation(id_utilizator, id_receptor).forEach(System.out::println);
        System.out.println();
    }

    private void afiseazaToateMesajele() {
        service.getMesajService().getAllMessages().forEach(System.out::println);
        System.out.println();
    }

    private void main_menu() {
        System.out.println("1. Adauga un Utilizator.");
        System.out.println("2. Sterge un Utilizator.");
        System.out.println("3. Adauga o Prietenie.");
        System.out.println("4. Sterge o Prietenie.");
        System.out.println("5. Afiseaza toti Utilizatorii.");
        System.out.println("6. Afiseaza toate Prieteniile.");
        System.out.println("7. Afiseaza numarul de comunitati.");
        System.out.println("8. Afiseaza membrii celei mai sociabile comunitati.");
        System.out.println("9. Afiseaza toate relatiile de prietenie ale unui utilizator.");
        System.out.println("10. Afiseaza toate relatiile de prietenie ale unui utilizator, create intr-o anumita luna a anului.");
        System.out.println("11. Trimite o cerere de prietenie.");
        System.out.println("12. Afiseaza toate cererile de prietenie primite de un anumit utilizator.");
        System.out.println("13. Raspunde unei cereri de prietenie.");
        System.out.println("14. Trimite un mesaj.");
        System.out.println("15. Raspunde unui mesaj.");
        System.out.println("16. Raspunde unui mesaj catre toti.");
        System.out.println("17. Afiseaza conversatia cu un anumit utilizator in ordine cronologica.");
        System.out.println("18. Afiseaza toate mesajele stocate.");
        System.out.println("0. Exit.");
    }

    public void run() throws Exception {
        while(true) {
            try {
                reader = new BufferedReader(new InputStreamReader(System.in));

                this.main_menu();
                System.out.println("Comanda: ");
                String cmd = reader.readLine();
                Integer cmdInt = Integer.parseInt(cmd);

                /*switch (cmdInt) {
                    case 1: //Adauga un Utilizator
                        this.adaugaUtilizator();
                    case 2: //Sterge un Utilizator
                        this.stergeUtilizator();
                    case 3: //Adauga o Prietenie
                        this.adaugaPrietenie();
                    case 4: //Sterge o Prietenie
                        this.stergePrietenie();
                    case 5: //Afiseaza toti Utilizatorii
                        this.printeazaUtilizatori();
                    case 6: //Afiseaza toate Prieteniile
                        this.printeazaPrietenii();
                    case 7: //Exit
                        reader.close();
                        return;
                }*/

                if (cmdInt == 1) {
                    //Adauga un Utilizator
                    this.adaugaUtilizator();
                }
                else if (cmdInt == 2) {
                    //Sterge un Utilizator
                    this.stergeUtilizator();
                }
                else if (cmdInt == 3) {
                    //Adauga o Prietenie
                    this.adaugaPrietenie();
                }
                else if (cmdInt == 4) {
                    //Sterge o Prietenie
                    this.stergePrietenie();
                }
                else if (cmdInt == 5) {
                    //Afiseaza toti Utilizatorii
                    this.printeazaUtilizatori();
                }
                else if (cmdInt == 6) {
                    //Afiseaza toate Prieteniile
                    this.printeazaPrietenii();
                }
                else if (cmdInt == 7) {
                    //Afiseaza numarul de comunitati
                    this.afiseazaNrComunitati();
                }
                else if (cmdInt == 8) {
                    //Afiseaza membrii celei mai sociabile comunitati
                    this.afiseazaCeaMaiSociabilaComunitate();
                }
                else if (cmdInt == 9) {
                    //Afiseaza toate relatiile de prietenie ale unui utilizator citit de la tastatura
                    this.afiseazaRelatiileDePrietenie();
                }
                else if (cmdInt == 10) {
                    //Afiseaza toate relatiile de prietenie ale unui utilizator create intr-o anumita luna a anului
                    this.afiseazaRelatiileDePrietenieDupaLuna();
                }
                else if (cmdInt == 11) {
                    //Trimite o cerere de prietenie
                    this.trimiteCerere();
                }
                else if (cmdInt == 12) {
                    //Afiseaza toate cererile de prietenie primite de un anumit utilizator
                    this.afiseazaCererile();
                }
                else if (cmdInt == 13) {
                    //Raspunde unei cereri de prietenie
                    this.raspundeCerere();
                }
                else if (cmdInt == 14) {
                    //Trimite un mesaj
                    this.trimiteMesaj();
                }
                else if (cmdInt == 15) {
                    //Raspunde unui mesaj
                    this.raspundeMesaj();
                }
                else if (cmdInt == 16) {
                    //Raspunde unui mesaj catre toti
                    this.raspundeTuturorMesaj();
                }
                else if (cmdInt == 17) {
                    //Afiseaza conversatia cu un anumit utilizator in ordine cronologica
                    this.afiseazaConversatie();
                }
                else if (cmdInt == 18) {
                    //Afiseaza toate mesajele stocate
                    this.afiseazaToateMesajele();
                }
                else if (cmdInt == 0) {
                    //Exit
                    reader.close();
                    return;
                }
            }
            catch (IOException e) {
                System.err.println("IOException: " + e.getMessage());
            }
            catch (ValidationException e) {
                System.err.println("ValidationException: " + e.getMessage());
            }
            catch (RepoException e) {
                System.err.println("RepoException: " + e.getMessage());
            }
            catch (ServiceException e) {
                System.err.println("ServiceException: " + e.getMessage());
            }
            catch (SQLException e) {
                System.err.println("SQLException: " + e.getMessage());
            }
            catch (Exception e) {
                System.err.println("Exception: " + e.getMessage());
            }
        }
    }

}
