package socialnetwork;

import socialnetwork.config.ApplicationContext;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.CerereValidator;
import socialnetwork.domain.validators.MesajValidator;
import socialnetwork.domain.validators.PrietenieValidator;
import socialnetwork.domain.validators.UtilizatorValidator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.CerereDBRepository;
import socialnetwork.repository.database.MesajDBRepository;
import socialnetwork.repository.database.PrietenieDBRepository;
import socialnetwork.repository.database.UtilizatorDBRepository;
import socialnetwork.repository.file.CerereFile;
import socialnetwork.repository.file.MesajFile;
import socialnetwork.repository.file.PrietenieFile;
import socialnetwork.repository.file.UtilizatorFile;
import socialnetwork.repository.memory.InMemoryRepository;
import socialnetwork.service.*;
import socialnetwork.ui.UI;

public class Main {

    public static void main(String[] args) throws Exception {
        ////REPO IN FISIER
        //String fileName=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.users");
        ////String fileName="data/users.csv";
        //Repository<Long,Utilizator> userFileRepository = new UtilizatorFile(fileName, new UtilizatorValidator());

        //String fileName_prietenii=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.friendships");
        ////String fileName_prietenii="data/friendships.csv";
        //Repository<Tuple<Long,Long>, Prietenie> prietenieFileRepository = new PrietenieFile(fileName_prietenii, new PrietenieValidator(userFileRepository));

        //String fileName_cereri=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.requests");
        ////String fileName_cereri="data/friend_requests.csv";
        //Repository<Tuple<Long,Long>, CerereDePrietenie> requestFileRepository = new CerereFile(fileName_cereri, new CerereValidator(userFileRepository, prietenieFileRepository));

        //String fileName_mesaje=ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.messages");
        ////String fileName_mesaje="data/messages.csv";
        //Repository<Long, Mesaj> mesajFileRepository = new MesajFile(fileName_mesaje, new MesajValidator(userFileRepository));

        //UtilizatorService service = new UtilizatorService(userFileRepository, prietenieFileRepository, requestFileRepository, mesajFileRepository);
        ////

        ////REPO IN MEMORIE
        //Repository<Long,Utilizator> userRepository = new InMemoryRepository<Long,Utilizator>(new UtilizatorValidator());
        //Repository<Tuple<Long,Long>,Prietenie> friendshipRepository = new InMemoryRepository<Tuple<Long,Long>, Prietenie>(new PrietenieValidator(userRepository));

        //UtilizatorService service = new UtilizatorService(userRepository, friendshipRepository);
        ////

        ////REPO IN BAZA DE DATE
        //final String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
        //final String username= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username");
        //final String pasword= ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password");
        //Repository<Long,Utilizator> userDBRepository = new UtilizatorDBRepository(url, username, pasword, new UtilizatorValidator());

        //PrietenieValidator prietenieValidator = new PrietenieValidator(userDBRepository);
        //Repository<Tuple<Long,Long>,Prietenie> friendshipDBRepository = new PrietenieDBRepository(url, username, pasword, prietenieValidator);
        //prietenieValidator.setRepoPrietenie(friendshipDBRepository);

        //Repository<Long,Mesaj> messageDBRepository = new MesajDBRepository(url, username, pasword, new MesajValidator(userDBRepository));

        //CerereValidator cerereValidator = new CerereValidator(userDBRepository, friendshipDBRepository);
        //Repository<Tuple<Long,Long>,CerereDePrietenie> requestDBRepository = new CerereDBRepository(url, username, pasword, cerereValidator);
        //cerereValidator.setRepoCerere(requestDBRepository);

        //UtilizatorService utilizatorService = new UtilizatorService(userDBRepository);
        //PrietenieService prietenieService = new PrietenieService(friendshipDBRepository);
        //CerereService cerereService = new CerereService(requestDBRepository);
        //MesajService mesajService = new MesajService(messageDBRepository);

        //SuperService service = new SuperService(utilizatorService, prietenieService, cerereService, mesajService);

        ////SETAREA SERVICE-URILOR DIN FIECARE SERVICE
        //service.getUtilizatorService().setPrietenieService(service.getPrietenieService());
        //service.getUtilizatorService().setCerereService(service.getCerereService());

        //service.getCerereService().setUtilizatorService(service.getUtilizatorService());
        //service.getCerereService().setPrietenieService(service.getPrietenieService());

        //service.getMesajService().setUtilizatorService(service.getUtilizatorService());
        ////

        //UI ui = new UI(service);
        //ui.run();

        MainApp.main(args);
    }

}


