package socialnetwork.utils.events;

import socialnetwork.domain.CerereDePrietenie;
import socialnetwork.domain.Prietenie;

public class FriendRequestChangeEvent implements Event {
    private ChangeEventType type;
    private CerereDePrietenie data, oldData;

    public FriendRequestChangeEvent(ChangeEventType type, CerereDePrietenie data) {
        this.type = type;
        this.data = data;
    }
    public FriendRequestChangeEvent(ChangeEventType type, CerereDePrietenie data, CerereDePrietenie oldData) {
        this.type = type;
        this.data = data;
        this.oldData = oldData;
    }

    public ChangeEventType getType() {
        return type;
    }

    public CerereDePrietenie getData() {
        return data;
    }

    public CerereDePrietenie getOldData() {
        return oldData;
    }
}