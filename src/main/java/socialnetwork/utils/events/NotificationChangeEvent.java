package socialnetwork.utils.events;

import socialnetwork.domain.Mesaj;
import socialnetwork.domain.Notificare;

public class NotificationChangeEvent implements Event {
    private ChangeEventType type;
    private Notificare data, oldData;

    public NotificationChangeEvent(ChangeEventType type, Notificare data) {
        this.type = type;
        this.data = data;
    }
    public NotificationChangeEvent(ChangeEventType type, Notificare data, Notificare oldData) {
        this.type = type;
        this.data = data;
        this.oldData = oldData;
    }

    public ChangeEventType getType() {
        return type;
    }

    public Notificare getData() {
        return data;
    }

    public Notificare getOldData() {
        return oldData;
    }
}
