package socialnetwork.utils.graph_algorithms;

import java.util.*;

public class FriendshipsGraph {

    private int number_of_vertices;
    private int number_of_edges;
    private static Map<Integer, HashSet<Integer>> adjMap; // adjMap -> contine listele de adiacenta
    private static Map<Integer, Integer> visited;

    private static Map<Integer, HashSet<Integer>> connectedComponents;
    private static Map<Integer, Integer> maxies; // maxies -> lungimea maxima pentru fiecare componenta conexa, calculata in DFS
    private Integer current_component; // componenta_curenta -> retine componenta conexa actuala

    public FriendshipsGraph(int vertices, int edges) {
        number_of_vertices = vertices;
        number_of_edges = edges;
        adjMap = new HashMap<>();
        visited = new HashMap<>();

        maxies = new HashMap<>();
        connectedComponents = new HashMap<>();
    }

    public void initialize(int vertices) {
        int index;
        for (index = 0; index < vertices; index++) {
            adjMap.putIfAbsent(index, new HashSet<Integer>());
            visited.put(index, 0);
        }
    }

    //Functia pentru adaugarea unei muchii
    public void addEdge(int s, int d) {
        adjMap.get(s).add(d);
        adjMap.get(d).add(s);
    }

    //Functia pentru marcarea varfurilor care pot fi vizitate
    private void findDFS(int vertex) {
        //marcarea varfului
        visited.put(vertex,1);

        Integer maxi = 0;
        //afisarea varfului
        //System.out.println("varful " + vertex + " a fost vizitat");
        connectedComponents.get(current_component).add(vertex);
        for (Integer child : adjMap.get(vertex)) {
            if (visited.get(child) == 0){
                maxi++;
                if (maxi > maxies.get(current_component)) {
                    maxies.put(current_component, maxi);
                }

                findDFS(child);
            }
        }
    }

    //Functia pentru afisarea grafului
    //pentru fiecare varf, se printeaza varfurile cu care se afla in legatura directa
    private void printGraph() {
        int vertex = 0;
        for (HashSet<Integer> v : adjMap.values()) {
            System.out.println("varful " + vertex + ": " + v.toString());
            vertex++;
        }
        System.out.println();
    }

    public void executeDFS() {
        current_component = 0;
        for(Integer vertex : visited.keySet()) {
            //verifica daca varful a fost vizitat
            if (visited.get(vertex) == 0) {
                //initializam maxies pentru componenta curenta
                maxies.put(current_component, 0);
                //initializam multimea pentru componenta curenta
                connectedComponents.putIfAbsent(current_component, new HashSet<Integer>());

                //se executa functia findDFS
                this.findDFS(vertex);

                //crestem contorul
                current_component++;
            }
        }
    }

    public int connectedComponentsCount() {
        executeDFS();
        return current_component;
    }

    public HashSet<Integer> longestPathComponent() {
        executeDFS();

        Integer maxi = maxies.get(0);
        int max_poz = 0;
        for (int i = 0; i < maxies.size(); i++) {
            if (maxi < maxies.get(i)) {
                maxi = maxies.get(i);
                max_poz = i;
            }
        }

        return connectedComponents.get(max_poz);
    }

}