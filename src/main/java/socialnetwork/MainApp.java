package socialnetwork;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import socialnetwork.config.ApplicationContext;
import socialnetwork.controller.FriendshipsController;
import socialnetwork.controller.LoginController;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.*;
import socialnetwork.repository.paging.PagingRepository;
import socialnetwork.service.SuperService;
import socialnetwork.service.UtilizatorService;
import socialnetwork.utils.events.FriendshipChangeEvent;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.mindrot.jbcrypt.BCrypt;

public class MainApp extends Application {
    SuperService service;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        ////REPO IN BAZA DE DATE
        final String url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
        final String username = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username");
        final String password = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password");

        ////REPOSITORY SIMPLU
        //Repository<Long, Utilizator> userDBRepository = new UtilizatorDBRepository(url, username, password, new UtilizatorValidator());

        //PrietenieValidator prietenieValidator = new PrietenieValidator(userDBRepository);
        //Repository<Tuple<Long,Long>, Prietenie> friendshipRepository = new PrietenieDBRepository(url, username, password, prietenieValidator);
        //prietenieValidator.setRepoPrietenie(friendshipRepository);

        //Repository<Long, Mesaj> messageDBRepository = new MesajDBRepository(url, username, password, new MesajValidator(userDBRepository));

        //CerereValidator cerereValidator = new CerereValidator(userDBRepository, friendshipRepository);
        //Repository<Tuple<Long,Long>, CerereDePrietenie> requestDBRepository = new CerereDBRepository(url, username, password, cerereValidator);
        //cerereValidator.setRepoCerere(requestDBRepository);
        ////

        ////REPOSITORY PAGINAT
        PagingRepository<Long, Utilizator> userDBRepository = new UtilizatorDBRepository(url, username, password, new UtilizatorValidator());

        PrietenieValidator prietenieValidator = new PrietenieValidator(userDBRepository);
        PagingRepository<Tuple<Long,Long>, Prietenie> friendshipRepository = new PrietenieDBRepository(url, username, password, prietenieValidator);
        prietenieValidator.setRepoPrietenie(friendshipRepository);

        PagingRepository<Long, Mesaj> messageDBRepository = new MesajDBRepository(url, username, password, new MesajValidator(userDBRepository));

        CerereValidator cerereValidator = new CerereValidator(userDBRepository, friendshipRepository);
        PagingRepository<Tuple<Long,Long>, CerereDePrietenie> requestDBRepository = new CerereDBRepository(url, username, password, cerereValidator);
        cerereValidator.setRepoCerere(requestDBRepository);

        EvenimentValidator evenimentValidator = new EvenimentValidator(userDBRepository);
        PagingRepository<Long,Eveniment> eventDBRepository = new EvenimentDBRepository(url, username, password, evenimentValidator);

        NotificareValidator notificareValidator = new NotificareValidator(userDBRepository);
        PagingRepository<Long,Notificare> notificationDBRepository = new NotificareDBRepository(url, username, password, notificareValidator);
        ////

        service = new SuperService(userDBRepository, friendshipRepository, requestDBRepository, messageDBRepository, eventDBRepository, notificationDBRepository);

        ////SETAREA SERVICE-URILOR DIN FIECARE SERVICE
        service.getUtilizatorService().setPrietenieService(service.getPrietenieService());
        service.getUtilizatorService().setCerereService(service.getCerereService());

        service.getCerereService().setUtilizatorService(service.getUtilizatorService());
        service.getCerereService().setPrietenieService(service.getPrietenieService());

        service.getMesajService().setUtilizatorService(service.getUtilizatorService());

        service.getNotificareService().setUtilizatorService(service.getUtilizatorService());

        service.getEvenimentService().setUtilizatorService(service.getUtilizatorService());
        service.getEvenimentService().setNotificareService(service.getNotificareService());

        service.getPrietenieService().setCerereService(service.getCerereService());
        ////

        /*service.getUtilizatorService().getAllUsers().forEach(System.out::println);
        System.out.println();
        service.getUtilizatorService().setPageSize(3);
        System.out.println("Elements on page 2");
        service.getUtilizatorService().getUsersOnPage(0).stream()
                .forEach(System.out::println);
        System.out.println("Elements on next page");
        service.getUtilizatorService().getNextUser().stream()
                .forEach(System.out::println);

        System.out.println();
        System.out.println();

        service.getPrietenieService().getAllFriendships().forEach(System.out::println);
        System.out.println();
        service.getPrietenieService().setPageSize(3);
        System.out.println("Elements on page 2");
        service.getPrietenieService().getUsersOnPage(0).stream()
                .forEach(System.out::println);
        System.out.println("Elements on next page");
        service.getPrietenieService().getNextUser().stream()
                .forEach(System.out::println);

        System.out.println();
        System.out.println();

        service.getMesajService().getAllMessages().forEach(System.out::println);
        System.out.println();
        service.getMesajService().setPageSize(3);
        System.out.println("Elements on page 2");
        service.getMesajService().getUsersOnPage(0).stream()
                .forEach(System.out::println);
        System.out.println("Elements on next page");
        service.getMesajService().getNextUser().stream()
                .forEach(System.out::println);

        System.out.println();
        System.out.println();

        service.getCerereService().getAllRequests().forEach(System.out::println);
        System.out.println();
        service.getCerereService().setPageSize(3);
        System.out.println("Elements on page 2");
        service.getCerereService().getUsersOnPage(0).stream()
                .forEach(System.out::println);
        System.out.println("Elements on next page");
        service.getCerereService().getNextUser().stream()
                .forEach(System.out::println);*/

        initView(primaryStage);
        primaryStage.setWidth(800);
        primaryStage.setTitle("Pagina de Conectare");
        primaryStage.show();
    }

    private void initView(Stage primaryStage) throws IOException {
        FXMLLoader loginLoader = new FXMLLoader();
        loginLoader.setLocation(getClass().getResource("/views/loginView.fxml"));
        AnchorPane loginTaskLayout = loginLoader.load();
        primaryStage.setScene(new Scene(loginTaskLayout));
        primaryStage.setWidth(800);

        LoginController loginController = loginLoader.getController();

        Utilizator user = null;
        UsersPage usersPage = new UsersPage(user, service);
        loginController.setPage(usersPage, primaryStage);

        /*FXMLLoader friendshipLoader = new FXMLLoader();
        friendshipLoader.setLocation(getClass().getResource("/views/friendshipsView.fxml"));
        AnchorPane friendshipTaskLayout = friendshipLoader.load();
        primaryStage.setScene(new Scene(friendshipTaskLayout));

        FriendshipsController friendshipsController = friendshipLoader.getController();
        friendshipsController.setService(service, primaryStage);*/
    }
}
