package socialnetwork.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import socialnetwork.domain.UsersPage;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.SuperService;

import java.io.IOException;

public class NewUserPageController {
    @FXML
    private BorderPane mainBorderPane;

    @FXML
    private Button notificationButton;

    private UsersPage usersPage;
    private Stage dialogStage;

    public void setPage(UsersPage usersPage, Stage stage) {
        this.usersPage = usersPage;
        this.dialogStage = stage;
        handleMyProfile();
    }

    private void setNotificationImage() {
        if (usersPage.getService().getNotificareService().unseenNotifications_forUser(usersPage.getConnected_user().getId())) {
            ImageView imageView = new ImageView();
            Image image = new Image("/images/exclamation-icon.png");
            imageView.setImage(image);
            imageView.setFitHeight(18);
            imageView.setFitWidth(18);
            notificationButton.setGraphic(imageView);
        }
        else {
            notificationButton.setGraphic(null);
        }
    }

    @FXML
    public void handleMyProfile() {
        this.setNotificationImage();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/userProfileView.fxml"));
            mainBorderPane.setCenter(loader.load());

            ProfileController profileController = loader.getController();
            profileController.setPage(usersPage, dialogStage);

        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    public void handleFriendsPage() {
        this.setNotificationImage();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/newFriendsPageView.fxml"));
            mainBorderPane.setCenter(loader.load());

            NewFriendsPageController newFriendsPageController = loader.getController();
            newFriendsPageController.setPage(usersPage, dialogStage);

        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    public void handleRequestsPage() {
        this.setNotificationImage();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/newRequestsPageView.fxml"));
            mainBorderPane.setCenter(loader.load());

            NewRequestPageController newRequestPageController = loader.getController();
            newRequestPageController.setPage(usersPage, dialogStage);

        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    public void handleMessagesPage() {
        this.setNotificationImage();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/newMessagesPageView.fxml"));
            mainBorderPane.setCenter(loader.load());

            NewMessagesPageController newMessagesPageController = loader.getController();
            newMessagesPageController.setPage(usersPage, dialogStage);

        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    public void handleNotificationsPage() {
        this.setNotificationImage();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/newNotificationsPageView.fxml"));
            mainBorderPane.setCenter(loader.load());

            NewNotificationsPageController newNotificationsPageController = loader.getController();
            newNotificationsPageController.setPage(usersPage, dialogStage);

        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    public void handleEventsPage() {
        this.setNotificationImage();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/newEventsPageView.fxml"));
            mainBorderPane.setCenter(loader.load());

            NewEventsPageController newEventsPageController = loader.getController();
            newEventsPageController.setPage(usersPage, dialogStage);

        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    public void handleLogout() {
        dialogStage.close();
        this.goToLogin();
    }

    public void goToLogin() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/loginView.fxml"));
            AnchorPane root = loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Pagina de Conectare");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.setWidth(800);
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);
            LoginController loginController = loader.getController();

            usersPage.setConnected_user(new Utilizator());
            loginController.setPage(usersPage, dialogStage);
            dialogStage.show();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    public void handleClose() {
        dialogStage.close();
    }
}
