package socialnetwork.controller;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Callback;
import socialnetwork.domain.*;
import socialnetwork.utils.Constants;
import socialnetwork.utils.events.FriendshipChangeEvent;
import socialnetwork.utils.events.NotificationChangeEvent;
import socialnetwork.utils.observer.Observer;

import java.util.List;

public class NewNotificationsPageController implements Observer<NotificationChangeEvent> {
    private UsersPage usersPage;
    private Stage dialogStage;
    private ObservableList<NotificareDTO> notificationsModel = FXCollections.observableArrayList();

    @FXML
    private TableView<NotificareDTO> tableView;
    @FXML
    private TableColumn<NotificareDTO,String> tableColumnDescription;
    @FXML
    private TableColumn<NotificareDTO,String> tableColumnType;
    @FXML
    private TableColumn<NotificareDTO,String> tableColumnStatus;
    @FXML
    private TableColumn<NotificareDTO,String> tableColumnDate;
    @FXML
    private TableColumn<NotificareDTO,NotificareDTO> tableColumnUnsubscribeButton;
    @FXML
    private TableColumn<NotificareDTO,NotificareDTO> tableColumnMaskAsReadButton;

    @FXML
    private Pagination paginationTableView;

    public void setPage(UsersPage usersPage, Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.usersPage = usersPage;
        usersPage.getService().getNotificareService().addObserver(this);
        initModel();
    }

    @FXML
    public void initialize() {
        tableColumnDescription.setCellValueFactory(new PropertyValueFactory<NotificareDTO, String>("description"));
        tableColumnType.setCellValueFactory(new PropertyValueFactory<NotificareDTO, String>("type"));
        tableColumnStatus.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getStatus().get(usersPage.getConnected_user().getId())));
        tableColumnDate.setCellValueFactory(new PropertyValueFactory<NotificareDTO, String>("date_time"));

        Callback<TableColumn<NotificareDTO,NotificareDTO>, TableCell<NotificareDTO,NotificareDTO>> cellFactory_unsubscribeButton = new Callback<TableColumn<NotificareDTO,NotificareDTO>, TableCell<NotificareDTO,NotificareDTO>>() {
            @Override
            public TableCell<NotificareDTO,NotificareDTO> call(TableColumn<NotificareDTO,NotificareDTO> param) {

                return new TableCell<NotificareDTO,NotificareDTO>() {
                    final Button unsubscribeButton = new Button("Dezabonare");

                    {
                        unsubscribeButton.setOnAction(event -> {
                            NotificareDTO selectedItem = tableView.getSelectionModel().getSelectedItem();
                            unsubscribeNotification(selectedItem);
                        });
                        unsubscribeButton.setStyle("-fx-background-color: #581845;-fx-border-width: 0;-fx-text-alignment: center;-fx-text-fill: #fcf7f7;-fx-font-family: 'Times New Roman';-fx-cursor: 'OPEN_HAND';");
                        ImageView imageView = new ImageView();
                        Image image = new Image("/images/unsubscribe-icon.png");
                        imageView.setImage(image);
                        imageView.setFitHeight(18);
                        imageView.setFitWidth(18);
                        unsubscribeButton.setGraphic(imageView);
                    }

                    @Override
                    protected void updateItem(NotificareDTO item, boolean empty) {
                        super.updateItem(item, empty);
                        this.setAlignment(Pos.CENTER);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(unsubscribeButton);
                        }
                    }
                };

            }
        };
        tableColumnUnsubscribeButton.setCellFactory(cellFactory_unsubscribeButton);

        Callback<TableColumn<NotificareDTO,NotificareDTO>, TableCell<NotificareDTO,NotificareDTO>> cellFactory_markReadButton = new Callback<TableColumn<NotificareDTO,NotificareDTO>, TableCell<NotificareDTO,NotificareDTO>>() {
            @Override
            public TableCell<NotificareDTO,NotificareDTO> call(TableColumn<NotificareDTO,NotificareDTO> param) {

                return new TableCell<NotificareDTO,NotificareDTO>() {
                    final Button markReadButton = new Button("Marcheaza ca citit");

                    {
                        markReadButton.setOnAction(event -> {
                            NotificareDTO selectedItem = tableView.getSelectionModel().getSelectedItem();
                            markNotificationAsRead(selectedItem);
                        });
                        markReadButton.setStyle("-fx-background-color: #581845;-fx-border-width: 0;-fx-text-alignment: center;-fx-text-fill: #fcf7f7;-fx-font-family: 'Times New Roman';-fx-cursor: 'OPEN_HAND';");
                        ImageView imageView = new ImageView();
                        Image image = new Image("/images/read-icon.png");
                        imageView.setImage(image);
                        imageView.setFitHeight(18);
                        imageView.setFitWidth(18);
                        markReadButton.setGraphic(imageView);
                    }

                    @Override
                    protected void updateItem(NotificareDTO item, boolean empty) {
                        super.updateItem(item, empty);
                        this.setAlignment(Pos.CENTER);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(markReadButton);
                        }
                    }
                };

            }
        };
        tableColumnMaskAsReadButton.setCellFactory(cellFactory_markReadButton);

        tableView.setItems(notificationsModel);
    }

    private void initModel() {
        int total_pages = (int) (Math.ceil(usersPage.getService().getNotificareService().number_of_notifications(usersPage.getConnected_user().getId()) * 1.0 / Constants.NUMBER_OF_RECORDS_ON_PAGE));
        if (total_pages == 0) total_pages = 1;
        paginationTableView.setPageCount(total_pages);
        paginationTableView.setCurrentPageIndex(0);
        paginationTableView.currentPageIndexProperty().addListener(
                ((observable, oldValue, newValue) -> updateNotificationsTableView(newValue.intValue()))
        );
        updateNotificationsTableView(0);
    }

    private void updateNotificationsTableView(int page) {
        List<NotificareDTO> notificationList = usersPage.getService().getNotificareService().getUsersNotifications_PAGED(page, usersPage.getConnected_user().getId());
        notificationsModel.setAll(notificationList);
    }

    @Override
    public void update(NotificationChangeEvent notificationChangeEvent) {
        initModel();
    }

    public void unsubscribeNotification(NotificareDTO selected_item) {
        if (selected_item != null) {
            try {
                usersPage.getService().getNotificareService().removeUser_fromNotification(usersPage.getConnected_user().getId(), selected_item.getId());
                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Notificare", "V-ati dezabonat cu succes de la notificarea de tipul " + selected_item.getType() + "!");
            }
            catch (Exception ex) {
                MessageAlert.showErrorMessage(null, ex.getMessage());
                ex.printStackTrace();
            }
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din tabel!");
        }
    }

    public void markNotificationAsRead(NotificareDTO selected_item) {
        if (selected_item != null) {
            try {
                Notificare notificare = usersPage.getService().getNotificareService().findNotification_id(selected_item.getId());
                usersPage.getService().getNotificareService().markNotificationAsRead(notificare, usersPage.getConnected_user().getId());
                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Notificare", "Ati marcat notificarea ca fiind citita!");
            }
            catch (Exception ex) {
                MessageAlert.showErrorMessage(null, ex.getMessage());
                ex.printStackTrace();
            }
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din tabel!");
        }
    }

}
