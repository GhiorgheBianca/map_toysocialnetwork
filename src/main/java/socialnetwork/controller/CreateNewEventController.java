package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import socialnetwork.domain.UsersPage;
import socialnetwork.domain.Utilizator;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class CreateNewEventController {
    @FXML
    private TextField textFieldName;
    @FXML
    private TextArea textAreaDescription;

    @FXML
    private ComboBox<String> comboBoxType = new ComboBox<String>();
    @FXML
    private ComboBox<Utilizator> comboBoxUsers = new ComboBox<Utilizator>();

    @FXML
    private DatePicker datePickerStart;
    @FXML
    private DatePicker datePickerEnd;

    @FXML
    private ListView<Utilizator> listViewUsers;

    private UsersPage usersPage;
    private Stage dialogStage;

    @FXML
    private void initialize() {
        List<String> options = new ArrayList<>();
        options.add("public");
        options.add("private");
        ObservableList<String> observableTypeList = FXCollections.observableArrayList(options);
        comboBoxType.setItems(observableTypeList);
    }

    private void initVisualObjects() {
        List<Utilizator> all_users = StreamSupport
                .stream(usersPage.getService().getUtilizatorService().getAllUsers().spliterator(), false)
                .collect(Collectors.toList());

        ObservableList<Utilizator> observableUserList = FXCollections.observableArrayList(all_users);
        comboBoxUsers.setItems(observableUserList);

        comboBoxUsers.setCellFactory(new Callback<ListView<Utilizator>, ListCell<Utilizator>>() {
            @Override
            public ListCell<Utilizator> call(ListView<Utilizator> param) {
                return new ListCell<Utilizator>() {
                    @Override
                    protected void updateItem(Utilizator item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item == null || empty) {
                            setText(null);
                        } else {
                            setText("Email: " + item.getEmail() + "; Nume: " + item.getFirstName() + "; Prenume: " + item.getLastName());
                        }
                    }
                };
            }
        });

        comboBoxUsers.setConverter(new StringConverter<Utilizator>() {
            @Override
            public String toString(Utilizator user) {
                if (user == null) {
                    return null;
                }
                else {
                    return "Email: " + user.getEmail() + "; Nume: " + user.getFirstName() + "; Prenume: " + user.getLastName();
                }
            }

            @Override
            public Utilizator fromString(String string) {
                return null;
            }
        });

        listViewUsers.setCellFactory(lv -> new ListCell<Utilizator>() {
            @Override
            public void updateItem(Utilizator user, boolean empty) {
                super.updateItem(user, empty) ;
                setText(empty ? null : "Email: " + user.getEmail() + "; Nume: " + user.getFirstName() + "; Prenume: " + user.getLastName());
            }
        });
    }

    public void setPage(UsersPage usersPage, Stage stage) {
        this.usersPage = usersPage;
        this.dialogStage = stage;
        this.initVisualObjects();
    }

    @FXML
    public void handleCreateEvent() {
        try {
            String nume = textFieldName.getText();
            String descriere = textAreaDescription.getText();

            String tip = comboBoxType.getSelectionModel().getSelectedItem();

            LocalDateTime start_date = datePickerStart.getValue().atTime(00, 00, 00);
            LocalDateTime final_date = datePickerEnd.getValue().atTime(00, 00, 00);

            Set<Long> participants = new HashSet<>();
            for (Utilizator user : listViewUsers.getItems()) {
                participants.add(user.getId());
            }

            usersPage.getService().getEvenimentService().addNewEvent(nume, descriere, tip, start_date, final_date, participants);
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Eveniment", "Evenimentul a fost creat cu succes!");
            dialogStage.close();
        }
        catch (Exception ex) {
            MessageAlert.showErrorMessage(null, ex.getMessage());
        }
    }

    @FXML
    public void handleAddUser() {
        Utilizator selected_user = comboBoxUsers.getSelectionModel().getSelectedItem();
        if (selected_user != null) {
            listViewUsers.getItems().add(selected_user);
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din comboBox!");
        }
    }

    @FXML
    public void handleDeleteUser() {
        Utilizator selected_user = listViewUsers.getSelectionModel().getSelectedItem();
        if (selected_user != null) {
            listViewUsers.getItems().remove(selected_user);
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din listView!");
        }
    }

    @FXML
    public void handleCancel() {
        dialogStage.close();
    }

}
