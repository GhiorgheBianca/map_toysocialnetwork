package socialnetwork.controller;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.stream.Stream;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import socialnetwork.domain.MesajDTO;
import socialnetwork.domain.PrietenDTO;
import socialnetwork.domain.UsersPage;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.SuperService;
import socialnetwork.utils.Constants;

import javax.swing.*;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ProfileController {
    @FXML
    private ComboBox<String> comboBoxReports = new ComboBox<String>();
    @FXML
    private ComboBox<Utilizator> comboBoxFriend = new ComboBox<Utilizator>();

    @FXML
    private DatePicker datePickerStart;
    @FXML
    private DatePicker datePickerEnd;

    @FXML
    private TextArea textAreaResults;

    @FXML
    private Label labelFirstName;
    @FXML
    private Label labelLastName;
    @FXML
    private Label labelEmail;

    private UsersPage usersPage;
    private Stage dialogStage;

    @FXML
    private void initialize() {
        List<String> reports_list = new ArrayList<>();
        reports_list.add("Raportul activitatilor din perioada selectata (prietenii + mesaje)");
        reports_list.add("Raportul mesajelor primite, de la prietenul ales, din perioada selectata");
        ObservableList<String> observableReportsList = FXCollections.observableArrayList(reports_list);
        comboBoxReports.setItems(observableReportsList);
    }

    private void initComboBox() {
        List<Utilizator> friend_users = new ArrayList<>();
        for (PrietenDTO friend_dto : usersPage.getService().getUtilizatorService().getAllFriends_forUser(usersPage.getConnected_user().getId())) {
            friend_users.add(friend_dto.getUser());
        }

        ObservableList<Utilizator> observableFriendList = FXCollections.observableArrayList(friend_users);
        comboBoxFriend.setItems(observableFriendList);

        comboBoxFriend.setCellFactory(new Callback<ListView<Utilizator>, ListCell<Utilizator>>() {
            @Override
            public ListCell<Utilizator> call(ListView<Utilizator> param) {
                return new ListCell<Utilizator>() {
                    @Override
                    protected void updateItem(Utilizator item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item == null || empty) {
                            setText(null);
                        } else {
                            setText("Email: " + item.getEmail() + "; Nume: " + item.getFirstName() + "; Prenume: " + item.getLastName());
                        }
                    }
                };
            }
        });

        comboBoxFriend.setConverter(new StringConverter<Utilizator>() {
            @Override
            public String toString(Utilizator user) {
                if (user == null) {
                    return null;
                }
                else {
                    return "Email: " + user.getEmail() + "; Nume: " + user.getFirstName() + "; Prenume: " + user.getLastName();
                }
            }

            @Override
            public Utilizator fromString(String string) {
                return null;
            }
        });
    }

    public void setPage(UsersPage usersPage, Stage stage) {
        this.usersPage = usersPage;
        this.dialogStage = stage;
        this.initComboBox();

        labelFirstName.setText(usersPage.getConnected_user().getFirstName());
        labelLastName.setText(usersPage.getConnected_user().getLastName());
        labelEmail.setText(usersPage.getConnected_user().getEmail());
    }

    @FXML
    public void handleGenerate() {
        String selected_report = comboBoxReports.getSelectionModel().getSelectedItem();
        Utilizator selected_friend = comboBoxFriend.getSelectionModel().getSelectedItem();
        LocalDate start_date = datePickerStart.getValue();
        LocalDate end_date = datePickerEnd.getValue();
        if (selected_report != null && (start_date != null && end_date != null)) {
            String results = "";
            if (selected_report.equals("Raportul activitatilor din perioada selectata (prietenii + mesaje)")) {
                List<PrietenDTO> prietenii = usersPage.getService().getUtilizatorService().getFriends_betweenDates(start_date.atTime(00,00,00), end_date.atTime(00,00,00), usersPage.getConnected_user().getId());
                List<MesajDTO> mesaje = usersPage.getService().getMesajService().getMessages_betweenDates(start_date.atTime(00,00,00), end_date.atTime(00,00,00), usersPage.getConnected_user().getId());

                results = "Perioada aleasa: " + start_date.atTime(00,00,00).format(Constants.DATE_TIME_FORMATTER) + " ~ " + end_date.atTime(00,00,00).format(Constants.DATE_TIME_FORMATTER) + "\n\n";
                results += "Prietenii formate in perioada mentionata: \n";
                for (PrietenDTO prietenDTO : prietenii) {
                    results += prietenDTO.toString() + "\n";
                }
                results += "\n" + "Mesaje primite in perioada mentionata: \n";
                for (MesajDTO mesajDTO : mesaje) {
                    results += mesajDTO.toString() + "\n";
                }
            }
            else if ((selected_friend != null) && selected_report.equals("Raportul mesajelor primite, de la prietenul ales, din perioada selectata")) {
                List<MesajDTO> mesaje = usersPage.getService().getMesajService().getMessages_betweenDates_fromUser(start_date.atTime(00,00,00), end_date.atTime(00,00,00), usersPage.getConnected_user().getId(), selected_friend.getId());

                results = "Perioada aleasa: " + start_date.atTime(00,00,00).format(Constants.DATE_TIME_FORMATTER) + " ~ " + end_date.atTime(00,00,00).format(Constants.DATE_TIME_FORMATTER) + "\n";
                results += "Prietenul ales: " + selected_friend.toString() + "\n\n";
                results += "Mesaje primite in perioada mentionata de la utilizatorul mentionat: \n";
                for (MesajDTO mesajDTO : mesaje) {
                    results += mesajDTO.toString() + "\n";
                }
            }
            else {
                MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din comboBox!");
            }
            textAreaResults.setText(results);
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din comboBox/datePicker!");
        }
    }

    @FXML
    public void handleSavePDF() throws DocumentException, FileNotFoundException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save as PDF");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("PDF File (*.pdf)", "*.pdf"));
        File file = fileChooser.showSaveDialog(dialogStage);
        if (file != null) {
            try {
                String selected_report = comboBoxReports.getSelectionModel().getSelectedItem();
                Utilizator selected_friend = comboBoxFriend.getSelectionModel().getSelectedItem();
                LocalDate start_date = datePickerStart.getValue();
                LocalDate end_date = datePickerEnd.getValue();
                if (selected_report != null && (start_date != null && end_date != null)) {
                    Document doc = new Document();
                    PdfWriter.getInstance(doc, new FileOutputStream(file.getPath()));
                    doc.open();

                    if (selected_report.equals("Raportul activitatilor din perioada selectata (prietenii + mesaje)")) {
                        List<PrietenDTO> prietenii = usersPage.getService().getUtilizatorService().getFriends_betweenDates(start_date.atTime(00,00,00), end_date.atTime(00,00,00), usersPage.getConnected_user().getId());
                        List<MesajDTO> mesaje = usersPage.getService().getMesajService().getMessages_betweenDates(start_date.atTime(00,00,00), end_date.atTime(00,00,00), usersPage.getConnected_user().getId());

                        String title = "Perioada aleasa: " + start_date.atTime(00,00,00).format(Constants.DATE_TIME_FORMATTER) + " ~ " + end_date.atTime(00,00,00).format(Constants.DATE_TIME_FORMATTER) + "\n\n\n";
                        title += "Prietenii formate in perioada mentionata: \n";

                        PdfPTable friends_table = new PdfPTable(4);
                        Stream.of("Nume", "Prenume", "Email", "Data imprietenirii").forEach(friends_table::addCell);

                        Font bold = new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD);
                        Paragraph friends_paragraph = new Paragraph(title);

                        prietenii.forEach(prietenDTO -> {
                                friends_table.addCell(prietenDTO.getFirstName());
                                friends_table.addCell(prietenDTO.getLastName());
                                friends_table.addCell(prietenDTO.getEmail());
                                friends_table.addCell(prietenDTO.getDate().format(Constants.DATE_TIME_FORMATTER));
                                });

                        friends_paragraph.add(friends_table);
                        doc.add(friends_paragraph);


                        title = "\n\n" + "Mesaje primite in perioada mentionata: \n";

                        PdfPTable messages_table = new PdfPTable(6);
                        Stream.of("ID mesaj", "De la", "Catre", "Text mesaj", "Data primirii", "ID reply").forEach(messages_table::addCell);

                        Paragraph messages_paragraph = new Paragraph(title);

                        mesaje.forEach(mesajDTO -> {
                            messages_table.addCell(mesajDTO.getId().toString());
                            messages_table.addCell(mesajDTO.getFrom());
                            messages_table.addCell(mesajDTO.getTo().toString());
                            messages_table.addCell(mesajDTO.getMessage());
                            messages_table.addCell(mesajDTO.getDate().format(Constants.DATE_TIME_FORMATTER));
                            if (mesajDTO.getId_reply() == null) {
                                messages_table.addCell("");
                            }
                            else {
                                messages_table.addCell(mesajDTO.getId_reply().toString());
                            }
                        });

                        messages_paragraph.add(messages_table);
                        doc.add(messages_paragraph);
                    }
                    else if ((selected_friend != null) && selected_report.equals("Raportul mesajelor primite, de la prietenul ales, din perioada selectata")) {
                        List<MesajDTO> mesaje = usersPage.getService().getMesajService().getMessages_betweenDates_fromUser(start_date.atTime(00,00,00), end_date.atTime(00,00,00), usersPage.getConnected_user().getId(), selected_friend.getId());

                        String title = "Perioada aleasa: " + start_date.atTime(00,00,00).format(Constants.DATE_TIME_FORMATTER) + " ~ " + end_date.atTime(00,00,00).format(Constants.DATE_TIME_FORMATTER) + "\n\n\n";
                        title += "Prietenul ales: " + selected_friend.toString() + "\n\n";
                        title += "Mesaje primite in perioada mentionata de la utilizatorul mentionat: \n";

                        PdfPTable messages_table = new PdfPTable(6);
                        Stream.of("ID mesaj", "De la", "Catre", "Text mesaj", "Data primirii", "ID reply").forEach(messages_table::addCell);

                        Paragraph messages_paragraph = new Paragraph(title);

                        mesaje.forEach(mesajDTO -> {
                            messages_table.addCell(mesajDTO.getId().toString());
                            messages_table.addCell(mesajDTO.getFrom());
                            messages_table.addCell(mesajDTO.getTo().toString());
                            messages_table.addCell(mesajDTO.getMessage());
                            messages_table.addCell(mesajDTO.getDate().format(Constants.DATE_TIME_FORMATTER));
                            if (mesajDTO.getId_reply() == null) {
                                messages_table.addCell("");
                            }
                            else {
                                messages_table.addCell(mesajDTO.getId_reply().toString());
                            }
                        });

                        messages_paragraph.add(messages_table);
                        doc.add(messages_paragraph);
                    }
                    else {
                        MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din comboBox!");
                    }

                    doc.close();
                }
                else {
                    MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din comboBox/datePicker!");
                }

                /*
                //VARIANTA ANTERIOARA:
                Document doc = new Document();
                PdfWriter.getInstance(doc, new FileOutputStream(file.getPath()));
                doc.open();

                Font bold = new Font(Font.FontFamily.HELVETICA, 18, Font.BOLD);
                Paragraph paragraph = new Paragraph(textAreaResults.getText());
                doc.add(paragraph);
                doc.close();
                */
            } catch (Exception ex) {
                MessageAlert.showErrorMessage(null, ex.getMessage());
            }
        }
    }
}
