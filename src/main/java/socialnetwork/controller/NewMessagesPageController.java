package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import socialnetwork.domain.*;
import socialnetwork.utils.Constants;
import socialnetwork.utils.events.MessageChangeEvent;
import socialnetwork.utils.observer.Observer;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class NewMessagesPageController implements Observer<MessageChangeEvent> {
    private UsersPage usersPage;
    private ObservableList<MesajDTO> model_sent = FXCollections.observableArrayList();
    private ObservableList<MesajDTO> model_received = FXCollections.observableArrayList();
    private Stage dialogStage;

    @FXML
    private TableView<MesajDTO> tableViewSentMessages;
    @FXML
    private TableColumn<MesajDTO,String> tableColumnId_Sent;
    @FXML
    private TableColumn<MesajDTO,String> tableColumnFrom_Sent;
    @FXML
    private TableColumn<MesajDTO,String> tableColumnTo_Sent;
    @FXML
    private TableColumn<MesajDTO,String> tableColumnMessage_Sent;
    @FXML
    private TableColumn<MesajDTO,String> tableColumnDate_Sent;
    @FXML
    private TableColumn<MesajDTO,String> tableColumnIdReply_Sent;
    @FXML
    private TableColumn<MesajDTO,MesajDTO> tableColumnRespondButton_Sent;

    @FXML
    private TableView<MesajDTO> tableViewReceivedMessages;
    @FXML
    private TableColumn<MesajDTO,String> tableColumnId_Received;
    @FXML
    private TableColumn<MesajDTO,String> tableColumnFrom_Received;
    @FXML
    private TableColumn<MesajDTO,String> tableColumnTo_Received;
    @FXML
    private TableColumn<MesajDTO,String> tableColumnMessage_Received;
    @FXML
    private TableColumn<MesajDTO,String> tableColumnDate_Received;
    @FXML
    private TableColumn<MesajDTO,String> tableColumnIdReply_Received;
    @FXML
    private TableColumn<MesajDTO,MesajDTO> tableColumnRespondButton_Received;

    @FXML
    private ComboBox<Utilizator> comboBoxUsers = new ComboBox<Utilizator>();

    @FXML
    private Pagination paginationTableSent;
    @FXML
    private Pagination paginationTableReceived;

    public void setPage(UsersPage usersPage,  Stage stage) {
        this.usersPage = usersPage;
        usersPage.getService().getMesajService().addObserver(this);
        initModel();
        this.dialogStage = stage;
        this.initVisualObjects();
    }

    @FXML
    public void initialize() {
        initialize_Sent();
        initialize_Received();
    }

    private void initialize_Sent() {
        tableColumnId_Sent.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("id"));
        tableColumnFrom_Sent.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("from"));
        tableColumnTo_Sent.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("to"));
        tableColumnMessage_Sent.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("message"));
        tableColumnDate_Sent.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("date"));
        tableColumnIdReply_Sent.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("id_reply"));

        Callback<TableColumn<MesajDTO,MesajDTO>, TableCell<MesajDTO,MesajDTO>> cellFactory = new Callback<TableColumn<MesajDTO,MesajDTO>, TableCell<MesajDTO,MesajDTO>>() {
            @Override
            public TableCell<MesajDTO,MesajDTO> call(TableColumn<MesajDTO,MesajDTO> param) {

                return new TableCell<MesajDTO,MesajDTO>() {
                    final Button respondButton = new Button("Raspunde");

                    {
                        respondButton.setOnAction(event -> {
                            MesajDTO selectedItem = tableViewSentMessages.getSelectionModel().getSelectedItem();
                            respondMessage(selectedItem);
                        });
                        respondButton.setStyle("-fx-background-color: #581845;-fx-border-width: 0;-fx-text-alignment: center;-fx-text-fill: #fcf7f7;-fx-font-family: 'Times New Roman';-fx-cursor: 'OPEN_HAND';");
                        ImageView imageView = new ImageView();
                        Image image = new Image("/images/respond-icon.png");
                        imageView.setImage(image);
                        imageView.setFitHeight(18);
                        imageView.setFitWidth(18);
                        respondButton.setGraphic(imageView);
                    }

                    @Override
                    protected void updateItem(MesajDTO item, boolean empty) {
                        super.updateItem(item, empty);
                        this.setAlignment(Pos.CENTER);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(respondButton);
                        }
                    }
                };

            }
        };
        tableColumnRespondButton_Sent.setCellFactory(cellFactory);

        tableViewSentMessages.setItems(model_sent);
    }

    private void initialize_Received() {
        tableColumnId_Received.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("id"));
        tableColumnFrom_Received.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("from"));
        tableColumnTo_Received.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("to"));
        tableColumnMessage_Received.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("message"));
        tableColumnDate_Received.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("date"));
        tableColumnIdReply_Received.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("id_reply"));

        Callback<TableColumn<MesajDTO,MesajDTO>, TableCell<MesajDTO,MesajDTO>> cellFactory = new Callback<TableColumn<MesajDTO,MesajDTO>, TableCell<MesajDTO,MesajDTO>>() {
            @Override
            public TableCell<MesajDTO,MesajDTO> call(TableColumn<MesajDTO,MesajDTO> param) {

                return new TableCell<MesajDTO,MesajDTO>() {
                    final Button respondButton = new Button("Raspunde");

                    {
                        respondButton.setOnAction(event -> {
                            MesajDTO selectedItem = tableViewReceivedMessages.getSelectionModel().getSelectedItem();
                            respondMessage(selectedItem);
                        });
                        respondButton.setStyle("-fx-background-color: #581845;-fx-border-width: 0;-fx-text-alignment: center;-fx-text-fill: #fcf7f7;-fx-font-family: 'Times New Roman';-fx-cursor: 'OPEN_HAND';");
                        ImageView imageView = new ImageView();
                        Image image = new Image("/images/respond-icon.png");
                        imageView.setImage(image);
                        imageView.setFitHeight(18);
                        imageView.setFitWidth(18);
                        respondButton.setGraphic(imageView);
                    }

                    @Override
                    protected void updateItem(MesajDTO item, boolean empty) {
                        super.updateItem(item, empty);
                        this.setAlignment(Pos.CENTER);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(respondButton);
                        }
                    }
                };

            }
        };
        tableColumnRespondButton_Received.setCellFactory(cellFactory);

        tableViewReceivedMessages.setItems(model_received);
    }

    private void initModel() {
        this.init_paginationSent();
        this.init_paginationReceived();
    }

    private void init_paginationSent() {
        int total_pages = (int) (Math.ceil(usersPage.getService().getMesajService().number_of_messages_Sent(usersPage.getConnected_user().getId()) * 1.0 / Constants.NUMBER_OF_RECORDS_ON_PAGE));
        if (total_pages == 0) total_pages = 1;
        paginationTableSent.setPageCount(total_pages);
        paginationTableSent.setCurrentPageIndex(0);
        paginationTableSent.currentPageIndexProperty().addListener(
                ((observable, oldValue, newValue) -> updateMessageTable_Sent(newValue.intValue()))
        );
        this.updateMessageTable_Sent(0);
    }

    private void updateMessageTable_Sent(int page) {
        List<MesajDTO> messageList = usersPage.getService().getMesajService().getMesssages_SentByUser_PAGED(page, usersPage.getConnected_user().getId());
        model_sent.setAll(messageList);
    }

    private void init_paginationReceived() {
        int total_pages = (int) (Math.ceil(usersPage.getService().getMesajService().number_of_messages_Received(usersPage.getConnected_user().getId()) * 1.0 / Constants.NUMBER_OF_RECORDS_ON_PAGE));
        if (total_pages == 0) total_pages = 1;
        paginationTableReceived.setPageCount(total_pages);
        paginationTableReceived.setCurrentPageIndex(0);
        paginationTableReceived.currentPageIndexProperty().addListener(
                ((observable, oldValue, newValue) -> updateMessageTable_Received(newValue.intValue()))
        );
        this.updateMessageTable_Received(0);
    }

    private void updateMessageTable_Received(int page) {
        List<MesajDTO> messageList = usersPage.getService().getMesajService().getMessages_ReceivedByUser_PAGED(page, usersPage.getConnected_user().getId());
        model_received.setAll(messageList);
    }

    private void initVisualObjects() {
        List<Utilizator> all_users = StreamSupport
                .stream(usersPage.getService().getUtilizatorService().getAllUsers().spliterator(), false)
                .collect(Collectors.toList());

        ObservableList<Utilizator> observableUserList = FXCollections.observableArrayList(all_users);
        comboBoxUsers.setItems(observableUserList);

        comboBoxUsers.setCellFactory(new Callback<ListView<Utilizator>, ListCell<Utilizator>>() {
            @Override
            public ListCell<Utilizator> call(ListView<Utilizator> param) {
                return new ListCell<Utilizator>() {
                    @Override
                    protected void updateItem(Utilizator item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item == null || empty) {
                            setText(null);
                        } else {
                            setText("Email: " + item.getEmail() + "; Nume: " + item.getFirstName() + "; Prenume: " + item.getLastName());
                        }
                    }
                };
            }
        });

        comboBoxUsers.setConverter(new StringConverter<Utilizator>() {
            @Override
            public String toString(Utilizator user) {
                if (user == null) {
                    return null;
                }
                else {
                    return "Email: " + user.getEmail() + "; Nume: " + user.getFirstName() + "; Prenume: " + user.getLastName();
                }
            }

            @Override
            public Utilizator fromString(String string) {
                return null;
            }
        });
    }

    @Override
    public void update(MessageChangeEvent messageChangeEvent) {
        initModel();
    }

    @FXML
    public void handleSendMessage(javafx.event.ActionEvent ev) {
        this.sendMessageDialog();
    }

    public void sendMessageDialog() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/sendMessageView.fxml"));
            AnchorPane root = loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Editare mesaj");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.setWidth(900);
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);

            SendMessageController sendMessageController = loader.getController();
            sendMessageController.setService(usersPage, dialogStage);
            dialogStage.show();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void respondMessage(MesajDTO message_dto) {
        if (message_dto != null) {
            Mesaj message = new Mesaj(message_dto.getId(), message_dto.getFrom_id(), message_dto.getTo_ids(), message_dto.getMessage(), message_dto.getDate(), message_dto.getId_reply());
            this.respondMessageDialog(message);
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din tabelul cu mesaje primite!");
        }
    }

    public void respondMessageDialog(Mesaj message) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/respondMessageView.fxml"));
            AnchorPane root = loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Editare mesaj");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.setWidth(900);
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);

            RespondMessageController respondMessageController = loader.getController();
            respondMessageController.setService(usersPage, dialogStage, message);
            dialogStage.show();
        }
        catch(IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    public void handleShowConversation(javafx.event.ActionEvent ev) {
        Utilizator user = comboBoxUsers.getSelectionModel().getSelectedItem();
        if (user != null) {
            this.showConversationDialog(user);
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din comboBox!");
        }
    }

    public void showConversationDialog(Utilizator user) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/conversationView.fxml"));
            AnchorPane root = loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Conversatie");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.setWidth(650);
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);

            ConversationController conversationController = loader.getController();
            conversationController.setService(usersPage, dialogStage, user);
            dialogStage.show();
        }
        catch(IOException ex) {
            ex.printStackTrace();
        }
    }
}
