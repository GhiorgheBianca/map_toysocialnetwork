package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import socialnetwork.domain.*;
import socialnetwork.service.SuperService;
import socialnetwork.service.UtilizatorService;
import socialnetwork.utils.events.FriendRequestChangeEvent;
import socialnetwork.utils.events.FriendshipChangeEvent;
import socialnetwork.utils.observer.Observer;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class FriendshipsController implements Observer<FriendshipChangeEvent> {
    private UsersPage usersPage;
    private Stage dialogStage;
    private ObservableList<PrietenDTO> model = FXCollections.observableArrayList();

    @FXML
    TableView<PrietenDTO> tableView;
    @FXML
    TableColumn<PrietenDTO,String> tableColumnFirstName;
    @FXML
    TableColumn<PrietenDTO,String> tableColumnLastName;
    @FXML
    TableColumn<PrietenDTO,String> tableColumnEmail;
    @FXML
    TableColumn<PrietenDTO,String> tableColumnDate;

    public void setService(UsersPage usersPage, Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.usersPage = usersPage;
        usersPage.getService().getPrietenieService().addObserver(this);
        initModel();
    }

    @FXML
    public void initialize() {
        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<PrietenDTO, String>("firstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<PrietenDTO, String>("lastName"));
        tableColumnEmail.setCellValueFactory(new PropertyValueFactory<PrietenDTO, String>("email"));
        tableColumnDate.setCellValueFactory(new PropertyValueFactory<PrietenDTO, String>("date"));
        tableView.setItems(model);
    }

    private void initModel() {
        Iterable<PrietenDTO> friendships = usersPage.getService().getUtilizatorService().getFriends_forUser(usersPage.getConnected_user().getId());
        List<PrietenDTO> friendshipList = StreamSupport
                .stream(friendships.spliterator(), false)
                .collect(Collectors.toList());
        model.setAll(friendshipList);
    }

    @Override
    public void update(FriendshipChangeEvent friendshipChangeEvent) {
        initModel();
    }

    @FXML
    public void handleManageFriendRequests(javafx.event.ActionEvent ev) {
        this.manageFriendRequestsDialog(null);
    }

    public void manageFriendRequestsDialog(PrietenDTO prietenie) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/friendrequestsView.fxml"));
            AnchorPane root = loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Cereri de Prietenie");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.setWidth(900);
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);

            FriendRequestsController friendRequestsController = loader.getController();
            friendRequestsController.setService(usersPage, dialogStage);
            dialogStage.show();
        }
        catch(IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    public void handleDeleteFriendship(javafx.event.ActionEvent ev) {
        PrietenDTO selected = tableView.getSelectionModel().getSelectedItem();
        if (selected != null) {
            try {
                String nume_prieten = selected.getFirstName() + " " + selected.getLastName();
                Prietenie deleted = usersPage.getService().getPrietenieService().deleteFriendship(new Tuple<>(usersPage.getConnected_user().getId(), selected.getUser().getId()));
                if (deleted != null) {
                    MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Stergere", "Prietenia dintre tine si " + nume_prieten + " a fost stearsa cu succes!");
                }
            }
            catch (Exception ex) {
                MessageAlert.showErrorMessage(null, ex.getMessage());
            }
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp!");
        }
    }

    @FXML
    public void handleManageMessages(javafx.event.ActionEvent ev) {
        this.manageMessagesDialog(null);
    }

    public void manageMessagesDialog(PrietenDTO prietenie) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/messagesView.fxml"));
            AnchorPane root = loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Mesaje");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.setWidth(900);
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);

            MessagesController messagesController = loader.getController();
            messagesController.setService(usersPage, dialogStage);
            dialogStage.show();
        }
        catch(IOException ex) {
            ex.printStackTrace();
        }
    }
}
