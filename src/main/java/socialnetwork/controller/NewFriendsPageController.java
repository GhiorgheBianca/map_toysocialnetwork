package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import socialnetwork.domain.*;
import socialnetwork.utils.Constants;
import socialnetwork.utils.events.FriendshipChangeEvent;
import socialnetwork.utils.observer.Observer;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class NewFriendsPageController implements Observer<FriendshipChangeEvent> {
    private UsersPage usersPage;
    private Stage dialogStage;
    private ObservableList<PrietenDTO> friendsModel = FXCollections.observableArrayList();

    @FXML
    private TableView<PrietenDTO> tableView;
    @FXML
    private TableColumn<PrietenDTO,String> tableColumnFirstName;
    @FXML
    private TableColumn<PrietenDTO,String> tableColumnLastName;
    @FXML
    private TableColumn<PrietenDTO,String> tableColumnEmail;
    @FXML
    private TableColumn<PrietenDTO,String> tableColumnDate;
    @FXML
    private TableColumn<PrietenDTO,PrietenDTO> tableColumnUnfriendButton;
    @FXML
    private TableColumn<PrietenDTO,PrietenDTO> tableColumnConversationButton;

    @FXML
    private Pagination paginationTableView;

    @FXML
    private TextField textFieldSearch;

    public void setPage(UsersPage usersPage, Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.usersPage = usersPage;
        usersPage.getService().getPrietenieService().addObserver(this);
        initModel();
    }

    @FXML
    public void initialize() {
        tableColumnFirstName.setCellValueFactory(new PropertyValueFactory<PrietenDTO, String>("firstName"));
        tableColumnLastName.setCellValueFactory(new PropertyValueFactory<PrietenDTO, String>("lastName"));
        tableColumnEmail.setCellValueFactory(new PropertyValueFactory<PrietenDTO, String>("email"));
        tableColumnDate.setCellValueFactory(new PropertyValueFactory<PrietenDTO, String>("date"));

        Callback<TableColumn<PrietenDTO,PrietenDTO>, TableCell<PrietenDTO,PrietenDTO>> cellFactory_unfriend = new Callback<TableColumn<PrietenDTO,PrietenDTO>, TableCell<PrietenDTO,PrietenDTO>>() {
            @Override
            public TableCell<PrietenDTO,PrietenDTO> call(TableColumn<PrietenDTO,PrietenDTO> param) {

                return new TableCell<PrietenDTO,PrietenDTO>() {
                    final Button unfriendButton = new Button("Sterge");

                    {
                        unfriendButton.setOnAction(event -> {
                            PrietenDTO selectedItem = tableView.getSelectionModel().getSelectedItem();
                            deleteFriendship(selectedItem);
                        });
                        unfriendButton.setStyle("-fx-background-color: #581845;-fx-border-width: 0;-fx-text-alignment: center;-fx-text-fill: #fcf7f7;-fx-font-family: 'Times New Roman';-fx-cursor: 'OPEN_HAND';");
                        ImageView imageView = new ImageView();
                        Image image = new Image("/images/delete-icon.png");
                        imageView.setImage(image);
                        imageView.setFitHeight(18);
                        imageView.setFitWidth(18);
                        unfriendButton.setGraphic(imageView);
                    }

                    @Override
                    protected void updateItem(PrietenDTO item, boolean empty) {
                        super.updateItem(item, empty);
                        this.setAlignment(Pos.CENTER);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(unfriendButton);
                        }
                    }
                };

            }
        };
        tableColumnUnfriendButton.setCellFactory(cellFactory_unfriend);

        Callback<TableColumn<PrietenDTO,PrietenDTO>, TableCell<PrietenDTO,PrietenDTO>> cellFactory_conversation = new Callback<TableColumn<PrietenDTO,PrietenDTO>, TableCell<PrietenDTO,PrietenDTO>>() {
            @Override
            public TableCell<PrietenDTO,PrietenDTO> call(TableColumn<PrietenDTO,PrietenDTO> param) {

                return new TableCell<PrietenDTO,PrietenDTO>() {
                    final Button conversationButton = new Button("Vezi conversatia");

                    {
                        conversationButton.setOnAction(event -> {
                            PrietenDTO selectedItem = tableView.getSelectionModel().getSelectedItem();
                            openConversation(selectedItem);
                        });
                        conversationButton.setStyle("-fx-background-color: #581845;-fx-border-width: 0;-fx-text-alignment: center;-fx-text-fill: #fcf7f7;-fx-font-family: 'Times New Roman';-fx-cursor: 'OPEN_HAND';");
                        ImageView imageView = new ImageView();
                        Image image = new Image("/images/conversation_betweenFriends-icon.png");
                        imageView.setImage(image);
                        imageView.setFitHeight(18);
                        imageView.setFitWidth(18);
                        conversationButton.setGraphic(imageView);
                    }

                    @Override
                    protected void updateItem(PrietenDTO item, boolean empty) {
                        super.updateItem(item, empty);
                        this.setAlignment(Pos.CENTER);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(conversationButton);
                        }
                    }
                };

            }
        };
        tableColumnConversationButton.setCellFactory(cellFactory_conversation);

        tableView.setItems(friendsModel);
    }

    private void initModel() {
        int total_pages = (int) (Math.ceil(usersPage.getService().getPrietenieService().number_of_friends(usersPage.getConnected_user().getId()) * 1.0 / Constants.NUMBER_OF_RECORDS_ON_PAGE));
        if (total_pages == 0) total_pages = 1;
        paginationTableView.setPageCount(total_pages);
        paginationTableView.setCurrentPageIndex(0);
        paginationTableView.currentPageIndexProperty().addListener(
                ((observable, oldValue, newValue) -> updateFriendshipsTableView(newValue.intValue()))
        );
        updateFriendshipsTableView(0);
    }

    private void updateFriendshipsTableView(int page) {
        List<PrietenDTO> friendshipList = usersPage.getService().getUtilizatorService().getFriends_forUser_PAGED(page, usersPage.getConnected_user().getId());
        friendsModel.setAll(friendshipList);
    }

    @Override
    public void update(FriendshipChangeEvent friendshipChangeEvent) {
        initModel();
    }

    public void deleteFriendship(PrietenDTO selected) {
        if (selected != null) {
            try {
                String nume_prieten = selected.getFirstName() + " " + selected.getLastName();
                Prietenie deleted = usersPage.getService().getPrietenieService().deleteFriendship(new Tuple<>(usersPage.getConnected_user().getId(), selected.getUser().getId()));
                if (deleted != null) {
                    MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Stergere", "Prietenia dintre tine si " + nume_prieten + " a fost stearsa cu succes!");
                }
            }
            catch (Exception ex) {
                MessageAlert.showErrorMessage(null, ex.getMessage());
            }
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp!");
        }
    }

    public void openConversation(PrietenDTO prietenDTO) {
        if (prietenDTO != null) {
            try {
                FXMLLoader loader = new FXMLLoader();
                loader.setLocation(getClass().getResource("/views/conversationView.fxml"));
                AnchorPane root = loader.load();
                Stage dialogStage = new Stage();
                dialogStage.setTitle("Conversatie");
                dialogStage.initModality(Modality.WINDOW_MODAL);
                dialogStage.setWidth(650);
                Scene scene = new Scene(root);
                dialogStage.setScene(scene);

                ConversationController conversationController = loader.getController();
                conversationController.setService(usersPage, dialogStage, prietenDTO.getUser());
                dialogStage.show();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp!");
        }
    }

    @FXML
    public void handleReset() {
        tableView.setItems(friendsModel);
    }

    @FXML
    public void handleTextFieldSearch() {
        ObservableList<PrietenDTO> observableList = FXCollections.observableArrayList(usersPage.getService().getUtilizatorService().getAllFriends_forUser(usersPage.getConnected_user().getId()));
        FilteredList<PrietenDTO> filteredList = new FilteredList<PrietenDTO>(observableList, b -> true);

        textFieldSearch.textProperty().addListener(((observable, oldValue, newValue) -> {
            filteredList.setPredicate(prietenDTO -> {
                //daca textField-ul este gol, afisam toate prieteniile
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                String lowerCaseFilter = newValue.toLowerCase();

                if (prietenDTO.getFirstName().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                else if (prietenDTO.getLastName().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                else if (prietenDTO.getEmail().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                else {
                    return false;
                }
            });
        }));

        SortedList<PrietenDTO> sortedList = new SortedList<>(filteredList);
        sortedList.comparatorProperty().bind(tableView.comparatorProperty());
        tableView.setItems(sortedList);
    }
}
