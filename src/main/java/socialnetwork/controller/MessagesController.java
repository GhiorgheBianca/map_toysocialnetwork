package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import socialnetwork.domain.*;
import socialnetwork.service.SuperService;
import socialnetwork.utils.events.FriendRequestChangeEvent;
import socialnetwork.utils.events.MessageChangeEvent;
import socialnetwork.utils.observer.Observer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MessagesController implements Observer<MessageChangeEvent> {
    private UsersPage usersPage;
    private ObservableList<MesajDTO> model_sent = FXCollections.observableArrayList();
    private ObservableList<MesajDTO> model_received = FXCollections.observableArrayList();
    private Stage dialogStage;

    @FXML
    TableView<MesajDTO> tableViewSentMessages;
    @FXML
    TableColumn<MesajDTO,String> tableColumnId_Sent;
    @FXML
    TableColumn<MesajDTO,String> tableColumnFrom_Sent;
    @FXML
    TableColumn<MesajDTO,String> tableColumnTo_Sent;
    @FXML
    TableColumn<MesajDTO,String> tableColumnMessage_Sent;
    @FXML
    TableColumn<MesajDTO,String> tableColumnDate_Sent;
    @FXML
    TableColumn<MesajDTO,String> tableColumnIdReply_Sent;

    @FXML
    TableView<MesajDTO> tableViewReceivedMessages;
    @FXML
    TableColumn<MesajDTO,String> tableColumnId_Received;
    @FXML
    TableColumn<MesajDTO,String> tableColumnFrom_Received;
    @FXML
    TableColumn<MesajDTO,String> tableColumnTo_Received;
    @FXML
    TableColumn<MesajDTO,String> tableColumnMessage_Received;
    @FXML
    TableColumn<MesajDTO,String> tableColumnDate_Received;
    @FXML
    TableColumn<MesajDTO,String> tableColumnIdReply_Received;

    @FXML
    ComboBox<Utilizator> comboBoxUsers = new ComboBox<Utilizator>();

    public void setService(UsersPage usersPage,  Stage stage) {
        this.usersPage = usersPage;
        usersPage.getService().getMesajService().addObserver(this);
        initModel();
        this.dialogStage = stage;
        this.initVisualObjects();
    }

    @FXML
    public void initialize() {
        tableColumnId_Sent.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("id"));
        tableColumnFrom_Sent.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("from"));
        tableColumnTo_Sent.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("to"));
        tableColumnMessage_Sent.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("message"));
        tableColumnDate_Sent.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("date"));
        tableColumnIdReply_Sent.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("id_reply"));
        tableViewSentMessages.setItems(model_sent);

        tableColumnId_Received.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("id"));
        tableColumnFrom_Received.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("from"));
        tableColumnTo_Received.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("to"));
        tableColumnMessage_Received.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("message"));
        tableColumnDate_Received.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("date"));
        tableColumnIdReply_Received.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("id_reply"));
        tableViewReceivedMessages.setItems(model_received);
    }

    private void initModel() {
        Iterable<MesajDTO> sent_requests = usersPage.getService().getMesajService().getMessages_sentByUser(usersPage.getConnected_user().getId());
        List<MesajDTO> sent_requestList = StreamSupport
                .stream(sent_requests.spliterator(), false)
                .collect(Collectors.toList());
        model_sent.setAll(sent_requestList);

        Iterable<MesajDTO> received_requests = usersPage.getService().getMesajService().getMessages_receivedByUser(usersPage.getConnected_user().getId());
        List<MesajDTO> received_requestList = StreamSupport
                .stream(received_requests.spliterator(), false)
                .collect(Collectors.toList());
        model_received.setAll(received_requestList);
    }

    private void initVisualObjects() {
        List<Utilizator> all_users = StreamSupport
                .stream(usersPage.getService().getUtilizatorService().getAllUsers().spliterator(), false)
                .collect(Collectors.toList());

        ObservableList<Utilizator> observableUserList = FXCollections.observableArrayList(all_users);
        comboBoxUsers.setItems(observableUserList);

        comboBoxUsers.setCellFactory(new Callback<ListView<Utilizator>, ListCell<Utilizator>>() {
            @Override
            public ListCell<Utilizator> call(ListView<Utilizator> param) {
                return new ListCell<Utilizator>() {
                    @Override
                    protected void updateItem(Utilizator item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item == null || empty) {
                            setText(null);
                        } else {
                            setText("Email: " + item.getEmail() + "; Nume: " + item.getFirstName() + "; Prenume: " + item.getLastName());
                        }
                    }
                };
            }
        });

        comboBoxUsers.setConverter(new StringConverter<Utilizator>() {
            @Override
            public String toString(Utilizator user) {
                if (user == null) {
                    return null;
                }
                else {
                    return "Email: " + user.getEmail() + "; Nume: " + user.getFirstName() + "; Prenume: " + user.getLastName();
                }
            }

            @Override
            public Utilizator fromString(String string) {
                return null;
            }
        });
    }

    @Override
    public void update(MessageChangeEvent messageChangeEvent) {
        initModel();
    }

    @FXML
    public void handleSendMessage(javafx.event.ActionEvent ev) {
        this.sendMessageDialog();
    }

    public void sendMessageDialog() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/sendMessageView.fxml"));
            AnchorPane root = loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Editare mesaj");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.setWidth(900);
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);

            SendMessageController sendMessageController = loader.getController();
            sendMessageController.setService(usersPage, dialogStage);
            dialogStage.show();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    public void handleRespondMessage(javafx.event.ActionEvent ev) {
        MesajDTO message_dto = tableViewReceivedMessages.getSelectionModel().getSelectedItem();
        if (message_dto != null) {
            Mesaj message = new Mesaj(message_dto.getId(), message_dto.getFrom_id(), message_dto.getTo_ids(), message_dto.getMessage(), message_dto.getDate(), message_dto.getId_reply());
            this.respondMessageDialog(message);
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din tabelul cu mesaje primite!");
        }
    }

    public void respondMessageDialog(Mesaj message) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/respondMessageView.fxml"));
            AnchorPane root = loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Editare mesaj");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.setWidth(900);
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);

            RespondMessageController respondMessageController = loader.getController();
            respondMessageController.setService(usersPage, dialogStage, message);
            dialogStage.show();
        }
        catch(IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    public void handleShowConversation(javafx.event.ActionEvent ev) {
        Utilizator user = comboBoxUsers.getSelectionModel().getSelectedItem();
        if (user != null) {
            this.showConversationDialog(user);
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din comboBox!");
        }
    }

    public void showConversationDialog(Utilizator user) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/conversationView.fxml"));
            AnchorPane root = loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Conversatie");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.setWidth(900);
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);

            ConversationController conversationController = loader.getController();
            conversationController.setService(usersPage, dialogStage, user);
            dialogStage.show();
        }
        catch(IOException ex) {
            ex.printStackTrace();
        }
    }
}
