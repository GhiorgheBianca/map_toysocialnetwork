package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import socialnetwork.domain.Mesaj;
import socialnetwork.domain.UsersPage;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.SuperService;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class SendMessageController {
    @FXML
    private TextArea textFieldMessage;

    @FXML
    private ComboBox<Utilizator> comboBoxUsers = new ComboBox<Utilizator>();

    @FXML
    private ListView<Utilizator> listViewUsers;

    private UsersPage usersPage;
    private Stage dialogStage;

    @FXML
    private void initialize() {

    }

    private void initVisualObjects() {
        List<Utilizator> all_users = StreamSupport
                .stream(usersPage.getService().getUtilizatorService().getAllUsers().spliterator(), false)
                .collect(Collectors.toList());

        ObservableList<Utilizator> observableUserList = FXCollections.observableArrayList(all_users);
        comboBoxUsers.setItems(observableUserList);

        comboBoxUsers.setCellFactory(new Callback<ListView<Utilizator>, ListCell<Utilizator>>() {
            @Override
            public ListCell<Utilizator> call(ListView<Utilizator> param) {
                return new ListCell<Utilizator>() {
                    @Override
                    protected void updateItem(Utilizator item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item == null || empty) {
                            setText(null);
                        } else {
                            setText("Email: " + item.getEmail() + "; Nume: " + item.getFirstName() + "; Prenume: " + item.getLastName());
                        }
                    }
                };
            }
        });

        comboBoxUsers.setConverter(new StringConverter<Utilizator>() {
            @Override
            public String toString(Utilizator user) {
                if (user == null) {
                    return null;
                }
                else {
                    return "Email: " + user.getEmail() + "; Nume: " + user.getFirstName() + "; Prenume: " + user.getLastName();
                }
            }

            @Override
            public Utilizator fromString(String string) {
                return null;
            }
        });

        listViewUsers.setCellFactory(lv -> new ListCell<Utilizator>() {
            @Override
            public void updateItem(Utilizator user, boolean empty) {
                super.updateItem(user, empty) ;
                setText(empty ? null : "Email: " + user.getEmail() + "; Nume: " + user.getFirstName() + "; Prenume: " + user.getLastName());
            }
        });
    }

    public void setService(UsersPage usersPage, Stage stage) {
        this.usersPage = usersPage;
        this.dialogStage = stage;
        this.initVisualObjects();
    }

    @FXML
    public void handleSendMessage() {
        try {
            String text_message = textFieldMessage.getText();

            List<Long> to_users = new ArrayList<>();
            for (Utilizator user : listViewUsers.getItems()) {
                to_users.add(user.getId());
            }

            usersPage.getService().getMesajService().sendNewMessage(usersPage.getConnected_user().getId(), to_users, text_message);
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Mesaj", "Mesajul a fost trimis cu succes!");
            dialogStage.close();
        }
        catch (Exception ex) {
            MessageAlert.showErrorMessage(null, ex.getMessage());
        }
    }

    @FXML
    public void handleAddUser() {
        Utilizator selected_user = comboBoxUsers.getSelectionModel().getSelectedItem();
        if (selected_user != null) {
            listViewUsers.getItems().add(selected_user);
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din comboBox!");
        }
    }

    @FXML
    public void handleDeleteUser() {
        Utilizator selected_user = listViewUsers.getSelectionModel().getSelectedItem();
        if (selected_user != null) {
            listViewUsers.getItems().remove(selected_user);
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din listView!");
        }
    }

    @FXML
    public void handleCancel() {
        dialogStage.close();
    }
}
