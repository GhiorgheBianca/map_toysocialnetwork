package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import socialnetwork.domain.*;
import socialnetwork.service.SuperService;
import socialnetwork.service.UtilizatorService;
import socialnetwork.utils.events.FriendRequestChangeEvent;
import socialnetwork.utils.events.FriendshipChangeEvent;
import socialnetwork.utils.observer.Observer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class FriendRequestsController implements Observer<FriendRequestChangeEvent> {
    private UsersPage usersPage;
    private ObservableList<CerereDTO> model_sent = FXCollections.observableArrayList();
    private ObservableList<CerereDTO> model_received = FXCollections.observableArrayList();
    private Stage dialogStage;

    @FXML
    TableView<CerereDTO> tableViewSentRequests;
    @FXML
    TableColumn<CerereDTO,String> tableColumnFirstName_Sent;
    @FXML
    TableColumn<CerereDTO,String> tableColumnLastName_Sent;
    @FXML
    TableColumn<CerereDTO,String> tableColumnEmail_Sent;
    @FXML
    TableColumn<CerereDTO,String> tableColumnState_Sent;
    @FXML
    TableColumn<CerereDTO,String> tableColumnDate_Sent;

    @FXML
    TableView<CerereDTO> tableViewReceivedRequests;
    @FXML
    TableColumn<CerereDTO,String> tableColumnFirstName_Received;
    @FXML
    TableColumn<CerereDTO,String> tableColumnLastName_Received;
    @FXML
    TableColumn<CerereDTO,String> tableColumnEmail_Received;
    @FXML
    TableColumn<CerereDTO,String> tableColumnState_Received;
    @FXML
    TableColumn<CerereDTO,String> tableColumnDate_Received;

    @FXML
    ComboBox<Utilizator> comboBoxEmails = new ComboBox<Utilizator>();
    @FXML
    ComboBox<String> comboBoxStatus = new ComboBox<String>();

    public void setService(UsersPage usersPage,  Stage stage) {
        this.usersPage = usersPage;
        usersPage.getService().getCerereService().addObserver(this);
        this.initModel();
        this.dialogStage = stage;
    }

    @FXML
    public void initialize() {
        tableColumnFirstName_Sent.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("firstName"));
        tableColumnLastName_Sent.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("lastName"));
        tableColumnEmail_Sent.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("email"));
        tableColumnDate_Sent.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("date"));
        tableColumnState_Sent.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("state"));
        tableViewSentRequests.setItems(model_sent);

        tableColumnFirstName_Received.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("firstName"));
        tableColumnLastName_Received.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("lastName"));
        tableColumnEmail_Received.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("email"));
        tableColumnDate_Received.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("date"));
        tableColumnState_Received.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("state"));
        tableViewReceivedRequests.setItems(model_received);

        List<String> status_list = new ArrayList<>();
        status_list.add("approved");
        status_list.add("rejected");
        ObservableList<String> observableStatusList = FXCollections.observableArrayList(status_list);
        comboBoxStatus.setItems(observableStatusList);
    }

    private void initModel() {
        Iterable<CerereDTO> sent_requests = usersPage.getService().getCerereService().getRequests_sentByUser(usersPage.getConnected_user().getId());
        List<CerereDTO> sent_requestList = StreamSupport
                .stream(sent_requests.spliterator(), false)
                .collect(Collectors.toList());
        model_sent.setAll(sent_requestList);

        Iterable<CerereDTO> received_requests = usersPage.getService().getCerereService().getRequests_receivedByUser(usersPage.getConnected_user().getId());
        List<CerereDTO> received_requestList = StreamSupport
                .stream(received_requests.spliterator(), false)
                .collect(Collectors.toList());
        model_received.setAll(received_requestList);

        ObservableList<Utilizator> observableUserList = FXCollections.observableArrayList(usersPage.getService().getUtilizatorService().getNonFriends_ForUser(usersPage.getService().getUtilizatorService().findUser_id(usersPage.getConnected_user().getId())));
        comboBoxEmails.setItems(observableUserList);
        this.initComboBoxEmails();
    }

    private void initComboBoxEmails() {
        comboBoxEmails.setCellFactory(new Callback<ListView<Utilizator>, ListCell<Utilizator>>() {
            @Override
            public ListCell<Utilizator> call(ListView<Utilizator> param) {
                return new ListCell<Utilizator>() {
                    @Override
                    protected void updateItem(Utilizator item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item == null || empty) {
                            setText(null);
                        } else {
                            setText("Email: " + item.getEmail() + "; Nume: " + item.getFirstName() + "; Prenume: " + item.getLastName());
                        }
                    }
                };
            }
        });

        comboBoxEmails.setConverter(new StringConverter<Utilizator>() {
            @Override
            public String toString(Utilizator user) {
                if (user == null) {
                    return null;
                }
                else {
                    return "Email: " + user.getEmail() + "; Nume: " + user.getFirstName() + "; Prenume: " + user.getLastName();
                }
            }

            @Override
            public Utilizator fromString(String string) {
                return null;
            }
        });
    }

    @Override
    public void update(FriendRequestChangeEvent friendRequestChangeEvent) {
        initModel();
    }

    @FXML
    public void handleSendRequest(javafx.event.ActionEvent ev) {
        Utilizator selected_user = comboBoxEmails.getSelectionModel().getSelectedItem();
        if (selected_user != null) {
            try {
                String nume_prieten = selected_user.getFirstName() + " " + selected_user.getLastName();
                usersPage.getService().getCerereService().sendRequest(usersPage.getConnected_user().getId(), selected_user.getId());
                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Cerere de Prietenie", "Cererea de prietenie catre " + nume_prieten + " a fost trimisa cu succes!");
            }
            catch (Exception ex) {
                MessageAlert.showErrorMessage(null, ex.getMessage());
            }
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din comboBox!");
        }
    }

    @FXML
    public void handleDeleteRequest(javafx.event.ActionEvent ev) {
        CerereDTO selected = tableViewSentRequests.getSelectionModel().getSelectedItem();
        if (selected != null) {
            try {
                String nume_prieten = selected.getFirstName() + " " + selected.getLastName();
                CerereDePrietenie deleted = usersPage.getService().getCerereService().deleteRequest(new Tuple<>(usersPage.getConnected_user().getId(), selected.getUser().getId()));
                if (deleted != null) {
                    MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Stergere", "Cererea de prietenie catre " + nume_prieten + " a fost stearsa cu succes!");
                }
            }
            catch (Exception ex) {
                MessageAlert.showErrorMessage(null, ex.getMessage());
            }
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din tabel!");
        }
    }

    @FXML
    public void handleUpdateRequest(javafx.event.ActionEvent ev) {
        CerereDTO selected_request = tableViewReceivedRequests.getSelectionModel().getSelectedItem();
        String selected_answer = comboBoxStatus.getSelectionModel().getSelectedItem();
        if (selected_answer != null && selected_request != null) {
            try {
                String nume_prieten = selected_request.getFirstName() + " " + selected_request.getLastName();
                usersPage.getService().getCerereService().answerRequest(usersPage.getConnected_user().getId(), selected_request.getUser().getId(), selected_answer);

                if (selected_answer.equals("approved")) {
                    MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Cerere de Prietenie", "Cererea de prietenie de la " + nume_prieten + " a fost acceptata! Acum sunteti prieteni.");
                }
                else {
                    MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Cerere de Prietenie", "Cererea de prietenie de la " + nume_prieten + " a fost respinsa!");
                }
            }
            catch (Exception ex) {
                MessageAlert.showErrorMessage(null, ex.getMessage());
            }
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din comboBox si/sau tabel!");
        }
    }
}
