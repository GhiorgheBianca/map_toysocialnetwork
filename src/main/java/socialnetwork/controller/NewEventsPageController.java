package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import socialnetwork.domain.*;
import socialnetwork.utils.Constants;
import socialnetwork.utils.events.EventChangeEvent;
import socialnetwork.utils.events.MessageChangeEvent;
import socialnetwork.utils.observer.Observer;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class NewEventsPageController implements Observer<EventChangeEvent> {
    private UsersPage usersPage;
    private ObservableList<EvenimentDTO> model_goesTo = FXCollections.observableArrayList();
    private ObservableList<EvenimentDTO> model_doesntGoTo = FXCollections.observableArrayList();
    private Stage dialogStage;

    @FXML
    private TableView<EvenimentDTO> tableView_goesTo;
    @FXML
    private TableColumn<EvenimentDTO,String> tableColumnName_goesTo;
    @FXML
    private TableColumn<EvenimentDTO,String> tableColumnDescription_goesTo;
    @FXML
    private TableColumn<EvenimentDTO,String> tableColumnType_goesTo;
    @FXML
    private TableColumn<EvenimentDTO,String> tableColumnStartDate_goesTo;
    @FXML
    private TableColumn<EvenimentDTO,String> tableColumnFinalDate_goesTo;
    @FXML
    private TableColumn<EvenimentDTO,String> tableColumnParticipants_goesTo;
    @FXML
    private TableColumn<EvenimentDTO,EvenimentDTO> tableColumnUnsubscribeButton_goesTo;

    @FXML
    private TableView<EvenimentDTO> tableView_doesntGoTo;
    @FXML
    private TableColumn<EvenimentDTO,String> tableColumnName_doesntGoTo;
    @FXML
    private TableColumn<EvenimentDTO,String> tableColumnDescription_doesntGoTo;
    @FXML
    private TableColumn<EvenimentDTO,String> tableColumnType_doesntGoTo;
    @FXML
    private TableColumn<EvenimentDTO,String> tableColumnStartDate_doesntGoTo;
    @FXML
    private TableColumn<EvenimentDTO,String> tableColumnFinalDate_doesntGoTo;
    @FXML
    private TableColumn<EvenimentDTO,String> tableColumnParticipants_doesntGoTo;
    @FXML
    private TableColumn<EvenimentDTO,EvenimentDTO> tableColumnSubscribeButton_doesntGoTo;

    @FXML
    private Pagination paginationTable_goesTo;
    @FXML
    private Pagination paginationTable_doesntGoTo;

    public void setPage(UsersPage usersPage,  Stage stage) {
        this.usersPage = usersPage;
        usersPage.getService().getEvenimentService().addObserver(this);
        initModel();
        this.dialogStage = stage;
    }

    @FXML
    public void initialize() {
        initialize_goesTo();
        initialize_doesntGoTo();
    }

    private void initialize_goesTo() {
        tableColumnName_goesTo.setCellValueFactory(new PropertyValueFactory<EvenimentDTO, String>("name"));
        tableColumnDescription_goesTo.setCellValueFactory(new PropertyValueFactory<EvenimentDTO, String>("description"));
        tableColumnType_goesTo.setCellValueFactory(new PropertyValueFactory<EvenimentDTO, String>("type"));
        tableColumnStartDate_goesTo.setCellValueFactory(new PropertyValueFactory<EvenimentDTO, String>("start_date"));
        tableColumnFinalDate_goesTo.setCellValueFactory(new PropertyValueFactory<EvenimentDTO, String>("final_date"));
        tableColumnParticipants_goesTo.setCellValueFactory(new PropertyValueFactory<EvenimentDTO, String>("participants_emails"));

        Callback<TableColumn<EvenimentDTO,EvenimentDTO>, TableCell<EvenimentDTO,EvenimentDTO>> cellFactory = new Callback<TableColumn<EvenimentDTO,EvenimentDTO>, TableCell<EvenimentDTO,EvenimentDTO>>() {
            @Override
            public TableCell<EvenimentDTO,EvenimentDTO> call(TableColumn<EvenimentDTO,EvenimentDTO> param) {

                return new TableCell<EvenimentDTO,EvenimentDTO>() {
                    final Button unsubscribeButton = new Button("Dezabonare");

                    {
                        unsubscribeButton.setOnAction(event -> {
                            EvenimentDTO selectedItem = tableView_goesTo.getSelectionModel().getSelectedItem();
                            unsubscribeEvent(selectedItem);
                        });
                        unsubscribeButton.setStyle("-fx-background-color: #581845;-fx-border-width: 0;-fx-text-alignment: center;-fx-text-fill: #fcf7f7;-fx-font-family: 'Times New Roman';-fx-cursor: 'OPEN_HAND';");
                        ImageView imageView = new ImageView();
                        Image image = new Image("/images/unsubscribe-icon.png");
                        imageView.setImage(image);
                        imageView.setFitHeight(18);
                        imageView.setFitWidth(18);
                        unsubscribeButton.setGraphic(imageView);
                    }

                    @Override
                    protected void updateItem(EvenimentDTO item, boolean empty) {
                        super.updateItem(item, empty);
                        this.setAlignment(Pos.CENTER);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(unsubscribeButton);
                        }
                    }
                };

            }
        };
        tableColumnUnsubscribeButton_goesTo.setCellFactory(cellFactory);

        tableView_goesTo.setItems(model_goesTo);
    }

    private void initialize_doesntGoTo() {
        tableColumnName_doesntGoTo.setCellValueFactory(new PropertyValueFactory<EvenimentDTO, String>("name"));
        tableColumnDescription_doesntGoTo.setCellValueFactory(new PropertyValueFactory<EvenimentDTO, String>("description"));
        tableColumnType_doesntGoTo.setCellValueFactory(new PropertyValueFactory<EvenimentDTO, String>("type"));
        tableColumnStartDate_doesntGoTo.setCellValueFactory(new PropertyValueFactory<EvenimentDTO, String>("start_date"));
        tableColumnFinalDate_doesntGoTo.setCellValueFactory(new PropertyValueFactory<EvenimentDTO, String>("final_date"));
        tableColumnParticipants_doesntGoTo.setCellValueFactory(new PropertyValueFactory<EvenimentDTO, String>("participants_emails"));

        Callback<TableColumn<EvenimentDTO,EvenimentDTO>, TableCell<EvenimentDTO,EvenimentDTO>> cellFactory = new Callback<TableColumn<EvenimentDTO,EvenimentDTO>, TableCell<EvenimentDTO,EvenimentDTO>>() {
            @Override
            public TableCell<EvenimentDTO,EvenimentDTO> call(TableColumn<EvenimentDTO,EvenimentDTO> param) {

                return new TableCell<EvenimentDTO,EvenimentDTO>() {
                    final Button subscribeButton = new Button("Abonare");

                    {
                        subscribeButton.setOnAction(event -> {
                            EvenimentDTO selectedItem = tableView_doesntGoTo.getSelectionModel().getSelectedItem();
                            subscribeEvent(selectedItem);
                        });
                        subscribeButton.setStyle("-fx-background-color: #581845;-fx-border-width: 0;-fx-text-alignment: center;-fx-text-fill: #fcf7f7;-fx-font-family: 'Times New Roman';-fx-cursor: 'OPEN_HAND';");
                        ImageView imageView = new ImageView();
                        Image image = new Image("/images/subscribe-icon.png");
                        imageView.setImage(image);
                        imageView.setFitHeight(18);
                        imageView.setFitWidth(18);
                        subscribeButton.setGraphic(imageView);
                    }

                    @Override
                    protected void updateItem(EvenimentDTO item, boolean empty) {
                        super.updateItem(item, empty);
                        this.setAlignment(Pos.CENTER);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(subscribeButton);
                        }
                    }
                };

            }
        };
        tableColumnSubscribeButton_doesntGoTo.setCellFactory(cellFactory);

        tableView_doesntGoTo.setItems(model_doesntGoTo);
    }

    private void initModel() {
        this.init_pagination_goesTo();
        this.init_pagination_doesntGoTo();
    }

    private void init_pagination_goesTo() {
        int total_pages = (int) (Math.ceil(usersPage.getService().getEvenimentService().number_of_events_goesTo(usersPage.getConnected_user().getId()) * 1.0 / Constants.NUMBER_OF_RECORDS_ON_PAGE));
        if (total_pages == 0) total_pages = 1;
        paginationTable_goesTo.setPageCount(total_pages);
        paginationTable_goesTo.setCurrentPageIndex(0);
        paginationTable_goesTo.currentPageIndexProperty().addListener(
                ((observable, oldValue, newValue) -> updateEventsTable_goesTo(newValue.intValue()))
        );
        this.updateEventsTable_goesTo(0);
    }

    private void updateEventsTable_goesTo(int page) {
        List<EvenimentDTO> eventList = usersPage.getService().getEvenimentService().getEvents_UserGoesTo_PAGED(page, usersPage.getConnected_user().getId());
        model_goesTo.setAll(eventList);
    }

    private void init_pagination_doesntGoTo() {
        int total_pages = (int) (Math.ceil(usersPage.getService().getEvenimentService().number_of_events_doesntGoTo(usersPage.getConnected_user().getId()) * 1.0 / Constants.NUMBER_OF_RECORDS_ON_PAGE));
        if (total_pages == 0) total_pages = 1;
        paginationTable_doesntGoTo.setPageCount(total_pages);
        paginationTable_doesntGoTo.setCurrentPageIndex(0);
        paginationTable_doesntGoTo.currentPageIndexProperty().addListener(
                ((observable, oldValue, newValue) -> updateEventsTable_doesntGoTo(newValue.intValue()))
        );
        this.updateEventsTable_doesntGoTo(0);
    }

    private void updateEventsTable_doesntGoTo(int page) {
        List<EvenimentDTO> eventList = usersPage.getService().getEvenimentService().getEvents_UserDoesntGoTo_PAGED(page, usersPage.getConnected_user().getId());
        model_doesntGoTo.setAll(eventList);
    }

    @Override
    public void update(EventChangeEvent eventChangeEvent) {
        initModel();
    }

    public void unsubscribeEvent(EvenimentDTO selected_item) {
        if (selected_item != null) {
            try {
                usersPage.getService().getEvenimentService().removeUser_fromEvent(usersPage.getConnected_user().getId(), selected_item.getId());
                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Eveniment", "V-ati dezabonat cu succes de la evenimentul '" + selected_item.getName() + "'!");
            }
            catch (Exception ex) {
                MessageAlert.showErrorMessage(null, ex.getMessage());
                ex.printStackTrace();
            }
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din primul tabel!");
        }
    }

    public void subscribeEvent(EvenimentDTO selected_item) {
        if (selected_item != null) {
            try {
                usersPage.getService().getEvenimentService().addUser_toEvent(usersPage.getConnected_user().getId(), selected_item.getId());
                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Eveniment", "V-ati abonat cu succes la evenimentul '" + selected_item.getName() + "'!");
            }
            catch (Exception ex) {
                MessageAlert.showErrorMessage(null, ex.getMessage());
                ex.printStackTrace();
            }
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din al doilea tabel!");
        }
    }

    @FXML
    public void handleCreateNewEvent() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/createEventView.fxml"));
            AnchorPane root = loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Creeaza un eveniment");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.setWidth(900);
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);

            CreateNewEventController createNewEventController = loader.getController();
            createNewEventController.setPage(usersPage, dialogStage);
            dialogStage.show();

        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
