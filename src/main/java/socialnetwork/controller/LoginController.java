package socialnetwork.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import socialnetwork.domain.Eveniment;
import socialnetwork.domain.Notificare;
import socialnetwork.domain.UsersPage;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.SuperService;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.mindrot.jbcrypt.BCrypt;

public class LoginController {
    @FXML
    private TextField textFieldEmail;
    @FXML
    private TextField PasswordField;

    private UsersPage usersPage;
    private Stage dialogStage;

    @FXML
    private void initialize() {

    }

    public void setPage(UsersPage usersPage, Stage stage) {
        this.usersPage = usersPage;
        this.dialogStage = stage;
    }

    @FXML
    public void handleLogin() {
        String email, password;
        email = textFieldEmail.getText();
        password = PasswordField.getText();

        Utilizator user = usersPage.getService().getUtilizatorService().findUser_email(email);
        if (user == null) {
            MessageAlert.showErrorMessage(null, "Email-ul si/sau parola introduse sunt incorecte!");
        }
        else {
            if (BCrypt.checkpw(password, user.getPassword())) {
                dialogStage.close();
                this.goToPageProfile(user);
            }
            else {
                MessageAlert.showErrorMessage(null, "Email-ul si/sau parola introduse sunt incorecte!");
            }
        }
    }

    public void goToPageProfile(Utilizator user) {
        try {
            FXMLLoader loader = new FXMLLoader();
            //loader.setLocation(getClass().getResource("/views/userProfileView.fxml"));
            loader.setLocation(getClass().getResource("/views/newUserPageView.fxml"));
            AnchorPane root = loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Pagina Utilizatorului");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.setWidth(1270);
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);

            NewUserPageController newUserPageController = loader.getController();
            usersPage.setConnected_user(user);
            newUserPageController.setPage(usersPage, dialogStage);

            this.updateNotifications();

            dialogStage.show();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void updateNotifications() {
        List<Long> interval = Arrays.asList(0L, 1L, 2L, 5L, 15L, 30L);

        for (Notificare notificare : usersPage.getService().getNotificareService().getAllNotifications()) {
            if (notificare.getTo().contains(usersPage.getConnected_user().getId()) && notificare.getType().equals("event")) {
                Eveniment eveniment = usersPage.getService().getEvenimentService().findEvent_id(Long.parseLong(notificare.getType_id()));

                Long daysTillEvent = Duration.between(LocalDateTime.now(), eveniment.getStart_date()).toDays();
                Long daysBetweenLastNotifications = Duration.between(notificare.getDate_time(), LocalDateTime.now()).toDays();

                if (interval.contains(daysTillEvent) && daysBetweenLastNotifications > 0) {
                    String descriere = "";
                    if (daysTillEvent == 1) {
                        descriere = "Mai este " + daysTillEvent.toString() + " zi pana la evenimentul " + eveniment.getName() + "!";
                    }
                    else {
                        descriere = "Mai sunt " + daysTillEvent.toString() + " zile pana la evenimentul " + eveniment.getName() + "!";
                    }
                    usersPage.getService().getNotificareService().updateNotification(notificare, descriere, usersPage.getConnected_user().getId());
                }
            }
        }
    }

    @FXML
    public void handleClickedCreateAccount() {
        this.goToCreateAccount();
    }

    public void goToCreateAccount() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/views/createAccountView.fxml"));
            AnchorPane root = loader.load();
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Pagina de Inregistrare");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            //dialogStage.setWidth(700);
            Scene scene = new Scene(root);
            dialogStage.setScene(scene);
            CreateAccountController createAccountController = loader.getController();

            createAccountController.setPage(usersPage, dialogStage);
            dialogStage.show();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    public void handleCancel() {
        dialogStage.close();
    }
}
