package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import socialnetwork.domain.*;
import socialnetwork.utils.Constants;
import socialnetwork.utils.events.FriendRequestChangeEvent;
import socialnetwork.utils.observer.Observer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class NewRequestPageController implements Observer<FriendRequestChangeEvent> {
    private UsersPage usersPage;
    private ObservableList<CerereDTO> model_sent = FXCollections.observableArrayList();
    private ObservableList<CerereDTO> model_received = FXCollections.observableArrayList();
    private Stage dialogStage;

    @FXML
    private TableView<CerereDTO> tableViewSentRequests;
    @FXML
    private TableColumn<CerereDTO,String> tableColumnFirstName_Sent;
    @FXML
    private TableColumn<CerereDTO,String> tableColumnLastName_Sent;
    @FXML
    private TableColumn<CerereDTO,String> tableColumnEmail_Sent;
    @FXML
    private TableColumn<CerereDTO,String> tableColumnState_Sent;
    @FXML
    private TableColumn<CerereDTO,String> tableColumnDate_Sent;
    @FXML
    private TableColumn<CerereDTO,CerereDTO> tableColumnDeleteButton_Sent;

    @FXML
    private TableView<CerereDTO> tableViewReceivedRequests;
    @FXML
    private TableColumn<CerereDTO,String> tableColumnFirstName_Received;
    @FXML
    private TableColumn<CerereDTO,String> tableColumnLastName_Received;
    @FXML
    private TableColumn<CerereDTO,String> tableColumnEmail_Received;
    @FXML
    private TableColumn<CerereDTO,String> tableColumnState_Received;
    @FXML
    private TableColumn<CerereDTO,String> tableColumnDate_Received;
    @FXML
    private TableColumn<CerereDTO,CerereDTO> tableColumnApproveButton_Received;
    @FXML
    private TableColumn<CerereDTO,CerereDTO> tableColumnRejectButton_Received;

    @FXML
    private ComboBox<Utilizator> comboBoxEmails = new ComboBox<Utilizator>();

    @FXML
    private Pagination paginationTableSent;
    @FXML
    private Pagination paginationTableReceived;

    public void setPage(UsersPage usersPage, Stage dialogStage) {
        this.dialogStage = dialogStage;
        this.usersPage = usersPage;
        usersPage.getService().getCerereService().addObserver(this);
        initModel();
    }

    @FXML
    public void initialize() {
        this.initialize_Sent();
        this.initialize_Received();
    }

    private void initialize_Sent() {
        tableColumnFirstName_Sent.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("firstName"));
        tableColumnLastName_Sent.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("lastName"));
        tableColumnEmail_Sent.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("email"));
        tableColumnDate_Sent.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("date"));
        tableColumnState_Sent.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("state"));

        Callback<TableColumn<CerereDTO,CerereDTO>, TableCell<CerereDTO,CerereDTO>> cellFactory = new Callback<TableColumn<CerereDTO,CerereDTO>, TableCell<CerereDTO,CerereDTO>>() {
            @Override
            public TableCell<CerereDTO,CerereDTO> call(TableColumn<CerereDTO,CerereDTO> param) {

                return new TableCell<CerereDTO,CerereDTO>() {
                    final Button deleteButton = new Button("Sterge");

                    {
                        deleteButton.setOnAction(event -> {
                            CerereDTO selectedItem = tableViewSentRequests.getSelectionModel().getSelectedItem();
                            deleteRequest(selectedItem);
                        });
                        deleteButton.setStyle("-fx-background-color: #581845;-fx-border-width: 0;-fx-text-alignment: center;-fx-text-fill: #fcf7f7;-fx-font-family: 'Times New Roman';-fx-cursor: 'OPEN_HAND';");
                        ImageView imageView = new ImageView();
                        Image image = new Image("/images/delete-icon.png");
                        imageView.setImage(image);
                        imageView.setFitHeight(18);
                        imageView.setFitWidth(18);
                        deleteButton.setGraphic(imageView);
                    }

                    @Override
                    protected void updateItem(CerereDTO item, boolean empty) {
                        super.updateItem(item, empty);
                        this.setAlignment(Pos.CENTER);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(deleteButton);
                        }
                    }
                };

            }
        };
        tableColumnDeleteButton_Sent.setCellFactory(cellFactory);

        tableViewSentRequests.setItems(model_sent);
    }

    private void initialize_Received() {
        tableColumnFirstName_Received.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("firstName"));
        tableColumnLastName_Received.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("lastName"));
        tableColumnEmail_Received.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("email"));
        tableColumnDate_Received.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("date"));
        tableColumnState_Received.setCellValueFactory(new PropertyValueFactory<CerereDTO, String>("state"));

        Callback<TableColumn<CerereDTO,CerereDTO>, TableCell<CerereDTO,CerereDTO>> cellFactory_approve = new Callback<TableColumn<CerereDTO,CerereDTO>, TableCell<CerereDTO,CerereDTO>>() {
            @Override
            public TableCell<CerereDTO,CerereDTO> call(TableColumn<CerereDTO,CerereDTO> param) {

                return new TableCell<CerereDTO,CerereDTO>() {
                    final Button approveButton = new Button("Aproba");

                    {
                        approveButton.setOnAction(event -> {
                            CerereDTO selectedItem = tableViewReceivedRequests.getSelectionModel().getSelectedItem();
                            updateRequest(selectedItem, "approved");
                        });
                        approveButton.setStyle("-fx-background-color: #581845;-fx-border-width: 0;-fx-text-alignment: center;-fx-text-fill: #fcf7f7;-fx-font-family: 'Times New Roman';-fx-cursor: 'OPEN_HAND';");
                        ImageView imageView = new ImageView();
                        Image image = new Image("/images/approve-icon.png");
                        imageView.setImage(image);
                        imageView.setFitHeight(18);
                        imageView.setFitWidth(18);
                        approveButton.setGraphic(imageView);
                    }

                    @Override
                    protected void updateItem(CerereDTO item, boolean empty) {
                        super.updateItem(item, empty);
                        this.setAlignment(Pos.CENTER);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(approveButton);
                        }
                    }
                };

            }
        };
        tableColumnApproveButton_Received.setCellFactory(cellFactory_approve);

        Callback<TableColumn<CerereDTO,CerereDTO>, TableCell<CerereDTO,CerereDTO>> cellFactory_reject = new Callback<TableColumn<CerereDTO,CerereDTO>, TableCell<CerereDTO,CerereDTO>>() {
            @Override
            public TableCell<CerereDTO,CerereDTO> call(TableColumn<CerereDTO,CerereDTO> param) {

                return new TableCell<CerereDTO,CerereDTO>() {
                    final Button rejectButton = new Button("Respinge");

                    {
                        rejectButton.setOnAction(event -> {
                            CerereDTO selectedItem = tableViewReceivedRequests.getSelectionModel().getSelectedItem();
                            updateRequest(selectedItem, "rejected");
                        });
                        rejectButton.setStyle("-fx-background-color: #581845;-fx-border-width: 0;-fx-text-alignment: center;-fx-text-fill: #fcf7f7;-fx-font-family: 'Times New Roman';-fx-cursor: 'OPEN_HAND';");
                        ImageView imageView = new ImageView();
                        Image image = new Image("/images/reject-icon.png");
                        imageView.setImage(image);
                        imageView.setFitHeight(18);
                        imageView.setFitWidth(18);
                        rejectButton.setGraphic(imageView);
                    }

                    @Override
                    protected void updateItem(CerereDTO item, boolean empty) {
                        super.updateItem(item, empty);
                        this.setAlignment(Pos.CENTER);
                        if (empty) {
                            setGraphic(null);
                        } else {
                            setGraphic(rejectButton);
                        }
                    }
                };

            }
        };
        tableColumnRejectButton_Received.setCellFactory(cellFactory_reject);

        tableViewReceivedRequests.setItems(model_received);
    }

    private void initModel() {
        this.init_paginationSent();
        this.init_paginationReceived();

        ObservableList<Utilizator> observableUserList = FXCollections.observableArrayList(usersPage.getService().getUtilizatorService().getNonFriends_ForUser(usersPage.getService().getUtilizatorService().findUser_id(usersPage.getConnected_user().getId())));
        comboBoxEmails.setItems(observableUserList);
        this.initComboBoxEmails();
    }

    private void init_paginationSent() {
        int total_pages = (int) (Math.ceil(usersPage.getService().getCerereService().number_of_requests_Sent(usersPage.getConnected_user().getId()) * 1.0 / Constants.NUMBER_OF_RECORDS_ON_PAGE));
        if (total_pages == 0) total_pages = 1;
        paginationTableSent.setPageCount(total_pages);
        paginationTableSent.setCurrentPageIndex(0);
        paginationTableSent.currentPageIndexProperty().addListener(
                ((observable, oldValue, newValue) -> updateRequestTable_Sent(newValue.intValue()))
        );
        this.updateRequestTable_Sent(0);
    }

    private void updateRequestTable_Sent(int page) {
        List<CerereDTO> requestList = usersPage.getService().getCerereService().getRequests_SentByUser_PAGED(page, usersPage.getConnected_user().getId());
        model_sent.setAll(requestList);
    }

    private void init_paginationReceived() {
        int total_pages = (int) (Math.ceil(usersPage.getService().getCerereService().number_of_requests_Received(usersPage.getConnected_user().getId()) * 1.0 / Constants.NUMBER_OF_RECORDS_ON_PAGE));
        if (total_pages == 0) total_pages = 1;
        paginationTableReceived.setPageCount(total_pages);
        paginationTableReceived.setCurrentPageIndex(0);
        paginationTableReceived.currentPageIndexProperty().addListener(
                ((observable, oldValue, newValue) -> updateRequestTable_Received(newValue.intValue()))
        );
        this.updateRequestTable_Received(0);
    }

    private void updateRequestTable_Received(int page) {
        List<CerereDTO> requestList = usersPage.getService().getCerereService().getRequests_ReceivedByUser_PAGED(page, usersPage.getConnected_user().getId());
        model_received.setAll(requestList);
    }

    private void initComboBoxEmails() {
        comboBoxEmails.setCellFactory(new Callback<ListView<Utilizator>, ListCell<Utilizator>>() {
            @Override
            public ListCell<Utilizator> call(ListView<Utilizator> param) {
                return new ListCell<Utilizator>() {
                    @Override
                    protected void updateItem(Utilizator item, boolean empty) {
                        super.updateItem(item, empty);

                        if (item == null || empty) {
                            setText(null);
                        } else {
                            setText("Email: " + item.getEmail() + "; Nume: " + item.getFirstName() + "; Prenume: " + item.getLastName());
                        }
                    }
                };
            }
        });

        comboBoxEmails.setConverter(new StringConverter<Utilizator>() {
            @Override
            public String toString(Utilizator user) {
                if (user == null) {
                    return null;
                }
                else {
                    return "Email: " + user.getEmail() + "; Nume: " + user.getFirstName() + "; Prenume: " + user.getLastName();
                }
            }

            @Override
            public Utilizator fromString(String string) {
                return null;
            }
        });
    }

    @Override
    public void update(FriendRequestChangeEvent friendRequestChangeEvent) {
        initModel();
    }

    @FXML
    public void handleSendRequest(javafx.event.ActionEvent ev) {
        Utilizator selected_user = comboBoxEmails.getSelectionModel().getSelectedItem();
        if (selected_user != null) {
            try {
                String nume_prieten = selected_user.getFirstName() + " " + selected_user.getLastName();
                usersPage.getService().getCerereService().sendRequest(usersPage.getConnected_user().getId(), selected_user.getId());
                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Cerere de Prietenie", "Cererea de prietenie catre " + nume_prieten + " a fost trimisa cu succes!");
            }
            catch (Exception ex) {
                MessageAlert.showErrorMessage(null, ex.getMessage());
            }
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din comboBox!");
        }
    }

    public void deleteRequest(CerereDTO selected) {
        if (selected != null) {
            try {
                String nume_prieten = selected.getFirstName() + " " + selected.getLastName();
                CerereDePrietenie deleted = usersPage.getService().getCerereService().deleteRequest(new Tuple<>(usersPage.getConnected_user().getId(), selected.getUser().getId()));
                if (deleted != null) {
                    MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Stergere", "Cererea de prietenie catre " + nume_prieten + " a fost stearsa cu succes!");
                }
            }
            catch (Exception ex) {
                MessageAlert.showErrorMessage(null, ex.getMessage());
            }
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din tabel!");
        }
    }

    public void updateRequest(CerereDTO selected_request, String selected_answer) {
        if (selected_answer != null && selected_request != null) {
            try {
                String nume_prieten = selected_request.getFirstName() + " " + selected_request.getLastName();
                usersPage.getService().getCerereService().answerRequest(usersPage.getConnected_user().getId(), selected_request.getUser().getId(), selected_answer);

                if (selected_answer.equals("approved")) {
                    MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Cerere de Prietenie", "Cererea de prietenie de la " + nume_prieten + " a fost acceptata! Acum sunteti prieteni.");
                }
                else {
                    MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Cerere de Prietenie", "Cererea de prietenie de la " + nume_prieten + " a fost respinsa!");
                }
            }
            catch (Exception ex) {
                MessageAlert.showErrorMessage(null, ex.getMessage());
                //ex.printStackTrace();
            }
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din tabel!");
        }
    }
}
