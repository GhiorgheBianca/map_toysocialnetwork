package socialnetwork.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.PasswordField;
import javafx.scene.control.CheckBox;
import javafx.stage.Stage;
import socialnetwork.domain.UsersPage;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.SuperService;

public class CreateAccountController {
    @FXML
    TextField textFieldFirstName;
    @FXML
    TextField textFieldLastName;
    @FXML
    TextField textFieldEmail;

    @FXML
    PasswordField passwordField;
    @FXML
    PasswordField passwordFieldConfirmation;

    @FXML
    CheckBox checkBoxTermsConds;
    @FXML
    CheckBox checkBoxPersonalData;

    private UsersPage usersPage;
    private Stage dialogStage;

    @FXML
    private void initialize() {

    }

    public void setPage(UsersPage usersPage,  Stage stage) {
        this.usersPage = usersPage;
        this.dialogStage = stage;
    }

    public void handleCreate() {
        String firstName = textFieldFirstName.getText();
        String lastName = textFieldLastName.getText();
        String email = textFieldEmail.getText();
        String password = passwordField.getText();
        String password_confirmation = passwordFieldConfirmation.getText();

        if (!firstName.equals("") && !lastName.equals("") && !email.equals("") && !password.equals("") && !password_confirmation.equals("")) {
            if (checkBoxTermsConds.isSelected() && checkBoxPersonalData.isSelected()) {
                if (password.equals(password_confirmation)) {
                    try {
                        Utilizator new_user = new Utilizator(firstName, lastName, email, password);
                        new_user.setPassword(password);
                        usersPage.getService().getUtilizatorService().addUtilizator(new_user);
                        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Cont nou", "Contul nou a fost creat cu succes!");
                        dialogStage.close();
                    }
                    catch (Exception ex) {
                        MessageAlert.showErrorMessage(null, ex.getMessage());
                    }
                }
                else {
                    MessageAlert.showErrorMessage(null, "Parola nu coincide cu ce ati scris la confirmarea parolei!");
                }
            }
            else {
                MessageAlert.showErrorMessage(null, "Trebuie sa acceptati termenii, conditiile si politica de utilizare a datelor cu caracter personal!");
            }
        }
        else {
            MessageAlert.showErrorMessage(null, "Trebuie sa completati toate campurile marcate cu *!");
        }
    }

    public void handleCancel() {
        dialogStage.close();
    }

}
