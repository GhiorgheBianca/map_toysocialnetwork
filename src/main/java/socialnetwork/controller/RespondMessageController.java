package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import socialnetwork.domain.Mesaj;
import socialnetwork.domain.UsersPage;
import socialnetwork.domain.Utilizator;
import socialnetwork.service.SuperService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class RespondMessageController {
    @FXML
    private TextArea textFieldMessage;

    @FXML
    ComboBox<String> comboBoxTo = new ComboBox<String>();

    private UsersPage usersPage;
    private Mesaj message;
    private Stage dialogStage;

    @FXML
    private void initialize() {
        List<String> to_options = new ArrayList<>();
        to_options.add("expeditor");
        to_options.add("expeditor + ceilalti destinatari");
        ObservableList<String> observableStatusList = FXCollections.observableArrayList(to_options);
        comboBoxTo.setItems(observableStatusList);
    }

    public void setService(UsersPage usersPage, Stage stage, Mesaj message) {
        this.usersPage = usersPage;
        this.message = message;
        this.dialogStage = stage;
    }

    @FXML
    public void handleSendMessage() {
        String selected_answer = comboBoxTo.getSelectionModel().getSelectedItem();
        if (selected_answer != null) {
            try {
                String text_message = textFieldMessage.getText();

                if (selected_answer.equals("expeditor")) {
                    usersPage.getService().getMesajService().respondMessage(usersPage.getConnected_user().getId(), this.message.getId(), text_message);
                } else if (selected_answer.equals("expeditor + ceilalti destinatari")) {
                    usersPage.getService().getMesajService().respondAllMessage(usersPage.getConnected_user().getId(), this.message.getId(), text_message);
                }

                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Mesaj", "Mesajul a fost trimis cu succes!");
                dialogStage.close();
            } catch (Exception ex) {
                MessageAlert.showErrorMessage(null, ex.getMessage());
            }
        }
        else {
            MessageAlert.showErrorMessage(null, "Nu a fost selectat niciun camp din comboBox!");
        }
    }

    @FXML
    public void handleCancel() {
        dialogStage.close();
    }
}
