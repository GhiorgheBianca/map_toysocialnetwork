package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import socialnetwork.domain.*;
import socialnetwork.service.SuperService;
import socialnetwork.utils.Constants;
import socialnetwork.utils.events.FriendRequestChangeEvent;
import socialnetwork.utils.events.MessageChangeEvent;
import socialnetwork.utils.observer.Observer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ConversationController implements Observer<MessageChangeEvent> {
    @FXML
    private TableView<MesajDTO> tableView;
    @FXML
    private TableColumn<MesajDTO,String> tableColumnEmail;
    @FXML
    private TableColumn<MesajDTO,String> tableColumnMessage;
    @FXML
    private TableColumn<MesajDTO,String> tableColumnDate;

    @FXML
    private TextField textFieldMessage;

    @FXML
    private Pagination paginationTable;

    private UsersPage usersPage;
    private Utilizator to_user;
    ObservableList<MesajDTO> model = FXCollections.observableArrayList();
    Stage dialogStage;

    @Override
    public void update(MessageChangeEvent messageChangeEvent) {
        initModel();
    }

    @FXML
    private void initialize() {
        tableColumnEmail.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("from"));
        tableColumnMessage.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("message"));
        tableColumnDate.setCellValueFactory(new PropertyValueFactory<MesajDTO, String>("date"));
        tableView.setItems(model);
    }

    private void initModel() {
        this.init_pagination();
    }

    private void init_pagination() {
        int total_pages = (int) (Math.ceil(usersPage.getService().getMesajService().number_of_messages_BetweenUsers(usersPage.getConnected_user().getId(), to_user.getId()) * 1.0 / Constants.NUMBER_OF_RECORDS_ON_PAGE));
        if (total_pages == 0) {
            total_pages = 1;
        }
        paginationTable.setPageCount(total_pages);
        paginationTable.setCurrentPageIndex(total_pages - 1);
        paginationTable.currentPageIndexProperty().addListener(
                ((observable, oldValue, newValue) -> updateMessageTable(newValue.intValue()))
        );
        this.updateMessageTable(total_pages - 1);
    }

    private void updateMessageTable(int page) {
        List<MesajDTO> messageList = usersPage.getService().getMesajService().getMesssages_BetweenUsers_PAGED(page, usersPage.getConnected_user().getId(), to_user.getId());
        model.setAll(messageList);
    }

    public void setService(UsersPage usersPage, Stage stage, Utilizator to_user) {
        this.usersPage = usersPage;
        usersPage.getService().getMesajService().addObserver(this);
        this.to_user = to_user;
        this.dialogStage = stage;
        this.initModel();
    }

    @FXML
    public void handleSendMessage_inConv() {
        try {
            String text_message = textFieldMessage.getText();

            List<Long> to_users = new ArrayList<>();
            to_users.add(to_user.getId());

            usersPage.getService().getMesajService().sendNewMessage(usersPage.getConnected_user().getId(), to_users, text_message);
            //MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Mesaj", "Mesajul a fost trimis cu succes!");
            textFieldMessage.setText("");
        }
        catch (Exception ex) {
            MessageAlert.showErrorMessage(null, ex.getMessage());
        }
    }
}
